use super::id_prefix_manager::IdPrefixManager;
use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::parsers::{parser_consts, Token};
use crate::scopes::ScopeId;
use crate::scopes::{BlockIdManager, LocalIdManager};
use crate::utils::to_system_string::ToSystemString;
use crate::utils::{base_utils, RNG};
use crate::values::{Value, ValueMap};
use std::collections::{HashMap, VecDeque};
use std::rc::Rc;

pub struct Instance {
  loading_queue: VecDeque<(Rc<Vec<Token>>, Rc<Vec<usize>>)>,
  global_vars: HashMap<String, Value>,
  block_vars: HashMap<usize, Value>,
  local_vars: HashMap<String, Value>,
  anonymous_vars: Vec<Value>,
  last_run: Option<Vec<Value>>,
  local_id_manager: LocalIdManager,
  block_id_manager: BlockIdManager,
  id_prefix_manager: IdPrefixManager,
  new_vector_id: usize,
  vector_list: HashMap<usize, Vec<Value>>,
  new_map_id: usize,
  map_list: HashMap<usize, ValueMap>,
  new_buffer_id: usize,
  buffer_list: HashMap<usize, Vec<u8>>,
  rng: RNG,
  is_debug: bool,
}

impl Instance {
  pub fn new(is_debug: bool, seed: u64) -> Self {
    let mut rng = RNG::new(seed);
    Self {
      loading_queue: VecDeque::new(),
      global_vars: HashMap::new(),
      block_vars: HashMap::new(),
      local_vars: HashMap::new(),
      anonymous_vars: Vec::new(),
      last_run: None,
      local_id_manager: LocalIdManager::new(is_debug),
      block_id_manager: BlockIdManager::new(),
      id_prefix_manager: IdPrefixManager::new(rng.generate()),
      new_vector_id: 0,
      vector_list: HashMap::new(),
      new_map_id: 0,
      map_list: HashMap::new(),
      new_buffer_id: 0,
      buffer_list: HashMap::new(),
      rng,
      is_debug,
    }
  }

  pub fn add_base_values(&mut self) {
    self.set_global_var("true", Value::Boolean(true));
    self.set_global_var("false", Value::Boolean(false));

    self.set_global_var("U_MIN", Value::Unsigned(u64::MIN)); //this is simply zero
    self.set_global_var("U_MAX", Value::Unsigned(u64::MAX));
    self.set_global_var("S_MIN", Value::Signed(i64::MIN));
    self.set_global_var("S_MAX", Value::Signed(i64::MAX));
    self.set_global_var("F_MIN", Value::Float(f64::MIN));
    self.set_global_var("F_MAX", Value::Float(f64::MAX));
    self.set_global_var("NaN", Value::Float(f64::NAN));
    self.set_global_var("Infinity", Value::Float(f64::INFINITY));
    self.set_global_var("-Infinity", Value::Float(f64::NEG_INFINITY));
  }

  pub fn parse(&mut self, code: String) -> Result<(Rc<Vec<Token>>, Rc<Vec<usize>>), String> {
    let result = crate::parsers::nau_parser::parse(
      &mut self.local_id_manager,
      &mut self.block_id_manager,
      code,
    );
    self.local_id_manager.flush_ids();
    if let Err(_) = result {
      self.block_id_manager.clear_blocks();
    } else {
      self.block_id_manager.panic_if_has_blocks();
    }
    return result;
  }

  pub fn add_to_queue(&mut self, token_list: Rc<Vec<Token>>, id_list: Rc<Vec<usize>>) {
    self.loading_queue.push_back((token_list, id_list));
  }

  pub fn get_local_id_manager(&self) -> &LocalIdManager {
    return &self.local_id_manager;
  }

  pub fn get_block_id_manager(&self) -> &BlockIdManager {
    return &self.block_id_manager;
  }

  pub fn get_id_prefix_manager(&mut self) -> &mut IdPrefixManager {
    return &mut self.id_prefix_manager;
  }

  pub fn get_last_run(&self) -> Option<Vec<Value>> {
    self.last_run.clone()
  }

  // Run

  fn normal_run(&mut self, asl: &ASL) -> Result<u64, String> {
    loop {
      match self.loading_queue.pop_front() {
        Some((token_list, id_list)) => {
          let result = CommandExec::run_command(self, asl, token_list, id_list);
          match result {
            CommandExecReturn::Error(err) => return Err(err),
            CommandExecReturn::Return(value) => {
              return Err(format!(
                "Return with the value `{}` reached the root level.",
                value.to_string(self, asl)
              ))
            }
            CommandExecReturn::Stack(stack) => self.last_run = Some(stack),
            CommandExecReturn::NoReturn => (),
            CommandExecReturn::End => (),
            CommandExecReturn::Loop(value) => {
              return Err(format!(
                "Attempt of {} a loop reached the root level.",
                if value { "continue" } else { "break" }
              ))
            }
            CommandExecReturn::Exit(exit_code) => return Ok(exit_code),
          }
        }
        None => return Ok(0),
      }
    }
  }

  fn debug_run(&mut self, asl: &ASL) -> Result<u64, String> {
    loop {
      match self.loading_queue.pop_front() {
        Some((token_list, scope_list)) => {
          let result = CommandExec::run_command(self, asl, token_list, scope_list);
          match result {
            CommandExecReturn::Error(err) => return Err(err),
            CommandExecReturn::Return(value) => {
              return Err(format!(
                "Return with the value `{}` reached the root level.",
                value.to_string(self, asl)
              ))
            }
            CommandExecReturn::Stack(stack) => {
              println!(
                "DEBUG STACK: [{}]",
                base_utils::sys_str_vec_to_str(self, asl, &stack, ",")
              );
              self.last_run = Some(stack);
            }
            CommandExecReturn::NoReturn => {
              println!("DEBUG STACK: empty");
            }
            CommandExecReturn::End => (),
            CommandExecReturn::Loop(value) => {
              return Err(format!(
                "Attempt of {} a loop reached the root level.",
                if value { "continue" } else { "break" }
              ))
            }
            CommandExecReturn::Exit(exit_code) => return Ok(exit_code),
          }
        }
        None => return Ok(0),
      }
    }
  }

  pub fn run(&mut self, asl: &ASL) -> Result<u64, String> {
    return if self.is_debug {
      self.debug_run(asl)
    } else {
      self.normal_run(asl)
    };
  }

  // Scope

  pub fn clear_scope_variables(&mut self, id_list: &Vec<usize>) -> Vec<(usize, Option<Value>)> {
    id_list
      .iter()
      .map(|id| (id.clone(), self.block_vars.remove(id)))
      .collect()
  }

  pub fn reload_scope_variables(&mut self, removed_values: Vec<(usize, Option<Value>)>) {
    for (id, entry) in removed_values {
      if let Some(value) = entry {
        self.block_vars.insert(id, value);
      } else {
        self.block_vars.remove(&id);
      }
    }
  }

  pub fn set_anonymous_variables(&mut self, new_values: Vec<Value>) -> Vec<Value> {
    let previous_values = self.anonymous_vars.clone();
    self.anonymous_vars = new_values;
    return previous_values;
  }

  // Get variable by name/id

  pub fn get_var(&self, scope_id: &ScopeId) -> Result<Value, String> {
    return match scope_id {
      ScopeId::Global(id) => self.get_global_var(id),
      ScopeId::Block(id) => self.get_block_var(id),
      ScopeId::Local(id) => self.get_local_var(id),
    };
  }

  fn get_global_var(&self, name: &str) -> Result<Value, String> {
    if let Some(value) = self.global_vars.get(name) {
      return Ok(value.clone());
    }
    return Err(format!("Global Variable `{}` does not exist.", name));
  }

  fn get_block_var(&self, id: &usize) -> Result<Value, String> {
    return if let Some(value) = self.block_vars.get(id) {
      Ok(value.clone())
    } else {
      Err(format!(
        "Variable `{}` does not exist.",
        self.block_id_manager.id_to_name(id)
      ))
    };
  }

  fn get_local_var(&self, id: &str) -> Result<Value, String> {
    return if let Some(value) = self.local_vars.get(id) {
      Ok(value.clone())
    } else {
      Err(format!(
        "Local Variable `{}` does not exist.",
        self.local_id_manager.id_to_name(id)
      ))
    };
  }

  pub fn get_anonymous_var(&self, id: &u8) -> Result<Value, String> {
    match id {
      0..=9 => {
        if let Some(value) = self.anonymous_vars.get(id.clone() as usize) {
          Ok(value.clone())
        } else {
          Err(format!(
            "Anonymous Variable with id \"{}\" does not exist.",
            id
          ))
        }
      }
      &parser_consts::ANONYMOUS_VAR_ID => {
        if self.anonymous_vars.len() > 0 {
          Ok(self.anonymous_vars[self.anonymous_vars.len() - 1].clone())
        } else {
          Err(String::from("Anonymous Variable is empty."))
        }
      }
      &parser_consts::ANONYMOUS_VAR_LIST_ID => {
        Ok(Value::List(Rc::new(self.anonymous_vars.clone())))
      }
      _ => panic!("Impossible anonymous id reached"),
    }
  }

  // Get variable by reference

  pub fn get_var_by_ref(&self, reference: &ScopeId) -> Result<Value, String> {
    match reference {
      ScopeId::Global(name) => {
        if let Some(value) = self.global_vars.get(name.as_str()) {
          return Ok(value.clone());
        }
      }
      ScopeId::Local(id) => {
        if let Some(value) = self.local_vars.get(id.as_str()) {
          return Ok(value.clone());
        }
      }
      ScopeId::Block(id) => {
        if let Some(value) = self.block_vars.get(id) {
          return Ok(value.clone());
        }
      }
    }
    return Err(format!("Referenced variable does not exist."));
  }

  // Set variable by name/id

  pub fn set_var(&mut self, scope_id: &ScopeId, value: Value) {
    return match scope_id {
      ScopeId::Global(id) => self.set_global_var(id, value),
      ScopeId::Block(id) => self.set_block_var(id, value),
      ScopeId::Local(id) => self.set_local_var(id, value),
    };
  }

  pub fn set_block_var(&mut self, id: &usize, value: Value) {
    self.block_vars.insert(id.clone(), value);
  }

  pub fn set_global_var(&mut self, name: &str, value: Value) {
    self.global_vars.insert(String::from(name), value);
  }

  fn set_local_var(&mut self, id: &str, value: Value) {
    self.local_vars.insert(String::from(id), value);
  }

  // Set variable by reference

  pub fn set_var_by_ref(&mut self, reference: &ScopeId, value: Value) {
    match reference {
      ScopeId::Global(name) => {
        self.global_vars.insert(String::from(name.as_str()), value);
      }
      ScopeId::Local(id) => {
        self.local_vars.insert(String::clone(id), value);
      }
      ScopeId::Block(id) => {
        self.block_vars.insert(id.clone(), value);
      }
    }
  }

  // Delete variable by reference

  pub fn delete_var_by_ref(&mut self, reference: &ScopeId) {
    match reference {
      ScopeId::Global(name) => {
        self.global_vars.remove(name.as_str());
      }
      ScopeId::Local(id) => {
        self.local_vars.remove(id.as_str());
      }
      ScopeId::Block(id) => {
        self.block_vars.remove(id);
      }
    }
  }

  // Create variable reference

  pub fn create_var_ref(&self, scope_id: &ScopeId) -> Value {
    match scope_id {
      ScopeId::Global(id) => self.create_global_var_ref(id),
      ScopeId::Block(id) => self.create_block_var_ref(id),
      ScopeId::Local(id) => self.create_local_var_ref(id),
    }
  }

  fn create_global_var_ref(&self, id: &str) -> Value {
    Value::Reference(ScopeId::Global(String::from(id)))
  }

  fn create_block_var_ref(&self, id: &usize) -> Value {
    Value::Reference(ScopeId::Block(id.clone()))
  }

  pub fn create_local_var_ref(&self, id: &str) -> Value {
    Value::Reference(ScopeId::Local(String::from(id)))
  }

  //Token type to name conversion

  pub fn scope_id_to_name(&self, scope_id: &ScopeId) -> String {
    match scope_id {
      ScopeId::Global(id) => id.clone(),
      ScopeId::Block(id) => self.block_id_manager.id_to_name(id),
      ScopeId::Local(id) => self.local_id_manager.id_to_name(id),
    }
  }

  // Vector Manipulation

  fn create_vector_id(&mut self) -> usize {
    let id = self.new_vector_id;
    self.new_vector_id += 1;
    id
  }

  pub fn create_new_vector(&mut self) -> usize {
    let id = self.create_vector_id();
    self.vector_list.insert(id, Vec::new());
    id
  }

  pub fn create_new_vector_from_list(&mut self, vec: &Vec<Value>) -> usize {
    let id = self.create_vector_id();
    self.vector_list.insert(id, vec.clone());
    id
  }

  pub fn clone_vector(&mut self, id: &usize) -> Result<usize, String> {
    let new_vec;
    if let Some(vec) = self.vector_list.get_mut(&id) {
      new_vec = vec.clone();
    } else {
      return Err(format!("Vector with id {} does not exist in memory.", id));
    }
    let id = self.create_vector_id();
    self.vector_list.insert(id, new_vec);
    Ok(id)
  }

  pub fn get_vector(&mut self, id: &usize) -> Result<&mut Vec<Value>, String> {
    if let Some(vec) = self.vector_list.get_mut(id) {
      Ok(vec)
    } else {
      Err(format!("Vector with id {} does not exist in memory.", id))
    }
  }

  pub fn delete_vector(&mut self, id: &usize) {
    self.vector_list.remove(&id);
  }

  pub fn clear_vector(&mut self, id: &usize) {
    if let Some(vec) = self.vector_list.get_mut(id) {
      vec.clear()
    }
  }

  // Map Manipulation

  fn create_map_id(&mut self) -> usize {
    let id = self.new_map_id;
    self.new_map_id += 1;
    id
  }

  pub fn create_new_map(&mut self) -> usize {
    let id = self.create_map_id();
    self.map_list.insert(id, ValueMap::new());
    id
  }

  pub fn create_new_map_from_dict(&mut self, dict: &ValueMap) -> usize {
    let id = self.create_map_id();
    self.map_list.insert(id, dict.clone());
    id
  }

  pub fn create_new_map_from_list(
    &mut self,
    list: &Vec<Value>,
    asl: &ASL,
  ) -> Result<usize, String> {
    match ValueMap::from_value_list(self, asl, list) {
      Ok(map) => {
        let id = self.create_map_id();
        self.map_list.insert(id, map);
        Ok(id)
      }
      Err(err) => Err(err),
    }
  }

  pub fn clone_map(&mut self, id: &usize) -> Result<usize, String> {
    let new_map;
    if let Some(map) = self.map_list.get_mut(&id) {
      new_map = map.clone();
    } else {
      return Err(format!("Map with id {} does not exist in memory.", id));
    }
    let id = self.create_map_id();
    self.map_list.insert(id, new_map);
    Ok(id)
  }

  pub fn get_map(&mut self, id: &usize) -> Result<&mut ValueMap, String> {
    if let Some(map) = self.map_list.get_mut(id) {
      Ok(map)
    } else {
      Err(format!("Map with id {} does not exist in memory.", id))
    }
  }

  pub fn delete_map(&mut self, id: &usize) {
    self.map_list.remove(&id);
  }

  pub fn clear_map(&mut self, id: &usize) {
    if let Some(map) = self.map_list.get_mut(id) {
      map.clear()
    }
  }

  // Buffer Manipulation

  fn create_buffer_id(&mut self) -> usize {
    let id = self.new_buffer_id;
    self.new_buffer_id += 1;
    id
  }

  pub fn create_new_buffer(&mut self) -> usize {
    let id = self.create_buffer_id();
    self.buffer_list.insert(id, Vec::new());
    id
  }

  pub fn create_new_buffer_from_raw(&mut self, raw: &Vec<u8>) -> usize {
    let id = self.create_buffer_id();
    self.buffer_list.insert(id, raw.clone());
    id
  }

  pub fn clone_buffer(&mut self, id: &usize) -> Result<usize, String> {
    let new_buffer;
    if let Some(buffer) = self.buffer_list.get_mut(&id) {
      new_buffer = buffer.clone();
    } else {
      return Err(format!("Buffer with id {} does not exist in memory.", id));
    }
    let id = self.create_buffer_id();
    self.buffer_list.insert(id, new_buffer);
    Ok(id)
  }

  pub fn get_buffer(&mut self, id: &usize) -> Result<&mut Vec<u8>, String> {
    if let Some(buffer) = self.buffer_list.get_mut(id) {
      Ok(buffer)
    } else {
      Err(format!("Buffer with id {} does not exist in memory.", id))
    }
  }

  pub fn delete_buffer(&mut self, id: &usize) {
    self.buffer_list.remove(&id);
  }

  pub fn clear_buffer(&mut self, id: &usize) {
    if let Some(buffer) = self.buffer_list.get_mut(id) {
      buffer.clear()
    }
  }

  pub fn replace_buffer(&mut self, id: &usize, new_buffer: Vec<u8>) {
    self.buffer_list.insert(id.clone(), new_buffer);
  }

  //Get random number generator

  pub fn get_rng(&mut self) -> &mut RNG {
    &mut self.rng
  }

  //Variable dump

  pub fn dump_global_vars(&mut self) -> Vec<(String, Value)> {
    let mut result = Vec::new();
    for k in self.global_vars.keys() {
      result.push((k.clone(), self.global_vars.get(k).unwrap().clone()));
    }
    result
  }
}
