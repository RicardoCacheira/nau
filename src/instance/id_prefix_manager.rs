use crate::utils::RNG;

pub struct IdPrefixManager {
  rng: RNG,
  prefix_list: Vec<(String, String)>,
  master_key: String,
  was_master_key_read: bool,
}

impl IdPrefixManager {
  pub fn new(generator_seed: u64) -> IdPrefixManager {
    let mut rng = RNG::new(generator_seed);
    let master_key = rng.generate_alfa(64);
    IdPrefixManager {
      rng,
      prefix_list: Vec::new(),
      master_key,
      was_master_key_read: false,
    }
  }

  pub fn get_master_key(&mut self) -> String {
    if self.was_master_key_read {
      String::new()
    } else {
      self.was_master_key_read = true;
      self.master_key.clone()
    }
  }

  pub fn check_id(&self, id: &str, key_list: &Vec<String>) -> bool {
    if id.is_empty() {
      return false;
    }
    if !self.is_master_key(key_list) {
      for (prefix, key) in self.prefix_list.iter() {
        if id.starts_with(prefix) {
          let mut iter = key_list.iter().skip_while(|p| p != &prefix);
          iter.next();
          if let Some(k) = iter.next() {
            return k == key;
          }
          return false;
        }
      }
    }
    return true;
  }
  pub fn add_prefix(&mut self, new_prefix: &str) -> Option<String> {
    for (prefix, _) in self.prefix_list.iter() {
      if prefix.starts_with(new_prefix) || new_prefix.starts_with(prefix) {
        return None;
      }
    }
    let key = self.rng.generate_alfa(32);
    self
      .prefix_list
      .push((String::from(new_prefix), key.clone()));
    return Some(key);
  }

  fn is_master_key(&self, key_list: &Vec<String>) -> bool {
    key_list.len() == 1 && key_list.get(0).unwrap() == &self.master_key
  }
}
