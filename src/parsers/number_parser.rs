use num_bigint::BigInt;
use num_rational::BigRational;

enum NumberSign {
  None,
  Negative,
}

#[derive(PartialEq)]
enum NumberSeparator {
  None,
  Point,
  Divider,
}

enum NumberType {
  None,
  Unsigned,
  Signed,
  Float,
}

struct TokenizedNumber {
  sign: NumberSign,
  first_num: String,
  separator: NumberSeparator,
  last_num: String,
  number_type: NumberType,
}

fn tokenize_number(input: &String) -> Option<TokenizedNumber> {
  let mut result = TokenizedNumber {
    sign: NumberSign::None,
    first_num: String::new(),
    separator: NumberSeparator::None,
    last_num: String::new(),
    number_type: NumberType::None,
  };
  for c in input.chars() {
    match (c, &result) {
      (
        '-',
        TokenizedNumber {
          sign: NumberSign::None,
          first_num,
          separator: NumberSeparator::None,
          last_num,
          number_type: NumberType::None,
        },
      ) if first_num.is_empty() && last_num.is_empty() => result.sign = NumberSign::Negative,
      (
        '/',
        TokenizedNumber {
          first_num,
          separator: NumberSeparator::None,
          number_type: NumberType::None,
          ..
        },
      ) if first_num.len() > 0 => result.separator = NumberSeparator::Divider,
      (
        '.',
        TokenizedNumber {
          separator: NumberSeparator::None,
          number_type: NumberType::None,
          ..
        },
      ) => result.separator = NumberSeparator::Point,
      (
        'u',
        TokenizedNumber {
          sign: NumberSign::None,
          first_num,
          separator: NumberSeparator::None,
          number_type: NumberType::None,
          ..
        },
      ) if first_num.len() > 0 => result.number_type = NumberType::Unsigned,
      (
        's',
        TokenizedNumber {
          first_num,
          separator: NumberSeparator::None,
          number_type: NumberType::None,
          ..
        },
      ) if first_num.len() > 0 => result.number_type = NumberType::Signed,
      (
        'f',
        TokenizedNumber {
          first_num,
          last_num,
          number_type: NumberType::None,
          ..
        },
      ) if first_num.len() > 0 || last_num.len() > 0 => result.number_type = NumberType::Float,
      (
        c,
        TokenizedNumber {
          separator: NumberSeparator::None,
          number_type: NumberType::None,
          ..
        },
      ) if c.is_ascii_digit() => {
        result.first_num.push(c);
      }
      (
        c,
        TokenizedNumber {
          separator: NumberSeparator::Point,
          number_type: NumberType::None,
          ..
        },
      )
      | (
        c,
        TokenizedNumber {
          separator: NumberSeparator::Divider,
          number_type: NumberType::None,
          ..
        },
      ) if c.is_ascii_digit() => {
        result.last_num.push(c);
      }
      (
        c,
        TokenizedNumber {
          separator: NumberSeparator::Point,
          number_type: NumberType::None,
          ..
        },
      )
      | (
        c,
        TokenizedNumber {
          separator: NumberSeparator::Divider,
          number_type: NumberType::None,
          ..
        },
      ) if c.is_ascii_digit() => {
        result.last_num.push(c);
      }
      (_, _) => return None,
    }
  }
  if result.last_num.is_empty() && result.separator != NumberSeparator::None {
    return None;
  }
  if result.first_num.is_empty() && result.last_num.is_empty() {
    return None;
  }
  return Some(result);
}

#[derive(PartialEq)]
pub enum ParsedNumber {
  Rational(BigRational),
  Unsigned(u64),
  Signed(i64),
  Float(f64),
  InvalidFormat,
}

impl ToString for ParsedNumber {
  fn to_string(&self) -> String {
    match self {
      Self::Rational(v) => format!("Rational({})", v),
      Self::Unsigned(v) => format!("Unsigned({})", v),
      Self::Signed(v) => format!("Signed({})", v),
      Self::Float(v) => format!("Float({})", v),
      Self::InvalidFormat => String::from("InvalidFormat"),
    }
  }
}

fn power_of_ten(exponent: usize) -> BigInt {
  let mut aux = String::with_capacity(exponent + 1);
  aux.push('1');
  for _ in 0..exponent {
    aux.push('0');
  }
  aux.parse().unwrap()
}

pub fn parse_number(s: &str) -> Result<ParsedNumber, String> {
  let parsed = tokenize_number(&String::from(s));
  if let Some(mut pn) = parsed {
    if let NumberSeparator::Point = pn.separator {
      if pn.first_num.is_empty() {
        pn.first_num.push('0');
      }
      if pn.last_num.is_empty() {
        pn.last_num.push('0');
      }
    }
    if let NumberSign::Negative = pn.sign {
      let mut aux = String::from("-");
      aux.push_str(&pn.first_num);
      pn.first_num = aux;
    }
    match pn {
      TokenizedNumber {
        number_type: NumberType::Unsigned,
        first_num,
        ..
      } => match first_num.parse() {
        Ok(value) => return Ok(ParsedNumber::Unsigned(value)),
        Err(err) => return Err(err.to_string()),
      },
      TokenizedNumber {
        number_type: NumberType::Signed,
        first_num,
        ..
      } => match first_num.parse() {
        Ok(value) => return Ok(ParsedNumber::Signed(value)),
        Err(err) => return Err(err.to_string()),
      },
      TokenizedNumber {
        number_type: NumberType::Float,
        first_num,
        separator: NumberSeparator::None,
        ..
      } => match first_num.parse() {
        Ok(value) => return Ok(ParsedNumber::Float(value)),
        Err(err) => return Err(err.to_string()),
      },
      TokenizedNumber {
        number_type: NumberType::Float,
        first_num,
        separator: NumberSeparator::Point,
        last_num,
        ..
      } => {
        let mut aux = String::from(&first_num);
        aux.push('.');
        aux.push_str(&last_num);
        match aux.parse() {
          Ok(value) => return Ok(ParsedNumber::Float(value)),
          Err(err) => return Err(err.to_string()),
        }
      }
      TokenizedNumber {
        number_type: NumberType::Float,
        first_num,
        separator: NumberSeparator::Divider,
        last_num,
        ..
      } => match (first_num.parse::<f64>(), last_num.parse::<f64>()) {
        (Ok(numerator), Ok(denominator)) => {
          return Ok(ParsedNumber::Float(numerator / denominator))
        }
        (Err(err1), Err(err2)) => {
          return Err(format!(
            "Numerator Error: {}\nDenominator Error: {}",
            err1.to_string(),
            err2.to_string()
          ))
        }
        (Err(err), _) => return Err(format!("Numerator Error: {}", err.to_string())),
        (_, Err(err)) => return Err(format!("Denominator Error: {}", err.to_string())),
      },
      TokenizedNumber {
        number_type: NumberType::None,
        first_num,
        separator: NumberSeparator::None,
        ..
      } => match first_num.parse() {
        Ok(value) => return Ok(ParsedNumber::Rational(value)),
        Err(err) => return Err(err.to_string()),
      },
      TokenizedNumber {
        number_type: NumberType::None,
        first_num,
        separator: NumberSeparator::Point,
        last_num,
        ..
      } => {
        let mut aux = String::from(&first_num);
        aux.push_str(&last_num);
        match aux.parse() {
          Ok(value) => {
            return Ok(ParsedNumber::Rational(BigRational::from((
              value,
              power_of_ten(last_num.len()),
            ))))
          }
          Err(err) => return Err(err.to_string()),
        }
      }
      TokenizedNumber {
        number_type: NumberType::None,
        first_num,
        separator: NumberSeparator::Divider,
        last_num,
        ..
      } => match (first_num.parse::<BigInt>(), last_num.parse::<BigInt>()) {
        (Ok(numerator), Ok(denominator)) => {
          return Ok(ParsedNumber::Rational(BigRational::from((
            numerator,
            denominator,
          ))))
        }
        (Err(err1), Err(err2)) => {
          return Err(format!(
            "Numerator Error: {}\nDenominator Error: {}",
            err1.to_string(),
            err2.to_string()
          ))
        }
        (Err(err), _) => return Err(format!("Numerator Error: {}", err.to_string())),
        (_, Err(err)) => return Err(format!("Denominator Error: {}", err.to_string())),
      },
    }
  }
  return Ok(ParsedNumber::InvalidFormat);
}