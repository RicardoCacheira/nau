use crate::parsers::number_parser::ParsedNumber;
use crate::parsers::{number_parser, parser_consts, var_checker, Token};
use crate::scopes::{BlockIdManager, LocalIdManager, ScopeId};
use std::rc::Rc;
use std::str::Chars;

macro_rules! return_error_line {
    ($err:expr,$info:expr) => {
        return Err(format!("{}\non {}:{}", $err, $info.file, $info.start_line));//TODO review if start line or curr line
    };
}

macro_rules! return_token_error_line {
  ($err:expr,$token:expr,$info:expr) => {
    return Err(format!(
      "{}\nat `{}`, on {}:{}",
      $err,
      $token,
      $info.file,
      $info.start_line //TODO review if start line or curr line
    ));
  };
}

struct CodeChars<'a> {
  char_list: Chars<'a>,
  line: usize,
  col: usize,
}

impl CodeChars<'_> {
  fn from(string: &String) -> CodeChars {
    CodeChars {
      char_list: string.chars(),
      line: 1,
      col: 0,
    }
  }
  fn next(&mut self) -> Option<char> {
    let res = self.char_list.next();
    if let Some(c) = res {
      if c == '\n' {
        self.line += 1;
        self.col = 0;
      } else {
        self.col += 1;
      }
    }
    res
  }
  fn reset_count(&mut self) {
    self.line = 1;
    self.col = 0;
  }
}

struct ParserState<'a> {
  local_manager: &'a mut LocalIdManager,
  block_manager: &'a mut BlockIdManager,
  result: Vec<Token>,
  blocks: Vec<ParserBlockToken>,
  file: String,
  start_line: usize,
  start_col: usize,
}

impl ParserState<'_> {
  fn get_start_file_position(&self) -> Rc<String> {
    Rc::new(format!(
      "{}:{}:{}",
      self.file, self.start_line, self.start_col
    ))
  }
  fn get_file_position(&self, line: usize, column: usize) -> Rc<String> {
    Rc::new(format!("{}:{}:{}", self.file, line, column))
  }
}

enum ParserBlockToken {
  Command(usize),
  List {
    size: usize,
    start_line: usize,
    start_col: usize,
  },
  Dictionary {
    size: usize,
    start_line: usize,
    start_col: usize,
  },
  FunctionCall {
    pos: usize,
    scope_id: ScopeId,
    start_line: usize,
    start_col: usize,
  },
  AnonymousFunctionCall {
    id: u8,
    pos: usize,
    start_line: usize,
    start_col: usize,
  },
  RefFunctionCall {
    pos: usize,
    scope_id: ScopeId,
    start_line: usize,
    start_col: usize,
  },
  AnonymousRefFunctionCall {
    id: u8,
    pos: usize,
    start_line: usize,
    start_col: usize,
  },
}

fn is_whitespace(c: char) -> bool {
  c.is_whitespace() || c == ','
}

fn add_token(parser_state: &mut ParserState, token: Token) {
  // if let Token::Define { .. } = token {
  // } else {
  //   parser_state.block_manager.tick_delay();
  // }
  parser_state.result.push(token);
}

fn add_block(parser_state: &mut ParserState, token: ParserBlockToken) {
  parser_state.block_manager.start_block();
  parser_state.blocks.push(token);
}

fn generate_scope_id(
  parser_state: &mut ParserState,
  scope_indicator: &str,
  name: &str,
  is_definition: bool,
) -> ScopeId {
  match scope_indicator {
    parser_consts::GLOBAL_STR => ScopeId::Global(String::from(name)),
    parser_consts::LOCAL_STR => {
      ScopeId::Local(parser_state.local_manager.generate_internal_id(name))
    }
    _ => {
      if is_definition {
        ScopeId::Block(parser_state.block_manager.new_var(name))
      } else {
        match parser_state.block_manager.get_var_id(name) {
          Some(id) => ScopeId::Block(id),
          None => ScopeId::Global(String::from(name)),
        }
      }
    }
  }
}

// fn generate_define_scope_id(
//   parser_state: &mut ParserState,
//   scope_indicator: &str,
//   name: &str,
// ) -> ScopeId {
//   match scope_indicator {
//     parser_consts::GLOBAL_STR => ScopeId::Global(String::from(name)),
//     parser_consts::LOCAL_STR => {
//       ScopeId::Local(parser_state.local_manager.generate_internal_id(name))
//     }
//     _ => ScopeId::Block(parser_state.block_manager.new_delayed_var(name)),
//   }
// }

fn parse_var(parser_state: &mut ParserState, buffer: String) -> Result<(), String> {
  let mut var = buffer.clone();

  let prefix: String = var_checker::get_prefix(&var);
  //Removes the prefix
  var = var.split_off(prefix.len());

  let suffix = if var.ends_with(parser_consts::COMMAND_START) {
    //Removes the suffix
    var.truncate(var.len() - 1);
    parser_consts::COMMAND_START
  } else {
    ' '
  };

  let scope_indicator = var_checker::get_scope_indicator(&var);
  var = var.split_off(scope_indicator.len());

  if let Some(id) = var_checker::check_if_var_is_anonymous(&var) {
    match (prefix.as_ref(), suffix) {
      (parser_consts::GET_REF_STR, ' ') => add_token(
        parser_state,
        Token::AnonymousGetRef {
          file: parser_state.get_start_file_position(),
          id,
        },
      ),
      ("", ' ') => add_token(
        parser_state,
        Token::AnonymousVar {
          file: parser_state.get_start_file_position(),
          id,
        },
      ),
      ("", parser_consts::COMMAND_START) => add_block(
        parser_state,
        ParserBlockToken::AnonymousFunctionCall {
          id,
          pos: parser_state.result.len(),
          start_line: parser_state.start_line,
          start_col: parser_state.start_col,
        },
      ),
      (parser_consts::GET_REF_STR, parser_consts::COMMAND_START) => add_block(
        parser_state,
        ParserBlockToken::AnonymousRefFunctionCall {
          id,
          pos: parser_state.result.len(),
          start_line: parser_state.start_line,
          start_col: parser_state.start_col,
        },
      ),
      (_, _) => return_error_line!(
        format!(
          "Impossible pattern found for an anonymous variable. ({}{}{})",
          prefix, var, suffix,
        ),
        parser_state
      ),
    };
    return Ok(());
  }

  if let Err(err) = var_checker::check_if_var_is_valid(&var) {
    return_token_error_line!(err, buffer, parser_state)
  }

  match (prefix.as_ref(), var.as_str(), suffix) {
    (parser_consts::DEFINE_STR, _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, true);
      add_token(
        parser_state,
        Token::Define {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    (parser_consts::SET_STR, _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_token(
        parser_state,
        Token::Set {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    (parser_consts::REF_STR, _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_token(
        parser_state,
        Token::Ref {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    (parser_consts::GET_REF_STR, _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_token(
        parser_state,
        Token::GetRef {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    (parser_consts::SET_REF_STR, _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_token(
        parser_state,
        Token::SetRef {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    //TODO make this method detect if the string is a proper variable
    (parser_consts::DECLARE_STR, _, ' ') => {
      let id = parser_state.block_manager.new_var(&var);
      add_token(parser_state, Token::Declare(id))
    }
    ("", _, ' ') => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_token(
        parser_state,
        Token::Var {
          file: parser_state.get_start_file_position(),
          scope_id,
        },
      )
    }
    ("", _, parser_consts::COMMAND_START) => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_block(
        parser_state,
        ParserBlockToken::FunctionCall {
          pos: parser_state.result.len(),
          scope_id,
          start_line: parser_state.start_line,
          start_col: parser_state.start_col,
        },
      )
    }
    (parser_consts::GET_REF_STR, _, parser_consts::COMMAND_START) => {
      let scope_id = generate_scope_id(parser_state, &scope_indicator, &var, false);
      add_block(
        parser_state,
        ParserBlockToken::RefFunctionCall {
          pos: parser_state.result.len(),
          scope_id,
          start_line: parser_state.start_line,
          start_col: parser_state.start_col,
        },
      )
    }
    (_, _, parser_consts::COMMAND_START) => {
      return_token_error_line!("Prefix found on a function call", buffer, parser_state)
    }
    (_, _, _) => panic!("Impossible pattern found."),
  };
  return Ok(());
}

fn parse_token(parser_state: &mut ParserState, buffer: String) -> Result<(), String> {
  match number_parser::parse_number(&buffer) {
    Ok(ParsedNumber::Rational(value)) => add_token(parser_state, Token::Number(value)),
    Ok(ParsedNumber::Unsigned(value)) => add_token(parser_state, Token::Unsigned(value)),
    Ok(ParsedNumber::Signed(value)) => add_token(parser_state, Token::Signed(value)),
    Ok(ParsedNumber::Float(value)) => add_token(parser_state, Token::Float(value)),
    Ok(ParsedNumber::InvalidFormat) => return parse_var(parser_state, buffer),
    Err(_) => return_token_error_line!("Error parsing number", buffer, parser_state),
  }
  return Ok(());
}

fn consume_comment(parser_state: &mut ParserState, code: &mut CodeChars) -> Result<(), String> {
  match code.next() {
    Some('\n') => {
      return Ok(());
    }
    None => return Ok(()),
    Some(parser_consts::COMMENT_BLOCK_CHAR) => {
      return consume_comment_block(parser_state, code);
    }
    Some(_) => (),
  }
  loop {
    match code.next() {
      Some('\n') => {
        return Ok(());
      }
      None => return Ok(()),
      Some(_) => (),
    }
  }
}

fn consume_comment_block(
  parser_state: &mut ParserState,
  code: &mut CodeChars,
) -> Result<(), String> {
  let mut block = String::new();
  loop {
    match code.next() {
      Some(parser_consts::COMMENT_BLOCK_CHAR) => match code.next() {
        Some(parser_consts::COMMENT_START_CHAR) => {
          if block.len() > 1
            && block.starts_with(parser_consts::COMMENT_START_CHAR)
            && block.ends_with(parser_consts::COMMENT_START_CHAR)
          {
            block.pop();
            let mut iter = block.chars();
            iter.next();
            parser_state.file = String::from(iter.as_str());
            code.reset_count();
            //TODO consider adding a flush_ids for the total bootstrap
          }
          return Ok(());
        }
        Some('\n') => (),
        Some(c) => {
          block.push(parser_consts::COMMENT_BLOCK_CHAR);
          block.push(c);
        }
        None => return_error_line!("Unfinished comment block", parser_state),
      },
      Some('\n') => (),
      Some(c) => block.push(c),
      None => return_error_line!("Unfinished comment block", parser_state),
    }
  }
}

fn consume_string(parser_state: &mut ParserState, code: &mut CodeChars) -> Result<Token, String> {
  let mut result = String::new();
  loop {
    match code.next() {
      Some(parser_consts::STRING_LIMITER_CHAR) => return Ok(Token::String(Rc::new(result))),
      Some('\\') => match code.next() {
        Some('\\') => result.push('\\'),
        Some('n') => result.push('\n'),
        Some('t') => result.push('\t'),
        Some('r') => result.push('\r'),
        Some('e') => result.push(parser_consts::ESCAPE_CHAR),
        Some('b') => result.push(parser_consts::BACKSPACE_CHAR),
        Some(parser_consts::STRING_LIMITER_CHAR) => result.push(parser_consts::STRING_LIMITER_CHAR),
        Some(_) | None => return_error_line!("Invalid escape sequence", parser_state),
      },
      Some('\n') => {
        result.push('\n');
      }
      Some(c) => result.push(c),
      None => return_error_line!("Unfinished string", parser_state),
    }
  }
}

fn char_to_u8(a: char) -> u8 {
  match a {
    '0'..='9' => a as u8 - '0' as u8,
    'A'..='F' => a as u8 - 'A' as u8 + 10,
    'a'..='f' => a as u8 - 'a' as u8 + 10,
    _ => 0,
  }
}

fn chars_to_u8(a: char, b: char) -> u8 {
  (char_to_u8(a) << 4) + char_to_u8(b)
}

fn consume_raw(parser_state: &mut ParserState, code: &mut CodeChars) -> Result<Token, String> {
  let mut result = Vec::new();
  loop {
    match code.next() {
      Some(parser_consts::RAW_CHAR) => {
        if let Some(parser_consts::SYSTEM_VALUE) = code.next() {
          return Ok(Token::Raw(Rc::new(result)));
        } else {
          return_error_line!(
            format!(
              "Invalid char '{}' on byte {}",
              parser_consts::RAW_CHAR,
              result.len()
            ),
            parser_state
          );
        }
      }
      Some(a) if a.is_ascii_hexdigit() => match code.next() {
        Some(b) if b.is_ascii_hexdigit() => result.push(chars_to_u8(a, b)),
        Some(_) => return_error_line!(
          format!("Missing valid second char on byte {}", result.len()),
          parser_state
        ),
        None => return_error_line!("Unfinished raw value", parser_state),
      },
      Some(a) if is_whitespace(a) => (),
      Some(a) => return_error_line!(
        format!("Invalid char '{}' on byte {}", a, result.len()),
        parser_state
      ),
      None => return_error_line!("Unfinished raw value", parser_state),
    }
  }
}

fn consume_system_value(
  parser_state: &mut ParserState,
  code: &mut CodeChars,
) -> Result<Token, String> {
  match code.next() {
    Some(parser_consts::RAW_CHAR) => return consume_raw(parser_state, code),
    Some(a) => return_error_line!(
      format!(
        "Unknow system value start '{}{}'",
        parser_consts::SYSTEM_VALUE,
        a
      ),
      parser_state
    ),
    None => return_error_line!("Unfinished raw value", parser_state),
  }
}

fn process_command_end(parser_state: &mut ParserState) -> Result<(), String> {
  match parser_state.blocks.pop() {
    Some(ParserBlockToken::Command(size)) => {
      let token_list = parser_state.result.split_off(size);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::Command {
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
        },
      );
    }
    Some(ParserBlockToken::FunctionCall {
      pos: p,
      scope_id,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(p);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::FunctionCall {
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          scope_id,
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(ParserBlockToken::AnonymousFunctionCall {
      id,
      pos: p,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(p);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::AnonymousFunctionCall {
          id,
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(ParserBlockToken::RefFunctionCall {
      pos: p,
      scope_id,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(p);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::RefFunctionCall {
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          scope_id,
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(ParserBlockToken::AnonymousRefFunctionCall {
      id,
      pos: p,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(p);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::AnonymousRefFunctionCall {
          id,
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(_) => return_error_line!("Ending non-command block", parser_state),
    None => return_error_line!("Missing command start", parser_state),
  }
  return Ok(());
}

fn process_list_end(parser_state: &mut ParserState) -> Result<(), String> {
  match parser_state.blocks.pop() {
    Some(ParserBlockToken::List {
      size,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(size);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::List {
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(_) => return_error_line!("Ending non-list block", parser_state),
    None => return_error_line!("Missing list start", parser_state),
  }
  return Ok(());
}

fn process_dictionary_end(parser_state: &mut ParserState) -> Result<(), String> {
  match parser_state.blocks.pop() {
    Some(ParserBlockToken::Dictionary {
      size,
      start_line,
      start_col,
    }) => {
      let token_list = parser_state.result.split_off(size);
      let id_list = parser_state.block_manager.end_block();
      add_token(
        parser_state,
        Token::Dictionary {
          token_list: Rc::new(token_list),
          id_list: Rc::new(id_list),
          file: parser_state.get_file_position(start_line, start_col),
        },
      );
    }
    Some(_) => return_error_line!("Ending non-dictionary block", parser_state),
    None => return_error_line!("Missing dictionary start", parser_state),
  }
  return Ok(());
}

pub fn parse(
  local_manager: &mut LocalIdManager,
  block_manager: &mut BlockIdManager,
  code: String,
) -> Result<(Rc<Vec<Token>>, Rc<Vec<usize>>), String> {
  let mut is_reading = false;
  let mut code = CodeChars::from(&code);
  let mut buffer = String::new();
  let mut parser_state = ParserState {
    local_manager,
    block_manager,
    result: Vec::new(),
    blocks: Vec::new(),
    file: String::from("system"),
    start_line: 1,
    start_col: 1,
  };
  parser_state.block_manager.start_block();
  loop {
    match code.next() {
      Some(c) => {
        if is_reading {
          match c {
            parser_consts::COMMENT_START_CHAR => {
              is_reading = false;
              parse_token(&mut parser_state, buffer)?;
              buffer = String::new();
              consume_comment(&mut parser_state, &mut code)?;
            }
            parser_consts::STRING_LIMITER_CHAR => {
              return_error_line!("Invalid string start", parser_state)
            }
            parser_consts::SYSTEM_VALUE => {
              return_error_line!("Invalid system value start", parser_state)
            }
            parser_consts::COMMAND_START => {
              is_reading = false;
              buffer.push(c);
              parse_token(&mut parser_state, buffer)?;
              buffer = String::new();
            }
            parser_consts::LIST_START => return_error_line!(
              format!(
                "Unexpected list start on token `{}{}`",
                buffer,
                parser_consts::LIST_START
              ),
              parser_state
            ),
            parser_consts::DICTIONARY_START => return_error_line!(
              format!(
                "Unexpected dictionary start on token `{}{}`",
                buffer,
                parser_consts::DICTIONARY_START
              ),
              parser_state
            ),
            parser_consts::COMMAND_END => {
              is_reading = false;
              parse_token(&mut parser_state, buffer)?;
              buffer = String::new();
              process_command_end(&mut parser_state)?;
            }
            parser_consts::LIST_END => {
              is_reading = false;
              parse_token(&mut parser_state, buffer)?;
              buffer = String::new();
              process_list_end(&mut parser_state)?;
            }
            parser_consts::DICTIONARY_END => {
              is_reading = false;
              parse_token(&mut parser_state, buffer)?;
              buffer = String::new();
              process_dictionary_end(&mut parser_state)?;
            }
            _ => {
              if is_whitespace(c) {
                is_reading = false;
                parse_token(&mut parser_state, buffer)?;
                buffer = String::new();
              } else {
                buffer.push(c);
              }
            }
          }
        } else if !is_whitespace(c) {
          match c {
            parser_consts::COMMENT_START_CHAR => consume_comment(&mut parser_state, &mut code)?,
            parser_consts::STRING_LIMITER_CHAR => {
              let str_token = consume_string(&mut parser_state, &mut code)?;
              add_token(&mut parser_state, str_token);
            }
            parser_consts::SYSTEM_VALUE => {
              let str_token = consume_system_value(&mut parser_state, &mut code)?;
              add_token(&mut parser_state, str_token);
            }
            parser_consts::COMMAND_START => {
              parser_state.block_manager.start_block();
              parser_state
                .blocks
                .push(ParserBlockToken::Command(parser_state.result.len()));
            }
            parser_consts::LIST_START => {
              parser_state.block_manager.start_block();
              parser_state.blocks.push(ParserBlockToken::List {
                size: parser_state.result.len(),
                start_line: parser_state.start_line,
                start_col: parser_state.start_col,
              });
            }
            parser_consts::DICTIONARY_START => {
              parser_state.block_manager.start_block();
              parser_state.blocks.push(ParserBlockToken::Dictionary {
                size: parser_state.result.len(),
                start_line: parser_state.start_line,
                start_col: parser_state.start_col,
              });
            }
            parser_consts::COMMAND_END => process_command_end(&mut parser_state)?,
            parser_consts::LIST_END => process_list_end(&mut parser_state)?,
            parser_consts::DICTIONARY_END => process_dictionary_end(&mut parser_state)?,
            _ => {
              is_reading = true;
              parser_state.start_line = code.line;
              parser_state.start_col = code.col;
              buffer.push(c);
            }
          }
        }
      }
      None => break,
    }
  }
  if is_reading {
    parse_token(&mut parser_state, buffer)?;
  }
  if parser_state.blocks.len() > 0 {
    //Type dependant error
    return_error_line!("Open block definition.", parser_state)
  }
  let id_list = parser_state.block_manager.end_block();
  return Ok((Rc::new(parser_state.result), Rc::new(id_list)));
}
