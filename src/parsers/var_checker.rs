use crate::parsers::number_parser;
use crate::parsers::number_parser::ParsedNumber;
use crate::parsers::parser_consts;

pub fn get_prefix(var: &str) -> String {
  String::from(if var.starts_with(parser_consts::DEFINE_STR) {
    parser_consts::DEFINE_STR
  } else if var.starts_with(parser_consts::SET_REF_STR) {
    parser_consts::SET_REF_STR
  } else if var.starts_with(parser_consts::DECLARE_STR) {
    parser_consts::DECLARE_STR
  } else if var.starts_with(parser_consts::SET_STR) {
    parser_consts::SET_STR
  } else if var.starts_with(parser_consts::REF_STR) {
    parser_consts::REF_STR
  } else if var.starts_with(parser_consts::GET_REF_STR) {
    parser_consts::GET_REF_STR
  } else {
    ""
  })
}

// fn get_suffix(var: &str) -> &str {
//   if var.starts_with(parser_consts::COMMAND_START) {
//     parser_consts::COMMAND_START
//   } else {
//     ""
//   }
// }

pub fn get_scope_indicator(var: &str) -> String {
  String::from(if var.starts_with(parser_consts::GLOBAL_STR) {
    parser_consts::GLOBAL_STR
  } else if var.starts_with(parser_consts::LOCAL_STR) {
    parser_consts::LOCAL_STR
  } else {
    ""
  })
}

pub fn check_if_var_is_anonymous(var: &str) -> Option<u8> {
  match var {
    parser_consts::ANONYMOUS_VAR => return Some(parser_consts::ANONYMOUS_VAR_ID),
    parser_consts::ANONYMOUS_VAR_LIST => return Some(parser_consts::ANONYMOUS_VAR_LIST_ID),
    _ => {
      let mut chars = var.chars();
      if let Some(parser_consts::ANONYMOUS_N_VAR) = chars.next() {
        if let Some(n) = chars.next() {
          if let None = chars.next() {
            if n.is_ascii_digit() {
              return Some((n as u8) - ('0' as u8));
            }
          }
        }
      }
    }
  };
  return None;
}

pub fn check_if_var_is_valid(var: &str) -> Result<(), String> {
  if var.is_empty() {
    Err(String::from("A variable can't be an empty string."))
  } else if let Some(_) = check_if_var_is_anonymous(var) {//Maybe this check isn't needed
    Err(format!(
      "A variable ({}) can't be a valid anonymous variable.",
      var
    ))
  } else if var.chars().next().unwrap().is_ascii_digit() {
    Err(format!("A variable ({}) can't start with a digit.", var))
  } else if get_prefix(var) != "" {
    Err(format!(
      "A variable ({}) can't start with a prefix ({}).",
      var,
      get_prefix(var)
    ))
  } else if get_scope_indicator(var) != "" {
    Err(format!(
      "A variable ({}) can't start with a scope indicator ({}).",
      var,
      get_scope_indicator(var)
    ))
  } else {
    match number_parser::parse_number(&var) {
      Ok(ParsedNumber::InvalidFormat) => Ok(()),
      Ok(_) => Err(format!("A variable ({}) can't be a valid number.", var)),
      Err(err) => Err(err),
    }
  }
}
