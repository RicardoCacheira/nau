use crate::asl::ASL;
use crate::instance::Instance;
use crate::parsers::parser_consts;
use crate::scopes::ScopeId;
use crate::utils::base_utils;
use crate::utils::to_system_string::ToSystemString;
use num_rational::BigRational;
use std::rc::Rc;

#[derive(Clone)]
pub enum Token {
  Number(BigRational),
  Unsigned(u64),
  Signed(i64),
  Float(f64),
  String(Rc<String>),
  Raw(Rc<Vec<u8>>),
  Command {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
  },
  List {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    file: Rc<String>,
  },
  Dictionary {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    file: Rc<String>,
  },
  Declare(usize),
  Var {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  AnonymousVar {
    id: u8,
    file: Rc<String>,
  },
  Define {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  Set {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  Ref {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  GetRef {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  AnonymousGetRef {
    id: u8,
    file: Rc<String>,
  },
  SetRef {
    scope_id: ScopeId,
    file: Rc<String>,
  },
  FunctionCall {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    scope_id: ScopeId,
    file: Rc<String>,
  },
  RefFunctionCall {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    scope_id: ScopeId,
    file: Rc<String>,
  },
  AnonymousFunctionCall {
    id: u8,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    file: Rc<String>,
  },
  AnonymousRefFunctionCall {
    id: u8,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    file: Rc<String>,
  },
}

fn anonymous_id_to_string(id: &u8) -> String {
  match id {
    0..=9 => format!("{}{}", parser_consts::ANONYMOUS_N_VAR, id),
    &parser_consts::ANONYMOUS_VAR_ID => String::from(parser_consts::ANONYMOUS_VAR),
    &parser_consts::ANONYMOUS_VAR_LIST_ID => String::from(parser_consts::ANONYMOUS_VAR_LIST),
    _ => panic!("Impossible anonymous id reached"),
  }
}

impl PartialEq for Token {
  fn eq(&self, other: &Self) -> bool {
    return match (self, other) {
      (Token::Number(s), Token::Number(o)) => s == o,
      (Token::Unsigned(s), Token::Unsigned(o)) => s == o,
      (Token::Signed(s), Token::Signed(o)) => s == o,
      (Token::Float(s), Token::Float(o)) => s == o,
      (Token::String(s), Token::String(o)) => s == o,
      (Token::Raw(s), Token::Raw(o)) => s == o,
      (
        Token::Command {
          token_list: st,
          id_list: si,
        },
        Token::Command {
          token_list: ot,
          id_list: oi,
        },
      ) => st == ot && si == oi,
      (Token::Declare(s_id), Token::Declare(o_id)) => s_id == o_id,
      (Token::Var { scope_id: st, .. }, Token::Var { scope_id: ot, .. }) => st == ot,
      (Token::AnonymousVar { id: si, .. }, Token::AnonymousVar { id: oi, .. }) => si == oi,
      (Token::Define { scope_id: st, .. }, Token::Define { scope_id: ot, .. }) => st == ot,
      (Token::Set { scope_id: st, .. }, Token::Set { scope_id: ot, .. }) => st == ot,
      (Token::Ref { scope_id: st, .. }, Token::Ref { scope_id: ot, .. }) => st == ot,
      (Token::GetRef { scope_id: st, .. }, Token::GetRef { scope_id: ot, .. }) => st == ot,
      (Token::AnonymousGetRef { id: si, .. }, Token::AnonymousGetRef { id: oi, .. }) => si == oi,
      (Token::SetRef { scope_id: st, .. }, Token::SetRef { scope_id: ot, .. }) => st == ot,
      (
        Token::FunctionCall {
          token_list: st,
          id_list: si,
          scope_id: stt,
          ..
        },
        Token::FunctionCall {
          token_list: ot,
          id_list: oi,
          scope_id: ott,
          ..
        },
      ) => st == ot && si == oi && stt == ott,
      (
        Token::AnonymousFunctionCall {
          id: si,
          id_list: sil,
          token_list: st,
          ..
        },
        Token::AnonymousFunctionCall {
          id: oi,
          id_list: oil,
          token_list: ot,
          ..
        },
      ) => si == oi && st == ot && sil == oil,
      (
        Token::RefFunctionCall {
          token_list: st,
          id_list: si,
          scope_id: stt,
          ..
        },
        Token::RefFunctionCall {
          token_list: ot,
          id_list: oi,
          scope_id: ott,
          ..
        },
      ) => st == ot && si == oi && stt == ott,
      (
        Token::AnonymousRefFunctionCall {
          id: si,
          id_list: sil,
          token_list: st,
          ..
        },
        Token::AnonymousRefFunctionCall {
          id: oi,
          id_list: oil,
          token_list: ot,
          ..
        },
      ) => si == oi && st == ot && sil == oil,
      (_, _) => false,
    };
  }
}

impl ToSystemString for Token {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    match self {
      Token::Number(value) => value.to_string(),
      Token::Unsigned(value) => format!("{}u", value),
      Token::Signed(value) => format!("{}n", value),
      Token::Float(value) => format!("{}f", value),
      Token::String(value) => format!(
        "\"{}\"",
        value
          .replace("\\", "\\\\")
          .replace("\n", "\\n")
          .replace("\r", "\\r")
          .replace("\t", "\\t")
          .replace("\"", "\\\"")
      ),
      Token::Raw(value) => format!("| {} |", base_utils::vec_8u_to_string(value)),
      Token::Command { token_list, .. } => format!(
        "{} {} {}",
        parser_consts::COMMAND_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, " "),
        parser_consts::COMMAND_END
      ),
      Token::List { token_list, .. } => format!(
        "{} {} {}",
        parser_consts::LIST_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::LIST_END
      ),
      Token::Dictionary { token_list, .. } => format!(
        "{} {} {}",
        parser_consts::DICTIONARY_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::DICTIONARY_END
      ),
      Token::Declare(id) => format!(
        "{}{}",
        parser_consts::DECLARE_STR,
        instance.get_block_id_manager().id_to_name(id)
      ),
      Token::Var { scope_id, .. } => scope_id.to_string(instance, asl),
      Token::AnonymousVar { id, .. } => format!("{}", anonymous_id_to_string(id)),
      Token::Define { scope_id, .. } => format!(
        "{}{}",
        parser_consts::DEFINE_STR,
        scope_id.to_string(instance, asl),
      ),
      Token::Set { scope_id, .. } => format!(
        "{}{}",
        parser_consts::SET_STR,
        scope_id.to_string(instance, asl),
      ),
      Token::Ref { scope_id, .. } => format!(
        "{}{}",
        parser_consts::REF_STR,
        scope_id.to_string(instance, asl),
      ),
      Token::GetRef { scope_id, .. } => format!(
        "{}{}",
        parser_consts::GET_REF_STR,
        scope_id.to_string(instance, asl),
      ),
      Token::AnonymousGetRef { id, .. } => format!(
        "{}{}",
        parser_consts::GET_REF_STR,
        anonymous_id_to_string(id)
      ),
      Token::SetRef { scope_id, .. } => format!(
        "{}{}",
        parser_consts::SET_REF_STR,
        scope_id.to_string(instance, asl),
      ),
      Token::FunctionCall {
        token_list,
        scope_id,
        ..
      } => format!(
        "{}{} {} {}",
        scope_id.to_string(instance, asl),
        parser_consts::COMMAND_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::COMMAND_END
      ),
      Token::AnonymousFunctionCall { id, token_list, .. } => format!(
        "{}{} {} {}",
        anonymous_id_to_string(id),
        parser_consts::COMMAND_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::COMMAND_END
      ),
      Token::RefFunctionCall {
        token_list,
        scope_id,
        ..
      } => format!(
        "{}{}{} {} {}",
        parser_consts::GET_REF_STR,
        scope_id.to_string(instance, asl),
        parser_consts::COMMAND_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::COMMAND_END
      ),
      Token::AnonymousRefFunctionCall { id, token_list, .. } => format!(
        "{}{}{} {} {}",
        parser_consts::GET_REF_STR,
        anonymous_id_to_string(id),
        parser_consts::COMMAND_START,
        base_utils::sys_str_vec_to_str(instance, asl, token_list, ", "),
        parser_consts::COMMAND_END
      ),
    }
  }
}

impl Token {
  pub fn to_simple_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    match self {
      Token::FunctionCall { scope_id, .. } => format!(
        "{}{} {}",
        scope_id.to_string(instance, asl),
        parser_consts::COMMAND_START,
        parser_consts::COMMAND_END
      ),
      Token::AnonymousFunctionCall { id, .. } => format!(
        "{}{} {}",
        anonymous_id_to_string(id),
        parser_consts::COMMAND_START,
        parser_consts::COMMAND_END
      ),
      Token::RefFunctionCall { scope_id, .. } => format!(
        "{}{}{} {}",
        parser_consts::GET_REF_STR,
        scope_id.to_string(instance, asl),
        parser_consts::COMMAND_START,
        parser_consts::COMMAND_END
      ),
      Token::AnonymousRefFunctionCall { id, .. } => format!(
        "{}{}{} {}",
        parser_consts::GET_REF_STR,
        anonymous_id_to_string(id),
        parser_consts::COMMAND_START,
        parser_consts::COMMAND_END
      ),
      _ => self.to_string(instance, asl),
    }
  }
}
