pub mod nau_parser;
pub mod number_parser;
pub mod parser_consts;
pub mod token;
pub mod var_checker;
pub use token::Token;
#[cfg(test)]
mod tests;
