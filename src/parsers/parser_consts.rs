pub const ESCAPE_CHAR: char = 27 as char;
pub const BACKSPACE_CHAR: char = 8 as char;
//Prefix strings
pub const DEFINE_STR: &str = "::";
pub const SET_STR: &str = "$";
pub const REF_STR: &str = "&";
pub const GET_REF_STR: &str = "@";
pub const SET_REF_STR: &str = "$@";
pub const DECLARE_STR: &str = "->";
//Scope indicators
pub const GLOBAL_STR: &str = "'";
pub const LOCAL_STR: &str = "_";
//Parser chars
pub const STRING_LIMITER_CHAR: char = '"';
pub const COMMENT_START_CHAR: char = '#';
pub const COMMENT_BLOCK_CHAR: char = '-';
pub const COMMAND_START: char = '(';
pub const COMMAND_END: char = ')';
pub const LIST_START: char = '[';
pub const LIST_END: char = ']';
pub const DICTIONARY_START: char = '{';
pub const DICTIONARY_END: char = '}';
//System value chars
pub const SYSTEM_VALUE: char = '%';
pub const RAW_CHAR: char = '|';
//Anonymous vars
pub const ANONYMOUS_N_VAR: char = '?';
pub const ANONYMOUS_VAR: &str = "?";
pub const ANONYMOUS_VAR_LIST: &str = "??";
pub const ANONYMOUS_VAR_ID: u8 = 10;
pub const ANONYMOUS_VAR_LIST_ID: u8 = 11;
