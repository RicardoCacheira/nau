use super::super::number_parser::*;
use num_bigint::BigInt;
use num_rational::BigRational;

#[test]
fn number_parser_test01() {
  assert!(
    parse_number("123").unwrap()
      == ParsedNumber::Rational(BigRational::from("123".parse::<BigInt>().unwrap()))
  );
}
#[test]
fn number_parser_test02() {
  let n = "12345".parse::<BigInt>().unwrap();
  let d = "100".parse::<BigInt>().unwrap();
  assert!(parse_number("123.45").unwrap() == ParsedNumber::Rational(BigRational::from((n, d))));
}
#[test]
fn number_parser_test03() {
  assert!(
    parse_number("120/10").unwrap()
      == ParsedNumber::Rational(BigRational::from("12".parse::<BigInt>().unwrap()))
  );
}
#[test]
fn number_parser_test04() {
  assert!(parse_number("123.").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test05() {
  let n = "45".parse::<BigInt>().unwrap();
  let d = "100".parse::<BigInt>().unwrap();
  assert!(parse_number(".45").unwrap() == ParsedNumber::Rational(BigRational::from((n, d))));
}
#[test]
fn number_parser_test06() {
  assert!(parse_number("123/").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test07() {
  assert!(parse_number("/45").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test08() {
  assert!(
    parse_number("-123").unwrap()
      == ParsedNumber::Rational(BigRational::from("-123".parse::<BigInt>().unwrap()))
  );
}
#[test]
fn number_parser_test09() {
  let n = "-12345".parse::<BigInt>().unwrap();
  let d = "100".parse::<BigInt>().unwrap();
  assert!(parse_number("-123.45").unwrap() == ParsedNumber::Rational(BigRational::from((n, d))));
}
#[test]
fn number_parser_test10() {
  assert!(
    parse_number("-120/10").unwrap()
      == ParsedNumber::Rational(BigRational::from("-12".parse::<BigInt>().unwrap()))
  );
}
#[test]
fn number_parser_test11() {
  assert!(parse_number("-123.").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test12() {
  let n = "-45".parse::<BigInt>().unwrap();
  let d = "100".parse::<BigInt>().unwrap();
  assert!(parse_number("-.45").unwrap() == ParsedNumber::Rational(BigRational::from((n, d))));
}
#[test]
fn number_parser_test13() {
  assert!(parse_number("-123/").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test14() {
  assert!(parse_number("-/45").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test15() {
  assert!(parse_number("123u").unwrap() == ParsedNumber::Unsigned(123));
}
#[test]
fn number_parser_test16() {
  assert!(parse_number("123.45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test17() {
  assert!(parse_number("123/45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test18() {
  assert!(parse_number("123.u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test19() {
  assert!(parse_number(".45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test20() {
  assert!(parse_number("123/u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test21() {
  assert!(parse_number("/45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test22() {
  assert!(parse_number("-123u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test23() {
  assert!(parse_number("-123.45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test24() {
  assert!(parse_number("-123/45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test25() {
  assert!(parse_number("-123.u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test26() {
  assert!(parse_number("-.45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test27() {
  assert!(parse_number("-123/u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test28() {
  assert!(parse_number("-/45u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test29() {
  assert!(parse_number("123s").unwrap() == ParsedNumber::Signed(123));
}
#[test]
fn number_parser_test30() {
  assert!(parse_number("123.45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test31() {
  assert!(parse_number("123/45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test32() {
  assert!(parse_number("123.s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test33() {
  assert!(parse_number(".45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test34() {
  assert!(parse_number("123/s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test35() {
  assert!(parse_number("/45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test36() {
  assert!(parse_number("-123s").unwrap() == ParsedNumber::Signed(-123));
}
#[test]
fn number_parser_test37() {
  assert!(parse_number("-123.45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test38() {
  assert!(parse_number("-123/45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test39() {
  assert!(parse_number("-123.s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test40() {
  assert!(parse_number("-.45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test41() {
  assert!(parse_number("-123/s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test42() {
  assert!(parse_number("-/45s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test43() {
  assert!(parse_number("123f").unwrap() == ParsedNumber::Float(123.0));
}
#[test]
fn number_parser_test44() {
  assert!(parse_number("123.45f").unwrap() == ParsedNumber::Float(123.45));
}
#[test]
fn number_parser_test45() {
  assert!(parse_number("120/10f").unwrap() == ParsedNumber::Float(12.0));
}
#[test]
fn number_parser_test46() {
  assert!(parse_number("123.f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test47() {
  assert!(parse_number(".45f").unwrap() == ParsedNumber::Float(0.45));
}
#[test]
fn number_parser_test48() {
  assert!(parse_number("123/f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test49() {
  assert!(parse_number("/45f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test50() {
  assert!(parse_number("-123f").unwrap() == ParsedNumber::Float(-123.0));
}
#[test]
fn number_parser_test51() {
  assert!(parse_number("-123.45f").unwrap() == ParsedNumber::Float(-123.45));
}
#[test]
fn number_parser_test52() {
  assert!(parse_number("-120/10f").unwrap() == ParsedNumber::Float(-12.0));
}
#[test]
fn number_parser_test53() {
  assert!(parse_number("-123.f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test54() {
  assert!(parse_number("-.45f").unwrap() == ParsedNumber::Float(-0.45));
}
#[test]
fn number_parser_test55() {
  assert!(parse_number("-123/f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test56() {
  assert!(parse_number("-/45f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test57() {
  assert!(parse_number("").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test58() {
  assert!(parse_number("u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test59() {
  assert!(parse_number("s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test60() {
  assert!(parse_number("f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test61() {
  assert!(parse_number(".").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test62() {
  assert!(parse_number(".u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test63() {
  assert!(parse_number(".s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test64() {
  assert!(parse_number(".f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test65() {
  assert!(parse_number("u.").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test66() {
  assert!(parse_number("n.").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test67() {
  assert!(parse_number("f.").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test68() {
  assert!(parse_number("/").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test69() {
  assert!(parse_number("/u").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test70() {
  assert!(parse_number("/s").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test71() {
  assert!(parse_number("/f").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test72() {
  assert!(parse_number("u/").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test73() {
  assert!(parse_number("n/").unwrap() == ParsedNumber::InvalidFormat);
}
#[test]
fn number_parser_test74() {
  assert!(parse_number("f/").unwrap() == ParsedNumber::InvalidFormat);
}
