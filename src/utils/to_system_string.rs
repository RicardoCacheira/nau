use crate::asl::ASL;
use crate::instance::Instance;

pub trait ToSystemString {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String;
}
