use crate::values::Value;
use num_bigint::BigInt;
use num_rational::BigRational;
use num_traits::cast::ToPrimitive;
use num_traits::sign::Signed;
use std::convert::TryInto;

pub fn rational_to_usize(br: &BigRational) -> Option<usize> {
  br.to_integer().to_usize()
}

pub fn rational_to_u64(br: &BigRational) -> Option<u64> {
  br.to_integer().to_u64()
}

pub fn rational_to_isize(br: &BigRational) -> Option<isize> {
  br.to_integer().to_isize()
}

pub fn rational_to_i64(br: &BigRational) -> Option<i64> {
  br.to_integer().to_i64()
}

pub fn usize_to_rational(u: usize) -> BigRational {
  BigRational::from(BigInt::from(u))
}

pub fn u64_to_rational(u: u64) -> BigRational {
  BigRational::from(BigInt::from(u))
}

pub fn i64_to_rational(i: i64) -> BigRational {
  BigRational::from_integer(BigInt::from(i))
}

pub fn f64_to_rational(f: f64) -> Option<BigRational> {
  BigRational::from_float(f)
}

pub fn integer_to_float(i: i64) -> f64 {
  i as f64
}

pub fn get_max_unsigned() -> BigRational {
  BigRational::from(BigInt::from(usize::MAX))
}

fn pos_val_to_usize(len: usize, pos: &Value) -> Option<usize> {
  Some(match pos {
    Value::Unsigned(u) => {
      if let Ok(value) = u.clone().try_into() {
        value
      } else {
        return None;
      }
    }
    Value::Signed(s) => {
      if s < &0 {
        let s_abs = (0 - s) as usize;
        if s_abs > len {
          return None;
        }
        len - s_abs
      } else {
        s.clone() as usize
      }
    }
    Value::Number(r) => {
      if let Some(mut pos_u) = rational_to_usize(&r.abs()) {
        if r.is_negative() {
          if pos_u > len {
            return None;
          }
          pos_u = len - pos_u;
        };
        pos_u
      } else {
        return None;
      }
    }
    Value::Float(f) => {
      if f < &0.0 {
        let f_abs = (0.0 - f) as usize;
        if f_abs > len {
          return None;
        }
        len - f_abs
      } else {
        f.clone() as usize
      }
    }
    _ => return None,
  })
}

fn get_pos_len(len: usize, pos: &Value) -> Option<usize> {
  if let Some(pos_final) = pos_val_to_usize(len, pos) {
    if pos_final < len {
      return Some(pos_final);
    }
  }
  None
}

fn get_pos_inclusive_len(len: usize, pos: &Value) -> Option<usize> {
  if let Some(pos_final) = pos_val_to_usize(len, pos) {
    if pos_final <= len {
      return Some(pos_final);
    }
  }
  None
}

pub fn get_unsigned(num: &Value) -> Option<usize> {
  pos_val_to_usize(0, num)
}

pub fn get_pos<T>(vec: &Vec<T>, pos: &Value) -> Option<usize> {
  get_pos_len(vec.len(), pos)
}

pub fn get_pos_str(s: &String, pos: &Value) -> Option<usize> {
  get_pos_len(s.chars().count(), pos)
}

pub fn get_pos_inclusive<T>(vec: &Vec<T>, pos: &Value) -> Option<usize> {
  get_pos_inclusive_len(vec.len(), pos)
}

pub fn get_pos_inclusive_str(s: &String, pos: &Value) -> Option<usize> {
  get_pos_inclusive_len(s.chars().count(), pos)
}

pub fn value_to_u64(pos: &Value) -> Option<u64> {
  match pos {
    Value::Unsigned(u) => Some(u.clone()),
    Value::Signed(s) => {
      if s < &0 {
        None
      } else {
        Some(s.clone() as u64)
      }
    }
    Value::Number(r) => {
      if r.is_negative() {
        None
      } else {
        rational_to_u64(&r.abs())
      }
    }
    Value::Float(f) => {
      if f < &0.0 {
        None
      } else {
        Some(f.clone() as u64)
      }
    }
    _ => return None,
  }
}
