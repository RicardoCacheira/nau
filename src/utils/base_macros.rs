#[macro_export]
macro_rules! rc_vec {
  ($($types:expr),* $(,)*) => {
      Rc::new(vec![$($types,)*])
  };
}

#[macro_export]
macro_rules! rc_str_f {
  ($string:expr) => {
    Rc::new(String::from($string))
  };
}

#[macro_export]
macro_rules! rc_str_n {
  ($string:expr) => {
    Rc::new(String::new($string))
  };
}
