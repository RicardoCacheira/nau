pub struct RNG {
  x_state: u64,
  y_state: u64,
  z_state: u64,
}

fn rotate_left(value: u64, rotation: u8) -> u64 {
  (value << rotation) | (value >> (64 - rotation))
}

impl RNG {
  pub fn new(seed: u64) -> Self {
    let mut rgn = RNG {
      x_state: 0,
      y_state: 0,
      z_state: 0,
    };
    rgn.seed(seed);
    rgn
  }

  pub fn seed(&mut self, mut seed: u64) {
    if seed == 0 {
      seed = 1;
    }
    self.x_state = seed;
    self.y_state = seed;
    self.z_state = seed;
    for _ in 0..32 {
      self.generate();
    }
  }

  pub fn generate(&mut self) -> u64 {
    let xp = self.x_state;
    let yp = self.y_state;
    let zp = self.z_state;
    self.x_state = 15241094284759029579_u64 * zp;
    self.y_state = yp - xp;
    self.y_state = rotate_left(self.y_state, 12);
    self.z_state = zp - yp;
    self.z_state = rotate_left(self.z_state, 44);
    return xp;
  }

  pub fn generate_to_max(&mut self, mut exclusive_max: u64) -> u64 {
    let mut mask = !0_u64;
    exclusive_max -= 1;
    mask >>= (exclusive_max | 1).leading_zeros();
    let mut result = self.generate() & mask;
    while result > exclusive_max {
      result = self.generate() & mask;
    }
    result
  }

  pub fn generate_between(&mut self, min: u64, max: u64) -> u64 {
    if min > max {
      panic!("min > max on generate_between");
    }
    return self.generate_to_max(max - min) + min;
  }

  pub fn generate_char(&mut self) -> char {
    let value = self.generate_to_max(62);
    ((match value {
      0..=9 => ('0' as u64),
      10..=35 => ('A' as u64) - 10,
      36..=61 => ('a' as u64) - 36,
      _ => panic!("impossible value"),
    } + value) as u8) as char
  }

  pub fn generate_alfa(&mut self, size: usize) -> String {
    let mut result = String::new();
    for _ in 0..size {
      result.push(self.generate_char());
    }
    result
  }
}
