use crate::asl::ASL;
use crate::instance::Instance;
use crate::parsers::parser_consts::{BACKSPACE_CHAR, ESCAPE_CHAR, STRING_LIMITER_CHAR};
use crate::utils::to_system_string::ToSystemString;
use crate::values::Value;
use std::env;
use std::string::ToString;

pub fn vec_to_str<T: ToString>(v: &Vec<T>, sep: &str) -> String {
    if v.len() > 0 {
        let mut iter = v.iter();
        let result = iter.next().unwrap().to_string();
        iter.fold(result, |acc, value| acc + sep + &value.to_string())
    } else {
        String::new()
    }
}

//TODO Make a better name
pub fn sys_str_vec_to_str<T: ToSystemString>(
    instance: &mut Instance,
    asl: &ASL,
    vec: &Vec<T>,
    sep: &str,
) -> String {
    if vec.len() > 0 {
        let mut iter = vec.iter();
        let result = iter.next().unwrap().to_string(instance, asl);
        iter.fold(result, |acc, value| {
            acc + sep + &value.to_string(instance, asl)
        })
    } else {
        String::new()
    }
}

//TODO this is ever worse
//this actually means ToSystemString reference vector to String
//names are hard
pub fn sys_str_ref_vec_to_str<T: ToSystemString>(
    instance: &mut Instance,
    asl: &ASL,
    vec: &Vec<&T>,
    sep: &str,
) -> String {
    if vec.len() > 0 {
        let mut iter = vec.iter();
        let result = iter.next().unwrap().to_string(instance, asl);
        iter.fold(result, |acc, value| {
            acc + sep + &value.to_string(instance, asl)
        })
    } else {
        String::new()
    }
}

pub fn is_debug() -> bool {
    match env::var("NAU_DEBUG") {
        Ok(val) => val.to_lowercase() == "true" || val == "1",
        Err(_) => false,
    }
}

pub fn to_token_string(s: &String) -> String {
    return format!(
        "{}{}{}",
        STRING_LIMITER_CHAR,
        s.clone()
            .replace("\\", "\\\\")
            .replace(ESCAPE_CHAR, "\\e")
            .replace(BACKSPACE_CHAR, "\\b")
            .replace("\n", "\\n")
            .replace("\t", "\\t")
            .replace("\r", "\\r")
            .replace(STRING_LIMITER_CHAR, &format!("\\{}", STRING_LIMITER_CHAR)),
        STRING_LIMITER_CHAR
    );
}

pub fn value_to_anoted_value_string(instance: &mut Instance, asl: &ASL, value: &Value) -> String {
    format!(
        "<{}:{}>",
        value.get_value_type().to_string(instance, asl),
        value.to_token_string(instance, asl)
    )
}

pub fn num_to_param_str(mut num: usize) -> String {
    let mut result = String::new();
    while num > 26 {
        let modulo = num % 26;
        result.push(('a' as usize + modulo) as u8 as char);
        num = (num - modulo) / 26;
    }
    let modulo = num % 26;
    result.push(('a' as usize + modulo) as u8 as char);
    result.chars().rev().collect::<String>()
}

pub fn vec_8u_to_string(vec: &Vec<u8>) -> String {
    if vec.len() > 0 {
        let mut iter = vec.iter();
        let result = format!("{:02X}", iter.next().unwrap());
        iter.fold(result, |acc, value| acc + " " + &format!("{:02X}", value))
    } else {
        String::new()
    }
}

pub fn vec_8u_to_u8_8_array(vec: &Vec<u8>) -> [u8; 8] {
    let mut bytes = [0, 0, 0, 0, 0, 0, 0, 0];
    let len = if vec.len() < 8 { vec.len() } else { 8 };
    for i in 0..len {
        bytes[i] = vec[i].clone();
    }
    bytes
}
