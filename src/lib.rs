pub mod command;
pub mod instance;
pub mod native_functions;
pub mod parsers;
pub mod scopes;
pub mod utils;
pub mod values;

#[path = "system/common/mod.rs"]
pub mod asl_common;

#[cfg(any(target_os = "macos", target_os = "linux"))]
#[path = "system/unix/mod.rs"]
pub mod asl;

#[cfg(windows)]
#[path = "system/win/mod.rs"]
pub mod asl;

#[cfg(not(any(windows, target_os = "macos", target_os = "linux")))]
#[path = "system/basic/mod.rs"]
pub mod asl;
