use crate::asl::ASL;
use crate::instance::Instance;
use crate::parsers::Token;
use crate::scopes::ScopeId;
use crate::utils::base_utils;
use crate::utils::to_system_string::ToSystemString;
use crate::values::{FunctionType, SystemType, Value, ValueMap};
use std::rc::Rc;

macro_rules! return_error_line {
  ($err:expr,$file:expr) => {
    return TokenExecReturn::Error(format!("{}\non {}", $err, $file));
  };
}

macro_rules! return_token_error_line {
  ($err:expr,$token:expr,$file:expr) => {
    return TokenExecReturn::Error(format!("{}\nat `{}`, on {}", $err, $token, $file));
  };
}

macro_rules! return_error_string {
  ($err:expr) => {
    return CommandExecReturn::Error($err);
  };
}

macro_rules! return_error_str {
  ($err:expr) => {
    return_error_string!(String::from($err))
  };
}

enum InstanceCallType {
  SetCall(ScopeId),
  SetRefCall(ScopeId),
}

struct InstanceCall {
  file: Rc<String>,
  stack_pos: usize,
  call_type: InstanceCallType,
}

enum SystemFunction<'a, 'b> {
  Nothing,
  PushToStack(Value),
  AddCheckAndRunCall(&'a mut Instance, &'b ASL, Value, Vec<Value>, Rc<String>),
  AddMetaCall(
    &'a mut Instance,
    &'b ASL,
    Value,
    Rc<Vec<usize>>,
    Rc<Vec<Token>>,
    Rc<String>,
  ),
  AddSetCall(ScopeId, Rc<String>),
  AddSetRefCall(ScopeId, Rc<String>),
}

enum TokenExecReturn {
  Continue,
  End, //Maybe make this have the commandExecReturn to be processed i9nstead of gettinh the "message"
  Error(String),
}

pub enum CommandExecReturn {
  Return(Value),     //tells the command to stop and return the given value
  End,               //tells the command to stop and return nothing
  Stack(Vec<Value>), //the command stack had a some values
  NoReturn,          //there was nothing in the command stack or the function returned nothing
  Loop(bool), //Stops the loops command execution an tells if the loop should continue or break
  Error(String),
  Exit(u64), //Tells the instance that the execution should stop and send the given value as the exit code
}

pub struct CommandExec {
  token_list: Rc<Vec<Token>>,
  pos: usize,
  stack: Vec<Value>,
  calls: Vec<InstanceCall>,
  return_to_pass: Option<CommandExecReturn>,
}

impl CommandExec {
  fn new(token_list: Rc<Vec<Token>>) -> CommandExec {
    CommandExec {
      token_list,
      pos: 0,
      stack: Vec::new(),
      calls: Vec::new(),
      return_to_pass: None,
    }
  }

  pub fn run_command(
    instance: &mut Instance,
    asl: &ASL,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
  ) -> CommandExecReturn {
    if token_list.len() > 0 {
      let created_scope = instance.clear_scope_variables(id_list.as_ref());
      let level = CommandExec::new(token_list);
      let level_return = level.run(instance, asl);
      instance.reload_scope_variables(created_scope);
      return level_return;
    }
    return CommandExecReturn::NoReturn;
  }

  pub fn run_persistant_command(
    instance: &mut Instance,
    asl: &ASL,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
  ) -> CommandExecReturn {
    if token_list.len() > 0 {
      instance.clear_scope_variables(id_list.as_ref());
      let level = CommandExec::new(token_list);
      let level_return = level.run(instance, asl);
      return level_return;
    }
    return CommandExecReturn::NoReturn;
  }

  pub fn run_command_with_named_params(
    instance: &mut Instance,
    asl: &ASL,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    param_ids: Vec<usize>,
    param_values: Vec<Value>,
  ) -> CommandExecReturn {
    if token_list.len() > 0 {
      let created_scope = instance.clear_scope_variables(&param_ids);
      for (i, value) in param_values.into_iter().enumerate() {
        instance.set_block_var(&param_ids[i], value);
      }
      let result = CommandExec::run_command(instance, asl, token_list, id_list);
      instance.reload_scope_variables(created_scope);
      return result;
    }
    return CommandExecReturn::NoReturn;
  }

  pub fn run_command_with_params(
    instance: &mut Instance,
    asl: &ASL,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    param_values: Vec<Value>,
  ) -> CommandExecReturn {
    if token_list.len() > 0 {
      let previous_values = instance.set_anonymous_variables(param_values);
      let result = CommandExec::run_command(instance, asl, token_list, id_list);
      instance.set_anonymous_variables(previous_values);
      return result;
    }
    return CommandExecReturn::NoReturn;
  }

  fn run_command_function(
    instance: &mut Instance,
    asl: &ASL,
    func: Value,
    mut param_values: Vec<Value>,
  ) -> CommandExecReturn {
    if let Value::Function {
      function_type:
        FunctionType::Command {
          com_token_list,
          com_id_list,
          start_value_list,
          all_scopes_list,
          ..
        },
      ..
    } = func
    {
      if start_value_list.len() > 0 {
        for value in start_value_list.iter() {
          param_values.push(value.clone());
        }
      }
      return CommandExec::run_command_with_named_params(
        instance,
        asl,
        com_token_list,
        com_id_list,
        Vec::clone(&all_scopes_list),
        param_values,
      );
    } else {
      return_error_str!("Given value is not a command function.");
    }
  }

  fn set_return_to_pass(&mut self, return_to_pass: CommandExecReturn) {
    self.return_to_pass = Some(return_to_pass);
    self.pos = self.token_list.len();
  }

  fn has_call(&self) -> bool {
    if let Some(_) = self.calls.last() {
      return true;
    }
    return false;
  }

  fn has_possible_call(&self) -> bool {
    match self.calls.last() {
      Some(call) => {
        return self.stack.len() - call.stack_pos >= 1;
      }
      None => return false,
    }
  }

  fn push_to_stack(&mut self, value: Value) {
    self.stack.push(value);
  }

  fn get_last_token(&self) -> Option<&Token> {
    self.token_list.get(self.pos - 1)
  }

  fn get_curr_token(&self) -> Option<&Token> {
    self.token_list.get(self.pos)
  }

  fn add_set_call(&mut self, scope_id: ScopeId, file: Rc<String>) {
    self.calls.push(InstanceCall {
      file,
      stack_pos: self.stack.len(),
      call_type: InstanceCallType::SetCall(scope_id),
    });
  }

  fn add_set_ref_call(&mut self, ref_value: ScopeId, file: Rc<String>) {
    self.calls.push(InstanceCall {
      file,
      stack_pos: self.stack.len(),
      call_type: InstanceCallType::SetRefCall(ref_value),
    });
  }

  fn run_last_call(&mut self, instance: &mut Instance) {
    let call = self.calls.pop().unwrap();
    //Isto esta mal, ele tem que retirar o valor da frente, nao o ultimo
    //ou sera que e um bug que passa a ser uma feature?
    let stack_value = self.stack.pop().unwrap();
    return match call.call_type {
      InstanceCallType::SetCall(scope_id) => instance.set_var(&scope_id, stack_value),
      InstanceCallType::SetRefCall(ref_value) => {
        instance.set_var_by_ref(&ref_value, stack_value);
      }
    };
  }

  fn inc_token_pos(&mut self) {
    self.pos += 1;
  }

  fn get_possible_sequence_of_values(
    instance: &mut Instance,
    asl: &ASL,
    type_list: &Vec<SystemType>,
    value_list: &Vec<Value>,
    previous_values: &Vec<Value>,
    start: usize,
  ) -> usize {
    let len = type_list.len();
    //If not enougth values exist
    if len > value_list.len() - start {
      return start;
    }
    for (i, t) in type_list.iter().enumerate() {
      if !t.is_type_of_with_previous(instance, asl, &value_list[start + i], previous_values) {
        return start;
      }
    }
    return start + len;
  }

  fn get_possible_multiple_sequence_of_values(
    instance: &mut Instance,
    asl: &ASL,
    type_list: &Vec<SystemType>,
    value_list: &Vec<Value>,
    previous_values: &Vec<Value>,
    start: usize,
  ) -> usize {
    let mut end = start;
    let mut aux = CommandExec::get_possible_sequence_of_values(
      instance,
      asl,
      type_list,
      value_list,
      previous_values,
      end,
    );
    while aux != end {
      end = aux;
      aux = CommandExec::get_possible_sequence_of_values(
        instance,
        asl,
        type_list,
        value_list,
        previous_values,
        end,
      );
    }
    return end;
  }

  fn check_and_return_func_params(
    instance: &mut Instance,
    asl: &ASL,
    param_list: &Vec<SystemType>,
    param_values: &Vec<Value>,
  ) -> Result<Vec<Value>, String> {
    let mut res = Vec::new();
    let mut value_pos = 0;
    let param_values_len = param_values.len();

    for i in 0..param_list.len() {
      let t = &param_list[i];
      if let SystemType::Multiple(types) | SystemType::Many(types) = t {
        let multiple_slice_end = CommandExec::get_possible_multiple_sequence_of_values(
          instance,
          asl,
          types,
          param_values,
          &res,
          value_pos,
        );
        if multiple_slice_end == value_pos {
          if let SystemType::Many(_) = t {
            return Err(format!(
              "No values found for the <{}> type.",
              t.to_string(instance, asl)
            ));
          }
        }
        res.push(Value::List(Rc::new(
          param_values[value_pos..multiple_slice_end].to_vec(),
        )));
        value_pos = multiple_slice_end;
      } else if param_values_len == value_pos {
        return Err(format!(
          "Not enough values for the given parameter list.\nMissing: {}",
          base_utils::sys_str_vec_to_str(instance, asl, &param_list[i..].to_vec(), ", ")
        ));
      } else if t.is_type_of_with_previous(instance, asl, &param_values[value_pos], &res) {
        res.push(param_values[value_pos].clone());
        value_pos += 1;
      } else {
        return Err(format!(
          "Value {} is not of the type <{}>.",
          base_utils::value_to_anoted_value_string(instance, asl, &param_values[value_pos]),
          t.to_string(instance, asl)
        ));
      }
    }

    if param_values_len == value_pos {
      return Ok(res);
    } else {
      let mut extra_values = Vec::with_capacity(param_values_len - value_pos);
      for i in value_pos..param_values_len {
        extra_values.push(base_utils::value_to_anoted_value_string(
          instance,
          asl,
          &param_values[i],
        ));
      }
      return Err(format!(
        "Too many values for the given param_list.\nExtra Value{}: {}",
        if extra_values.len() > 1 { "s" } else { "" },
        base_utils::vec_to_str(&extra_values, ", ")
      ));
    }
  }

  fn process_multiple_many_call_result(
    &mut self,
    instance: &mut Instance,
    asl: &ASL,
    type_list: &Vec<SystemType>,
    is_many_type: bool,
    value: &Value,
  ) -> Result<(), String> {
    if let Value::List(value_list) = value {
      if is_many_type {
        if value_list.len() == 0 {
          return Err(String::from(
            "Function returned a empty list for a function with a `Many` return type.",
          ));
        }
      }
      let mut aux_pos = 0;
      for val in value_list.iter() {
        if type_list[aux_pos].is_type_of(instance, asl, val) {
          self.push_to_stack(val.clone());
          aux_pos = (aux_pos + 1) % type_list.len();
        } else {
          return Err(format!(
            "Value at sequence position {} was of type `{}` instead of type `{}`.",
            aux_pos,
            val.get_value_type().to_string(instance, asl),
            type_list[aux_pos].to_string(instance, asl)
          ));
        }
      }
      return Ok(());
    } else {
      return Err(format!(
        "Function returned a value of type `{}` instead of a sequence of `{}`.",
        value.get_value_type().to_string(instance, asl),
        base_utils::sys_str_vec_to_str(instance, asl, type_list, ", ")
      ));
    }
  }

  fn process_single_call_result(
    &mut self,
    instance: &mut Instance,
    asl: &ASL,
    result_type: &SystemType,
    value: &Value,
  ) -> Result<(), String> {
    if result_type.is_type_of(instance, asl, &value) {
      self.push_to_stack(value.clone());
      return Ok(());
    } else {
      return Err(format!(
        "Return was of type `{}` instead of type `{}`.",
        value.get_value_type().to_string(instance, asl),
        result_type.to_string(instance, asl)
      ));
    }
  }

  fn process_function_call_result(
    &mut self,
    instance: &mut Instance,
    asl: &ASL,
    result_type: &SystemType,
    result: CommandExecReturn,
  ) -> Result<(), String> {
    match &result {
      CommandExecReturn::Error(err) => {
        return Err(err.clone());
      }
      CommandExecReturn::Exit(_) => {
        self.set_return_to_pass(result);
        return Ok(());
      }
      CommandExecReturn::Loop(_) => match result_type {
        SystemType::Void => {
          return Err(String::from(
            "Continue/Break request received on function with return type of `Void`.",
          ))
        }
        SystemType::Voidable(_) => {
          return Err(format!(
            "Continue/Break request received on function with return type of `{}`.",
            result_type.to_string(instance, asl)
          ))
        }
        SystemType::Permeable => {
          self.set_return_to_pass(result);
          return Ok(());
        }
        _ => {
          return Err(format!(
            "Continue/Break request received for the function with return type of `{}`.",
            result_type.to_string(instance, asl)
          ))
        }
      },
      CommandExecReturn::End => match result_type {
        SystemType::Void => return Ok(()),
        SystemType::Permeable => {
          self.set_return_to_pass(result);
          return Ok(());
        }
        SystemType::Voidable(_) => return Ok(()),
        _ => {
          return Err(format!(
            "End request received for the function with return type of `{}`.",
            result_type.to_string(instance, asl)
          ))
        }
      },
      CommandExecReturn::Stack(_) => match result_type {
        SystemType::Void => return Ok(()),
        SystemType::Permeable => return Ok(()),
        SystemType::Voidable(_) => return Ok(()),
        _ => {
          return Err(format!(
            "No return received for the function with return type of `{}`.",
            result_type.to_string(instance, asl)
          ))
        }
      },
      CommandExecReturn::NoReturn => match result_type {
        SystemType::Void => return Ok(()),
        SystemType::Permeable => return Ok(()),
        SystemType::Voidable(_) => return Ok(()),
        _ => {
          return Err(format!(
            "No return received for the function with return type of `{}`.",
            result_type.to_string(instance, asl)
          ))
        }
      },
      CommandExecReturn::Return(value) => match result_type {
        SystemType::Void => {
          return Err(String::from(
            "Return request received on function with return type of `Void`.",
          ))
        }
        SystemType::Permeable => {
          self.set_return_to_pass(result);
          return Ok(());
        }
        SystemType::Voidable(result_type) => {
          return self.process_single_call_result(instance, asl, result_type, value)
        }
        SystemType::Multiple(type_list) => {
          return self.process_multiple_many_call_result(instance, asl, type_list, false, value)
        }
        SystemType::Many(type_list) => {
          return self.process_multiple_many_call_result(instance, asl, type_list, true, value)
        }
        _ => return self.process_single_call_result(instance, asl, result_type, value),
      },
    }
  }

  fn process_command_call_result(&mut self, result: CommandExecReturn) -> Result<(), String> {
    match result {
      CommandExecReturn::Error(err) => {
        return Err(err);
      }
      CommandExecReturn::Exit(_) => {
        self.set_return_to_pass(result);
        return Ok(());
      }
      CommandExecReturn::NoReturn => return Ok(()),
      CommandExecReturn::Return(_) | CommandExecReturn::End | CommandExecReturn::Loop(_) => {
        self.set_return_to_pass(result);
        return Ok(());
      }
      //Pushes the last result in the end stack if it exists
      CommandExecReturn::Stack(mut stack) => {
        self.push_to_stack(stack.pop().unwrap()); //this always works because a stack return has to have at least one value

        return Ok(());
      }
    }
  }

  fn check_and_run_func(
    &mut self,
    instance: &mut Instance,
    asl: &ASL,
    func: Value,
    param_values: Vec<Value>,
  ) -> Result<(), String> {
    match func.clone() {
      Value::Function {
        param_list,
        return_type,
        function_type,
      } => {
        match CommandExec::check_and_return_func_params(
          instance,
          asl,
          param_list.as_ref(),
          &param_values,
        ) {
          Ok(values) => {
            let result = match function_type {
              FunctionType::Native(func) => func(instance, asl, values),
              _ => CommandExec::run_command_function(instance, asl, func, values),
            };
            return self.process_function_call_result(instance, asl, &return_type, result);
          }
          Err(err) => return Err(err),
        }
      }
      Value::Command {
        token_list,
        id_list,
      } => {
        let result =
          CommandExec::run_command_with_params(instance, asl, token_list, id_list, param_values);
        return self.process_command_call_result(result);
      }
      Value::MultiFunction(funcs) => {
        for f in funcs.iter() {
          if let Value::Function {
            param_list,
            return_type,
            function_type,
          } = f
          {
            if let Ok(values) = CommandExec::check_and_return_func_params(
              instance,
              asl,
              param_list.as_ref(),
              &param_values,
            ) {
              let result = match function_type {
                FunctionType::Native(func) => func(instance, asl, values),
                _ => CommandExec::run_command_function(instance, asl, f.clone(), values),
              };
              return self.process_function_call_result(instance, asl, &return_type, result);
            }
          }
        }
        let mut error_string =
          String::from("No valid option found for the given parameters.\nGiven: ");
        error_string.push_str(&base_utils::vec_to_str(
          &param_values
            .iter()
            .map(|x| base_utils::value_to_anoted_value_string(instance, asl, x))
            .collect(),
          ", ",
        ));
        error_string.push_str("\nValid options:\n");
        error_string.push_str(&base_utils::vec_to_str(
          &funcs
            .iter()
            .map(|f| {
              if let Value::Function { param_list, .. } = f {
                if param_list.len() > 0 {
                  base_utils::vec_to_str(
                    &param_list
                      .iter()
                      .map(|x| format!("<{}>", x.to_string(instance, asl)))
                      .collect(),
                    ", ",
                  )
                } else {
                  String::from("No parameters")
                }
              } else {
                String::from("")
              }
            })
            .collect(),
          "\n",
        ));
        return Err(error_string);
      }
      _ => panic!("Called check_and_run_func without a function"),
    }
  }

  fn run_meta_func(
    &mut self,
    instance: &mut Instance,
    asl: &ASL,
    func: Value,
    id_list: Rc<Vec<usize>>,
    token_list: Rc<Vec<Token>>,
  ) -> Result<(), String> {
    match func.clone() {
      Value::Meta {
        return_type,
        start_value_list,
        com_token_list,
        com_id_list,
        all_scopes_list,
        ..
      } => {
        let mut values = vec![Value::Command {
          id_list,
          token_list,
        }];
        if start_value_list.len() > 0 {
          for value in start_value_list.iter() {
            values.push(value.clone());
          }
        }
        let result = CommandExec::run_command_with_named_params(
          instance,
          asl,
          com_token_list,
          com_id_list,
          Vec::clone(&all_scopes_list),
          values,
        );
        return self.process_function_call_result(instance, asl, &return_type, result);
      }
      _ => panic!("called run_meta_func without a meta function"),
    }
  }

  fn run_instance_function(&mut self, sysfunc: SystemFunction) -> Result<(), (String, Rc<String>)> {
    match sysfunc {
      SystemFunction::PushToStack(value) => self.push_to_stack(value),
      SystemFunction::AddCheckAndRunCall(instance, asl, value, param_values, file) => {
        if let Err(err) = self.check_and_run_func(instance, asl, value, param_values) {
          return Err((err, file));
        }
      }
      SystemFunction::AddMetaCall(instance, asl, value, id_list, token_list, file) => {
        if let Err(err) = self.run_meta_func(instance, asl, value, id_list, token_list) {
          return Err((err, file));
        }
      }
      SystemFunction::AddSetCall(scope_id, file) => self.add_set_call(scope_id, file),
      SystemFunction::AddSetRefCall(ref_value, file) => self.add_set_ref_call(ref_value, file),
      SystemFunction::Nothing => (),
    };
    return Ok(());
  }

  fn add_function_call<'a, 'b>(
    instance: &'a mut Instance,
    asl: &'b ASL,
    value: Value,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
    file: Rc<String>,
  ) -> Result<SystemFunction<'a, 'b>, String> {
    let param_values;
    match value {
      Value::Meta { .. } => {
        return Ok(SystemFunction::AddMetaCall(
          instance, asl, value, id_list, token_list, file,
        ))
      }
      Value::Command { .. } | Value::Function { .. } | Value::MultiFunction { .. } => {
        let result = CommandExec::run_command(instance, asl, token_list, id_list);
        match &result {
          CommandExecReturn::Stack(stack) => {
            param_values = stack.clone();
          }
          CommandExecReturn::NoReturn => {
            param_values = Vec::with_capacity(0);
          }
          CommandExecReturn::Error(err) => {
            return Err(err.clone());
          }
          CommandExecReturn::Return(_) => {
            return Err(String::from("Return sent inside a function call."))
          }
          CommandExecReturn::End => {
            return Err(String::from("End request sent inside a function call."))
          }
          CommandExecReturn::Loop(_) => {
            return Err(String::from(
              "Continue/Break request sent inside a function call.",
            ))
          }
          CommandExecReturn::Exit(_) => {
            return Err(String::from("Exit request sent inside a function call."))
          }
        }
        return Ok(SystemFunction::AddCheckAndRunCall(
          instance,
          asl,
          value,
          param_values,
          file,
        ));
      }
      Value::Reference(reference) => match instance.get_var_by_ref(&reference) {
        Ok(value) => {
          return CommandExec::add_function_call(instance, asl, value, token_list, id_list, file)
        }
        Err(err) => return Err(err),
      },
      _ => return Err(String::from("Attempting to call a non function value.")),
    }
  }

  fn run_token(&mut self, instance: &mut Instance, asl: &ASL) -> TokenExecReturn {
    if self.has_possible_call() {
      self.run_last_call(instance);
      return TokenExecReturn::Continue;
    } else {
      let mut sysfunc = SystemFunction::Nothing;
      match self.get_curr_token() {
        Some(token) => match token {
          Token::Number(num) => sysfunc = SystemFunction::PushToStack(Value::Number(num.clone())),
          Token::Unsigned(num) => {
            sysfunc = SystemFunction::PushToStack(Value::Unsigned(num.clone()))
          }
          Token::Signed(num) => sysfunc = SystemFunction::PushToStack(Value::Signed(num.clone())),
          Token::Float(num) => sysfunc = SystemFunction::PushToStack(Value::Float(num.clone())),
          Token::String(string) => {
            sysfunc = SystemFunction::PushToStack(Value::String(string.clone()))
          }
          Token::Raw(value) => sysfunc = SystemFunction::PushToStack(Value::Raw(value.clone())),
          Token::Command {
            token_list,
            id_list,
          } => {
            sysfunc = SystemFunction::PushToStack(Value::Command {
              token_list: token_list.clone(),
              id_list: id_list.clone(),
            })
          }
          Token::List {
            token_list,
            id_list,
            file,
          } => {
            let result =
              CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
            match result {
              CommandExecReturn::Stack(stack) => {
                sysfunc = SystemFunction::PushToStack(Value::List(Rc::new(stack)));
              }
              CommandExecReturn::NoReturn => {
                sysfunc = SystemFunction::PushToStack(Value::List(Rc::new(Vec::with_capacity(0))));
              }
              CommandExecReturn::Error(err) => {
                return_token_error_line!(err, token.to_string(instance, asl), file)
              }
              CommandExecReturn::Return(_) => return_token_error_line!(
                "Return sent inside a list definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::End => return_token_error_line!(
                "End request sent inside a list definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::Loop(_) => return_token_error_line!(
                "Continue/Break request sent inside a list definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::Exit(_) => return_token_error_line!(
                "Exit request sent inside a list definition.",
                token.to_string(instance, asl),
                file
              ),
            }
          }
          Token::Dictionary {
            token_list,
            id_list,
            file,
          } => {
            let result =
              CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
            match result {
              CommandExecReturn::Stack(stack) => {
                match ValueMap::from_value_list(instance, asl, &stack) {
                  Ok(map) => sysfunc = SystemFunction::PushToStack(Value::Dictionary(Rc::new(map))),
                  Err(err) => {
                    return_token_error_line!(err.to_string(), token.to_string(instance, asl), file)
                  }
                }
              }
              CommandExecReturn::NoReturn => {
                sysfunc = SystemFunction::PushToStack(Value::Dictionary(Rc::new(ValueMap::new())))
              }
              CommandExecReturn::Error(err) => {
                return_token_error_line!(err, token.to_string(instance, asl), file)
              }
              CommandExecReturn::Return(_) => return_token_error_line!(
                "Return sent inside a dictionary definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::End => return_token_error_line!(
                "End request sent inside a dictionary definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::Loop(_) => return_token_error_line!(
                "Continue/Break request sent inside a dictionary definition.",
                token.to_string(instance, asl),
                file
              ),
              CommandExecReturn::Exit(_) => return_token_error_line!(
                "Exit request sent inside a dictionary definition.",
                token.to_string(instance, asl),
                file
              ),
            }
          }
          Token::Declare(id) => {
            sysfunc = SystemFunction::PushToStack(Value::Reference(ScopeId::Block(id.clone())));
          }
          Token::Var { scope_id, file } => match instance.get_var(scope_id) {
            Ok(value) => {
              sysfunc = SystemFunction::PushToStack(value);
            }
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::AnonymousVar { id, file } => match instance.get_anonymous_var(id) {
            Ok(value) => {
              sysfunc = SystemFunction::PushToStack(value);
            }
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::Define { scope_id, file } => {
            sysfunc = SystemFunction::AddSetCall(scope_id.clone(), file.clone())
          }
          Token::Set { scope_id, file } => {
            sysfunc = SystemFunction::AddSetCall(scope_id.clone(), file.clone())
          }
          Token::Ref { scope_id, .. } => {
            sysfunc = SystemFunction::PushToStack(instance.create_var_ref(scope_id))
          }
          Token::GetRef { scope_id, file } => match instance.get_var(scope_id) {
            Ok(Value::Reference(reference)) => match instance.get_var_by_ref(&reference) {
              Ok(value) => {
                sysfunc = SystemFunction::PushToStack(value);
              }
              Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
            },
            Ok(_) => return_token_error_line!(
              "Given value is not a reference.",
              token.to_string(instance, asl),
              file
            ),
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::AnonymousGetRef { id, file } => match instance.get_anonymous_var(id) {
            Ok(Value::Reference(reference)) => match instance.get_var_by_ref(&reference) {
              Ok(value) => {
                sysfunc = SystemFunction::PushToStack(value);
              }
              Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
            },
            Ok(_) => return_token_error_line!(
              "Given value is not a reference.",
              token.to_string(instance, asl),
              file
            ),
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::SetRef { scope_id, file } => match instance.get_var(scope_id) {
            Ok(ref_value) => {
              if let Value::Reference(reference) = ref_value {
                sysfunc = SystemFunction::AddSetRefCall(reference, file.clone())
              }
            }
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::FunctionCall {
            token_list,
            id_list,
            scope_id,
            file,
          } => match instance.get_var(scope_id) {
            Ok(value) => {
              match CommandExec::add_function_call(
                instance,
                asl,
                value,
                token_list.clone(),
                id_list.clone(),
                file.clone(),
              ) {
                Ok(sf) => sysfunc = sf,
                Err(err) => {
                  return_token_error_line!(err, token.to_simple_string(instance, asl), file)
                }
              }
            }
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::AnonymousFunctionCall {
            id,
            token_list,
            id_list,
            file,
          } => match instance.get_anonymous_var(id) {
            Ok(value) => {
              match CommandExec::add_function_call(
                instance,
                asl,
                value,
                token_list.clone(),
                id_list.clone(),
                file.clone(),
              ) {
                Ok(sf) => sysfunc = sf,
                Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
              }
            }
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::RefFunctionCall {
            token_list,
            id_list,
            scope_id,
            file,
          } => match instance.get_var(scope_id) {
            Ok(Value::Reference(reference)) => match instance.get_var_by_ref(&reference) {
              Ok(value) => {
                match CommandExec::add_function_call(
                  instance,
                  asl,
                  value,
                  token_list.clone(),
                  id_list.clone(),
                  file.clone(),
                ) {
                  Ok(sf) => sysfunc = sf,
                  Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
                }
              }
              Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
            },
            Ok(_) => return_token_error_line!(
              "Given value is not a reference.",
              token.to_string(instance, asl),
              file
            ),
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
          Token::AnonymousRefFunctionCall {
            id,
            token_list,
            id_list,
            file,
          } => match instance.get_anonymous_var(id) {
            Ok(Value::Reference(reference)) => match instance.get_var_by_ref(&reference) {
              Ok(value) => {
                match CommandExec::add_function_call(
                  instance,
                  asl,
                  value,
                  token_list.clone(),
                  id_list.clone(),
                  file.clone(),
                ) {
                  Ok(sf) => sysfunc = sf,
                  Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
                }
              }
              Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
            },
            Ok(_) => return_token_error_line!(
              "Given value is not a reference.",
              token.to_string(instance, asl),
              file
            ),
            Err(err) => return_token_error_line!(err, token.to_string(instance, asl), file),
          },
        },
        None => {
          if !self.has_possible_call() && self.has_call() {
            let call = self.calls.pop().unwrap();
            return_error_line!(String::from("Unfinished function call"), call.file)
          }
          return TokenExecReturn::End;
        }
      }
      self.inc_token_pos();
      if let Err((err, file)) = self.run_instance_function(sysfunc) {
        return_token_error_line!(
          err,
          self
            .get_last_token()
            .unwrap()
            .to_simple_string(instance, asl),
          file
        )
      }
      return TokenExecReturn::Continue;
    }
  }

  fn run(mut self, instance: &mut Instance, asl: &ASL) -> CommandExecReturn {
    loop {
      let result = self.run_token(instance, asl);
      match result {
        TokenExecReturn::Continue => (),
        TokenExecReturn::Error(err) => return_error_string!(err),
        TokenExecReturn::End => match self.return_to_pass {
          Some(return_to_pass) => {
            return return_to_pass;
          }
          None => {
            if self.stack.len() > 0 {
              return CommandExecReturn::Stack(self.stack);
            } else {
              return CommandExecReturn::NoReturn;
            }
          }
        },
      }
    }
  }
}
