use crate::asl::ASL;
use crate::instance::Instance;
use crate::parsers::parser_consts;
use crate::utils::to_system_string::ToSystemString;

#[derive(Clone, PartialEq)]
pub enum ScopeId {
  Block(usize),
  Local(String),
  Global(String),
}

impl ToSystemString for ScopeId {
  fn to_string(&self, instance: &mut Instance, _: &ASL) -> String {
    let name = instance.scope_id_to_name(self);
    match self {
      ScopeId::Block(_) => name,
      ScopeId::Global(_) => format!("{}{}", parser_consts::GLOBAL_STR, name),
      ScopeId::Local(_) => format!("{}{}", parser_consts::LOCAL_STR, name),
    }
  }
}
