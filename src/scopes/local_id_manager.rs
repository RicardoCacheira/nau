use std::collections::HashMap;

pub const INTERNAL_ID_INDICATOR: char = '@';

pub struct LocalIdManager {
  pub is_valid_external_id: fn(&str) -> bool,
  counter: usize,
  curr_var_list: HashMap<String, String>,
  name_registry: HashMap<String, String>,
}

fn is_valid_external_id_normal(id: &str) -> bool {
  return if let Some(c) = id.chars().next() {
    c != INTERNAL_ID_INDICATOR
  } else {
    false
  };
}

fn is_valid_external_id_debug(_: &str) -> bool {
  true
}

impl LocalIdManager {
  pub fn new(is_debug: bool) -> LocalIdManager {
    LocalIdManager {
      is_valid_external_id: if is_debug {
        is_valid_external_id_debug
      } else {
        is_valid_external_id_normal
      },
      counter: 0,
      curr_var_list: HashMap::new(),
      name_registry: HashMap::new(),
    }
  }

  pub fn generate_internal_id(&mut self, var_name: &str) -> String {
    if self.curr_var_list.contains_key(var_name) {
      return self.curr_var_list.get(var_name).unwrap().clone();
    } else {
      self.counter += 1;
      let id = format!("@{}", self.counter);
      self
        .curr_var_list
        .insert(String::from(var_name), id.clone());
      self
        .name_registry
        .insert(id.clone(), String::from(var_name));
      return id;
    }
  }

  pub fn flush_ids(&mut self) {
    self.curr_var_list.clear();
  }

  pub fn id_to_name(&self, id: &str) -> String {
    match self.name_registry.get(id) {
      Some(name) => name.clone(),
      None => format!("{{id:{}}}", id),
    }
  }
}
