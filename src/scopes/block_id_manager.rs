use std::collections::HashMap;

struct Block {
  var_list: HashMap<String, usize>,
  id_list: Vec<usize>,
  //delayed_list: HashMap<String, usize>,
  //is_delayed: bool,
}

impl Block {
  fn new() -> Block {
    Block {
      var_list: HashMap::new(),
      id_list: Vec::new(),
      // delayed_list: HashMap::new(),
      // is_delayed: false,
    }
  }

  // fn tick_delay(&mut self) {
  //   if self.is_delayed {
  //     for (name, id) in self.delayed_list.drain() {
  //       self.var_list.insert(name, id);
  //       self.id_list.push(id);
  //     }
  //     self.is_delayed = false;
  //   }
  // }

  fn insert(&mut self, name: &str, id: usize) {
    self.var_list.insert(String::from(name), id);
    self.id_list.push(id);
  }

  // fn insert_delayed(&mut self, name: &str, id: usize) {
  //   self.delayed_list.insert(String::from(name), id);
  //   self.is_delayed = true;
  // }
}

pub struct BlockIdManager {
  counter: usize,
  block_list: Vec<Block>,
  name_registry: HashMap<usize, String>,
}

impl BlockIdManager {
  pub fn new() -> BlockIdManager {
    BlockIdManager {
      counter: 0,
      block_list: Vec::new(),
      name_registry: HashMap::new(),
    }
  }

  fn generate_internal_id(&mut self) -> usize {
    self.counter += 1;
    return self.counter;
  }

  pub fn start_block(&mut self) {
    self.block_list.push(Block::new());
  }

  pub fn end_block(&mut self) -> Vec<usize> {
    self.block_list.pop().unwrap().id_list
  }

  pub fn new_var(&mut self, name: &str) -> usize {
    let id = self.generate_internal_id();
    self.name_registry.insert(id, String::from(name));
    let curr_block = self.block_list.last_mut().unwrap();
    curr_block.insert(name, id);
    id
  }

  // pub fn new_delayed_var(&mut self, name: &str) -> usize {
  //   let id = self.generate_internal_id();
  //   self.name_registry.insert(id, String::from(name));
  //   let curr_block = self.block_list.last_mut().unwrap();
  //   curr_block.insert_delayed(name, id);
  //   id
  // }

  pub fn get_var_id(&self, name: &str) -> Option<usize> {
    for block in self.block_list.iter().rev() {
      if block.var_list.contains_key(name) {
        return Some(block.var_list.get(name).unwrap().clone());
      }
    }
    None
  }

  pub fn panic_if_has_blocks(&self) {
    if self.block_list.len() > 0 {
      panic!("Block Id Manager has open blocks!")
    }
  }

  pub fn clear_blocks(&mut self) {
    self.block_list.clear();
  }

  pub fn id_to_name(&self, id: &usize) -> String {
    match self.name_registry.get(id) {
      Some(name) => name.clone(),
      None => format!("{{id:{}}}", id),
    }
  }

  // pub fn tick_delay(&mut self) {
  //   self.block_list.last_mut().unwrap().tick_delay();
  // }
}
