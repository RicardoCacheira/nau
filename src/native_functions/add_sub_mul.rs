use super::lists_strings_raws;
use super::numbers;
use crate::instance::Instance;
use crate::values::{function, SystemType as Type};
use std::rc::Rc;

pub fn load_native_functions(intr: &mut Instance) {
  def_multi_func!(
    intr,
    "add",
    [
      new_func!([Type::Number, Type::Number], Type::Number, numbers::add_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::PrimitiveUnsigned,
        numbers::add_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::PrimitiveSigned,
        numbers::add_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::PrimitiveFloat,
        numbers::add_float
      ),
      new_func!(
        [Type::String, Type::Any],
        Type::String,
        lists_strings_raws::join_strings
      ),
      new_func!(
        [Type::List, Type::List],
        Type::List,
        lists_strings_raws::join_lists
      )
    ]
  );
  def_multi_func!(
    intr,
    "sub",
    [
      new_func!([Type::Number, Type::Number], Type::Number, numbers::sub_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::PrimitiveUnsigned,
        numbers::sub_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::PrimitiveSigned,
        numbers::sub_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::PrimitiveFloat,
        numbers::sub_float
      ),
      new_func!(
        [Type::List, Type::List],
        Type::List,
        lists_strings_raws::sub_lists
      ),
      new_func!(
        [Type::String, Type::String],
        Type::String,
        lists_strings_raws::sub_string
      ),
    ]
  );
  def_multi_func!(
    intr,
    "mul",
    [
      new_func!([Type::Number, Type::Number], Type::Number, numbers::mul_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::PrimitiveUnsigned,
        numbers::mul_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::PrimitiveSigned,
        numbers::mul_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::PrimitiveFloat,
        numbers::mul_float
      ),
      new_func!(
        [Type::List, Type::AnyNumber],
        Type::List,
        lists_strings_raws::mul_list
      ),
      new_func!(
        [Type::String, Type::AnyNumber],
        Type::String,
        lists_strings_raws::mul_string
      ),
      new_func!(
        [Type::Raw, Type::AnyNumber],
        Type::Raw,
        lists_strings_raws::mul_raw
      ),
    ]
  );
}
