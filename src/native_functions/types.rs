use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::rc_vec;
use crate::utils::number_utils;
use crate::values::{function, SystemType as Type, Value, ValueMapInterface};
use std::rc::Rc;

// "Voidable" native functions

fn create_voidable_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let t = system_type!(params, 0);
  ret_val!(Value::Type(Type::Voidable(Rc::new(t.clone()))));
}

// "Multiple" native functions

fn create_multiple_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:1");
    }
  }
  ret_val!(Value::Type(Type::Multiple(Rc::new(list))));
}

// "Many" native functions

fn create_many_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:2");
    }
  }
  ret_val!(Value::Type(Type::Many(Rc::new(list))));
}

// "Set" native functions

fn create_set_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:3");
    }
  }
  ret_val!(Value::Type(Type::Set(Rc::new(list))));
}

// "ListOf" native functions

fn create_list_of_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::ListOf(Rc::new(list))));
}

// "UniqueList" native functions

fn create_unique_list_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::UniqueList(Rc::new(list))));
}

// "Sequence" native functions

fn create_sequence_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::Sequence(Rc::new(list))));
}

// "VectorOf" native functions

fn create_vector_of_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::VectorOf(Rc::new(list))));
}

// "DictionaryOf" native functions

fn create_dictionary_of_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::DictionaryOf(Rc::new(list))));
}

// "MapOf" native functions

fn create_map_of_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      list.push(t.clone());
    } else {
      panic!("TYPES:4");
    }
  }
  ret_val!(Value::Type(Type::MapOf(Rc::new(list))));
}

// "Interface" native functions

fn create_interface_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let interface = ValueMapInterface::from_value_map(dictionary!(params, 0).as_ref());
  match interface {
    Ok(interface) => ret_val!(Value::Type(Type::Interface(Rc::new(interface)))),
    Err(err) => ret_error_str!(err),
  }
}

// "DictionaryInterface" native functions

fn create_dictionary_interface_type(
  _: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let interface = ValueMapInterface::from_value_map(dictionary!(params, 0).as_ref());
  match interface {
    Ok(interface) => ret_val!(Value::Type(Type::DictionaryInterface(Rc::new(interface)))),
    Err(err) => ret_error_str!(err),
  }
}

// "MapInterface" native functions

fn create_map_interface_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let interface = ValueMapInterface::from_value_map(dictionary!(params, 0).as_ref());
  match interface {
    Ok(interface) => ret_val!(Value::Type(Type::MapInterface(Rc::new(interface)))),
    Err(err) => ret_error_str!(err),
  }
}

// "Sized" native functions

fn create_sized_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let t = system_type!(params, 0);
  let min = number_utils::rational_to_usize(number!(params, 1));
  let max = number_utils::rational_to_usize(number!(params, 2));
  match (min, max) {
    (Some(min), Some(max)) => {
      if max == 0 || min <= max {
        ret_val!(Value::Type(Type::Sized {
          value_type: Rc::new(t.clone()),
          min,
          max
        }));
      }
      ret_error_str!("Max value must be equal or greater than the Min value.")
    }
    (_, _) => ret_error_str!("Error processing the Sized limits."),
  }
}

// "Or" native functions

fn create_or_type(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut aux = Vec::with_capacity(vals.len());
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      if !aux.contains(t) {
        aux.push(t.clone());
      }
    } else {
      panic!("TYPES:5");
    }
  }
  //Removes all subtypes of other types considering that those types already consider all values of the subtypes
  for (i0, t0) in aux.iter().enumerate() {
    let mut is_subtype = false;
    for (i1, t1) in aux.iter().enumerate() {
      //the reason that it checks the position instead of checking if the types are the same is to remove possible duplicates
      if i0 != i1 && t1.is_supertype_of(instance, asl, t0) {
        is_subtype = true;
        break;
      }
    }
    if !is_subtype {
      list.push(t0.clone());
    }
  }
  ret_val!(Value::Type(Type::Or(Rc::new(list))));
}

// "And" native functions

fn create_and_type(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut aux = Vec::with_capacity(vals.len());
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      if !aux.contains(t) {
        aux.push(t.clone());
      }
    } else {
      panic!("TYPES:5");
    }
  }
  //Removes all supertypes of other types considering that those types already restrict the values of the supertypes
  for (i0, t0) in aux.iter().enumerate() {
    let mut is_supertype = false;
    for (i1, t1) in aux.iter().enumerate() {
      //the reason that it checks the position instead of checking if the types are the same is to remove possible duplicates
      if i0 != i1 && t0.is_supertype_of(instance, asl, t1) {
        is_supertype = true;
        break;
      }
    }
    if !is_supertype {
      list.push(t0.clone());
    }
  }
  ret_val!(Value::Type(Type::And(Rc::new(list))));
}

// "ValueOption" native functions

fn create_value_option_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Type(Type::ValueOption(list!(params, 0).clone())));
}

// "RefOf" native functions

fn create_ref_of_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Type(Type::RefOf(Rc::new(
    system_type!(params, 0).clone()
  ))));
}

// "Check" native functions

fn create_check_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let t = system_type!(params, 0);
  let (token_list, id_list) = command!(params, 1);
  ret_val!(Value::Type(Type::Check {
    value_type: Rc::new(t.clone()),
    token_list: token_list.clone(),
    id_list: id_list.clone(),
  }));
}

// "SelfType" native functions

fn create_self_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Type(Type::SelfType(string!(params, 0).clone())));
}

// "SubType" native functions

fn create_sub_type(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut aux = Vec::with_capacity(vals.len());
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      if !aux.contains(t) {
        aux.push(t.clone());
      }
    } else {
      panic!("TYPES:5");
    }
  }
  //Removes all subtypes of other types considering that those types already consider all values of the subtypes
  for (i0, t0) in aux.iter().enumerate() {
    let mut is_subtype = false;
    for (i1, t1) in aux.iter().enumerate() {
      //the reason that it checks the position instead of checking if the types are the same is to remove possible duplicates
      if i0 != i1 && t1.is_supertype_of(instance, asl, t0) {
        is_subtype = true;
        break;
      }
    }
    if !is_subtype {
      list.push(t0.clone());
    }
  }
  ret_val!(Value::Type(Type::SubType(Rc::new(list))));
}

// "SuperType" native functions

fn create_super_type(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vals = list!(params, 0);
  let mut aux = Vec::with_capacity(vals.len());
  let mut list = Vec::with_capacity(vals.len());
  for val in vals.iter() {
    if let Value::Type(t) = val {
      if !aux.contains(t) {
        aux.push(t.clone());
      }
    } else {
      panic!("TYPES:5");
    }
  }
  //Removes all supertypes of other types considering that those types already restrict the values of the supertypes
  for (i0, t0) in aux.iter().enumerate() {
    let mut is_supertype = false;
    for (i1, t1) in aux.iter().enumerate() {
      //the reason that it checks the position instead of checking if the types are the same is to remove possible duplicates
      if i0 != i1 && t0.is_supertype_of(instance, asl, t1) {
        is_supertype = true;
        break;
      }
    }
    if !is_supertype {
      list.push(t0.clone());
    }
  }
  ret_val!(Value::Type(Type::SuperType(Rc::new(list))));
}

// "Not" native functions

fn create_not_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Type(Type::Not(Rc::new(
    system_type!(params, 0).clone()
  ))));
}

// "Dynamic" native functions

fn create_dynamic_type(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let reference = reference!(params, 0);
  ret_val!(Value::Type(Type::Dynamic(reference.clone())));
}

// "new_type" native functions

fn create_custom_type_instance(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list, id_list) = command!(params, 2);
  ret_val!(Value::Type(Type::CustomTypeInstance {
    name: string!(params, 0).clone(),
    value_type: Rc::new(system_type!(params, 1).clone()),
    to_string_token_list: token_list.clone(),
    to_string_id_list: id_list.clone(),
  }));
}

pub fn load_native_functions(intr: &mut Instance) {
  let type_list = Type::Many(rc_vec![Type::ValueType]);
  def_func!(
    intr,
    "Voidable",
    [Type::ValueType],
    Type::Type,
    create_voidable_type
  );
  def_func!(
    intr,
    "Multiple",
    [type_list.clone()],
    Type::ParamType,
    create_multiple_type
  );
  def_func!(
    intr,
    "Many",
    [type_list.clone()],
    Type::ParamType,
    create_many_type
  );
  def_func!(
    intr,
    "Set",
    [type_list.clone()],
    Type::ValueType,
    create_set_type
  );
  def_func!(
    intr,
    "ListOf",
    [type_list.clone()],
    Type::ValueType,
    create_list_of_type
  );
  def_func!(
    intr,
    "UniqueList",
    [type_list.clone()],
    Type::ValueType,
    create_unique_list_type
  );
  def_func!(
    intr,
    "Sequence",
    [type_list.clone()],
    Type::ValueType,
    create_sequence_type
  );
  def_func!(
    intr,
    "VectorOf",
    [type_list.clone()],
    Type::ValueType,
    create_vector_of_type
  );
  def_func!(
    intr,
    "DictionaryOf",
    [type_list.clone()],
    Type::ValueType,
    create_dictionary_of_type
  );
  def_func!(
    intr,
    "MapOf",
    [type_list.clone()],
    Type::ValueType,
    create_map_of_type
  );
  def_func!(
    intr,
    "Interface",
    [DictionaryOf!(Type::ValueType)],
    Type::ValueType,
    create_interface_type
  );
  def_func!(
    intr,
    "DictionaryInterface",
    [DictionaryOf!(Type::ValueType)],
    Type::ValueType,
    create_dictionary_interface_type
  );
  def_func!(
    intr,
    "MapInterface",
    [DictionaryOf!(Type::ValueType)],
    Type::ValueType,
    create_map_interface_type
  );
  def_func!(
    intr,
    "Sized",
    [
      SubType!(Type::List, Type::String, Type::Vector),
      Type::SystemUnsigned,
      Type::SystemUnsigned
    ],
    Type::ValueType,
    create_sized_type
  );
  def_func!(
    intr,
    "Or",
    [type_list.clone()],
    Type::ValueType,
    create_or_type
  );
  def_func!(
    intr,
    "And",
    [type_list.clone()],
    Type::ValueType,
    create_and_type
  );
  def_func!(
    intr,
    "ValueOption",
    [Type::Many(rc_vec![Type::Any])],
    Type::ValueType,
    create_value_option_type
  );
  def_func!(
    intr,
    "RefOf",
    [Type::ValueType],
    Type::ValueType,
    create_ref_of_type
  );
  def_func!(
    intr,
    "Check",
    [Type::ValueType, Type::Command],
    Type::ValueType,
    create_check_type
  );
  def_func!(
    intr,
    "SelfType",
    [Type::String],
    Type::ValueType,
    create_self_type
  );
  def_func!(
    intr,
    "SubType",
    [type_list.clone()],
    Type::ValueType,
    create_sub_type
  );
  def_func!(
    intr,
    "SuperType",
    [type_list.clone()],
    Type::ValueType,
    create_super_type
  );
  def_func!(
    intr,
    "Not",
    [Type::ValueType],
    Type::ValueType,
    create_not_type
  );
  def_func!(
    intr,
    "Dynamic",
    [Type::Reference],
    Type::ValueType,
    create_dynamic_type
  );
  def_func!(
    intr,
    "new_type",
    [Type::String, Type::ValueType, Type::Command],
    Type::ValueType,
    create_custom_type_instance
  );
}
