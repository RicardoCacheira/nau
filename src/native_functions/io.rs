use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::utils::to_system_string::ToSystemString;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "in" native functions

fn read_input(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::String(Rc::new(asl.console.read())));
}

// "get_cursor_pos" native functions

fn get_cursor_pos(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  let (col, row) = asl.console.get_cursor_pos();
  ret_val!(Value::List(Rc::new(vec![
    Value::Unsigned(col as u64),
    Value::Unsigned(row as u64)
  ])));
}

// "set_cursor_pos" native functions

fn set_cursor_pos(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  asl
    .console
    .set_cursor_pos(unsigned!(params, 0) as usize, unsigned!(params, 1) as usize);
  ret_none!();
}

// "cursor_on" native functions

fn cursor_on(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  asl.console.cursor_on();
  ret_none!();
}

// "cursor_off" native functions

fn cursor_off(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  asl.console.cursor_off();
  ret_none!();
}

// "print" native functions

fn print(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  asl.console.print(&params[0].to_string(instance, asl));
  ret_none!();
}

// "clear" native functions

fn clear(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  asl.console.clear();
  ret_none!();
}

// "term_size" native functions

fn term_size(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  let (col, row) = asl.console.size();

  ret_val!(Value::List(Rc::new(vec![
    Value::Unsigned(col as u64),
    Value::Unsigned(row as u64)
  ])));
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(intr, "in", [], Type::String, read_input);
  def_func!(
    intr,
    "get_cursor_pos",
    [],
    Set!(Type::PrimitiveUnsigned, Type::PrimitiveUnsigned),
    get_cursor_pos
  );
  def_func!(
    intr,
    "set_cursor_pos",
    [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
    Type::Void,
    set_cursor_pos
  );
  def_func!(intr, "cursor_on", [], Type::Void, cursor_on);
  def_func!(intr, "cursor_off", [], Type::Void, cursor_off);
  def_func!(intr, "print", [Type::Any], Type::Void, print);
  def_func!(intr, "clear", [], Type::Void, clear);
  def_func!(
    intr,
    "term_size",
    [],
    Set!(Type::PrimitiveUnsigned, Type::PrimitiveUnsigned),
    term_size
  );
}
