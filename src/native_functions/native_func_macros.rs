#[macro_export]
macro_rules! ret_error_string {
  ($err:expr) => {
    return CommandExecReturn::Error($err);
  };
}

#[macro_export]
macro_rules! ret_error_str {
  ($err:expr) => {
    ret_error_string!(String::from($err))
  };
}

#[macro_export]
macro_rules! ret_val {
  ($val:expr) => {
    return CommandExecReturn::Return($val);
  };
}

#[macro_export]
macro_rules! ret_none {
  () => {
    return CommandExecReturn::NoReturn;
  };
}

#[macro_export]
macro_rules! number {
  ($list:expr,$pos:expr) => {
    if let Value::Number(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! unsigned {
  ($list:expr,$pos:expr) => {
    if let Value::Unsigned(a) = $list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! signed {
  ($list:expr,$pos:expr) => {
    if let Value::Signed(a) = $list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! float {
  ($list:expr,$pos:expr) => {
    if let Value::Float(a) = $list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! string {
  ($list:expr,$pos:expr) => {
    if let Value::String(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! raw {
  ($list:expr,$pos:expr) => {
    if let Value::Raw(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! val_to_string {
  ($value:expr,$err:expr) => {
    if let Value::String(a) = &$value {
      a
    } else {
      ret_error_str!($err);
    }
  };
}

#[macro_export]
macro_rules! boolean {
  ($list:expr,$pos:expr) => {
    if let Value::Boolean(a) = $list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! reference {
  ($list:expr,$pos:expr) => {
    if let Value::Reference(reference) = &$list[$pos] {
      reference
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! block_reference {
  ($list:expr,$pos:expr) => {
    if let Value::Reference(ScopeId::Block(id)) = &$list[$pos] {
      id
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! global_reference {
  ($list:expr,$pos:expr) => {
    if let Value::Reference(ScopeId::Global(name)) = &$list[$pos] {
      name
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! local_reference {
  ($list:expr,$pos:expr) => {
    if let Value::Reference(ScopeId::Local(id)) = &$list[$pos] {
      id
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! system_type {
  ($list:expr,$pos:expr) => {
    if let Value::Type(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

//Returns the type of a given value
#[macro_export]
macro_rules! val_to_system_type {
  ($value:expr,$err:expr) => {
    if let Value::Type(a) = &$value {
      a
    } else {
      ret_error_str!($err);
    }
  };
}

#[macro_export]
macro_rules! list {
  ($list:expr,$pos:expr) => {
    if let Value::List(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! vector_id {
  ($list:expr,$pos:expr) => {
    if let Value::Vector(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! vector {
  ($instance:expr,$list:expr,$pos:expr) => {
    if let Value::Vector(a) = &$list[$pos] {
      match $instance.get_vector(a) {
        Ok(vector) => vector,
        Err(err) => ret_error_string!(err),
      }
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! vector_or_list {
  ($instance:expr,$list:expr,$pos:expr) => {
    if let Value::Vector(a) = &$list[$pos] {
      match $instance.get_vector(a) {
        Ok(vector) => vector,
        Err(err) => ret_error_string!(err),
      }
    } else if let Value::List(a) = &$list[$pos] {
      a.as_ref()
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! dictionary {
  ($list:expr,$pos:expr) => {
    if let Value::Dictionary(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! map_id {
  ($list:expr,$pos:expr) => {
    if let Value::Map(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! map {
  ($instance:expr,$list:expr,$pos:expr) => {
    if let Value::Map(a) = &$list[$pos] {
      match $instance.get_map(a) {
        Ok(map) => map,
        Err(err) => ret_error_string!(err),
      }
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! buffer_id {
  ($list:expr,$pos:expr) => {
    if let Value::Buffer(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! buffer {
  ($instance:expr,$list:expr,$pos:expr) => {
    if let Value::Buffer(a) = &$list[$pos] {
      match $instance.get_buffer(a) {
        Ok(buffer) => buffer,
        Err(err) => ret_error_string!(err),
      }
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! command {
  ($list:expr,$pos:expr) => {
    if let Value::Command {
      id_list,
      token_list,
    } = &$list[$pos]
    {
      (token_list, id_list)
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! function {
  ($list:expr,$pos:expr) => {
    if let Value::Function {
      param_list,
      return_type,
      function_type,
    } = &$list[$pos]
    {
      (param_list, return_type, function_type)
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! multi_function {
  ($list:expr,$pos:expr) => {
    if let Value::MultiFunction(a) = &$list[$pos] {
      a
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! meta {
  ($list:expr,$pos:expr) => {
    if let Value::Meta {
      return_type,
      input_com_var_id,
      start_value_list,
      start_value_id_list,
      com_token_list,
      com_id_list,
      ..
    } = &$list[$pos]
    {
      (
        return_type,
        input_com_var_id,
        start_value_list,
        start_value_id_list,
        com_token_list,
        com_id_list,
      )
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! wrapped_value {
  ($list:expr,$pos:expr) => {
    if let Value::WrappedValue { value, .. } = &$list[$pos] {
      value
    } else {
      ret_error_string!(format!("Error on param {}", $pos));
    }
  };
}

#[macro_export]
macro_rules! new_func {
    ([$($params:expr),*],$return_type:expr,$func:expr) => {
      function::create_native_function(Rc::new(vec![$($params,)*]),$return_type, $func).unwrap()
    };
}

// Creates a single function definition
#[macro_export]
macro_rules! def_func {
    ($instance:expr,$name:expr,[$($params:expr),*],$return_type:expr,$func:expr) => {
        $instance.set_global_var(
            $name,
            function::create_native_function(Rc::new(vec![$($params,)*]),$return_type, $func).unwrap(),
        );
    };
}

#[macro_export]
macro_rules! def_multi_func {
    ($instance:expr,$name:expr,[$($params:expr),* $(,)*]) => {
        $instance.set_global_var(
            $name,
            function::create_multi_function(Rc::new(vec![$($params,)*])).unwrap(),
        );
    };
}
