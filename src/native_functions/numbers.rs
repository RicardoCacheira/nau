use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::values::{function, SystemType as Type, Value};
use num_rational::BigRational;
use num_traits::identities::Zero;
use num_traits::sign::Signed;
use std::rc::Rc;

// "add" native functions
// Used by add_sub_mul.rs

pub fn add_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  ret_val!(Value::Number(a + b));
}

pub fn add_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  ret_val!(Value::Unsigned(a.wrapping_add(b)));
}

pub fn add_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  ret_val!(Value::Signed(a.wrapping_add(b)));
}

pub fn add_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  ret_val!(Value::Float(a + b));
}

// "sub" native functions
// Used by add_sub_mul.rs

pub fn sub_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  ret_val!(Value::Number(a - b));
}

pub fn sub_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  ret_val!(Value::Unsigned(a.wrapping_sub(b)));
}

pub fn sub_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  ret_val!(Value::Signed(a.wrapping_sub(b)));
}

pub fn sub_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  ret_val!(Value::Float(a - b));
}

// "mul" native functions
// Used by add_sub_mul.rs

pub fn mul_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  ret_val!(Value::Number(a * b));
}

pub fn mul_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  ret_val!(Value::Unsigned(a.wrapping_mul(b)));
}

pub fn mul_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  ret_val!(Value::Signed(a.wrapping_mul(b)));
}

pub fn mul_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  ret_val!(Value::Float(a * b));
}

// "div" native functions

fn div_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  ret_val!(Value::Number(a / b));
}

fn div_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  ret_val!(Value::Unsigned(a.wrapping_div(b)));
}

fn div_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  ret_val!(Value::Signed(a.wrapping_div(b)));
}

fn div_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  ret_val!(Value::Float(a / b));
}

// "mod" native functions

fn mod_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  ret_val!(Value::Number(a % b));
}

fn mod_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  ret_val!(Value::Unsigned(a % b));
}

fn mod_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  ret_val!(Value::Signed(a % b));
}

fn mod_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  ret_val!(Value::Float(a % b));
}

// "div_mod" native functions

fn div_mod_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = number!(params, 0);
  let b = number!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  let remainder = a % b;
  ret_val!(Value::List(Rc::new(vec![
    Value::Number((a - remainder) / b),
    Value::Number(a % b)
  ])));
}

fn div_mod_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = unsigned!(params, 0);
  let b = unsigned!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  ret_val!(Value::List(Rc::new(vec![
    Value::Unsigned(a.wrapping_div(b)),
    Value::Unsigned(a % b)
  ])));
}

fn div_mod_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = signed!(params, 0);
  let b = signed!(params, 1);
  if b.is_zero() {
    ret_error_str!("Divided by zero.");
  }
  ret_val!(Value::List(Rc::new(vec![
    Value::Signed(a.wrapping_div(b)),
    Value::Signed(a % b)
  ])));
}

fn div_mod_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = float!(params, 0);
  let b = float!(params, 1);
  let remainder = a % b;
  ret_val!(Value::List(Rc::new(vec![
    Value::Float((a - remainder) / b),
    Value::Float(a % b)
  ])));
}

// "reciprocal" native functions

fn reciprocal(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.recip()));
}

// "abs" native functions

fn abs(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.abs()));
}

// "floor" native functions

fn floor(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.floor()));
}

// "ceil" native functions

fn ceil(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.ceil()));
}

// "numerator" native functions

fn numerator(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(BigRational::from(num.numer().clone())));
}

// "denominator" native functions

fn denominator(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(BigRational::from(num.denom().clone())));
}

// "trunc" native functions

fn trunc(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.trunc()));
}

// "fract" native functions

fn fract(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  ret_val!(Value::Number(num.fract()));
}

pub fn load_native_functions(intr: &mut Instance) {
  def_multi_func!(
    intr,
    "div",
    [
      new_func!([Type::Number, Type::Number], Type::Number, div_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::PrimitiveUnsigned,
        div_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::PrimitiveSigned,
        div_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::PrimitiveFloat,
        div_float
      ),
    ]
  );
  def_multi_func!(
    intr,
    "mod",
    [
      new_func!([Type::Number, Type::Number], Type::Number, mod_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::PrimitiveUnsigned,
        mod_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::PrimitiveSigned,
        mod_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::PrimitiveFloat,
        mod_float
      ),
    ]
  );
  def_multi_func!(
    intr,
    "div_mod",
    [
      new_func!(
        [Type::Number, Type::Number],
        Set!(Type::Number, Type::Number),
        div_mod_num
      ),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Set!(Type::PrimitiveUnsigned, Type::PrimitiveUnsigned),
        div_mod_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Set!(Type::PrimitiveSigned, Type::PrimitiveSigned),
        div_mod_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Set!(Type::PrimitiveFloat, Type::PrimitiveFloat),
        div_mod_float
      ),
    ]
  );
  def_func!(intr, "reciprocal", [Type::Number], Type::Number, reciprocal);
  def_func!(intr, "abs", [Type::Number], Type::Number, abs);
  def_func!(intr, "floor", [Type::Number], Type::Number, floor);
  def_func!(intr, "ceil", [Type::Number], Type::Number, ceil);
  def_func!(intr, "numerator", [Type::Number], Type::Number, numerator);
  def_func!(
    intr,
    "denominator",
    [Type::Number],
    Type::Number,
    denominator
  );
  def_func!(intr, "trunc", [Type::Number], Type::Number, trunc);
  def_func!(intr, "fract", [Type::Number], Type::Number, fract);
}
