use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::native_functions::utils;
use crate::utils::number_utils;
use crate::values::{function, SystemType as Type, Value};
use num_traits::sign::Signed;
use std::rc::Rc;

// "continue" native functions

fn send_loop_continue(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  return CommandExecReturn::Loop(true);
}

// "break" native functions

fn send_loop_break(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  return CommandExecReturn::Loop(false);
}

// "for" native functions

pub fn list_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for (i, value) in list.iter().enumerate() {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![
        Value::Number(number_utils::usize_to_rational(i)),
        value.clone(),
      ],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn vector_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vector = vector!(instance, params, 0).clone();
  let (token_list, scope_list) = command!(params, 1);
  for (i, value) in vector.iter().enumerate() {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![
        Value::Number(number_utils::usize_to_rational(i)),
        value.clone(),
      ],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn number_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = number!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  let max = number_utils::rational_to_usize(&num.abs()).unwrap();
  if num.is_negative() {
    let max_m1 = max - 1;
    for i in 0..max {
      let result = CommandExec::run_command_with_params(
        instance,
        asl,
        token_list.clone(),
        scope_list.clone(),
        vec![Value::Number(number_utils::usize_to_rational(max_m1 - i))],
      );
      match result {
        CommandExecReturn::Loop(continue_loop) => {
          if !continue_loop {
            ret_none!();
          }
        }
        CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
        _ => return result,
      }
    }
  } else {
    for i in 0..max {
      let result = CommandExec::run_command_with_params(
        instance,
        asl,
        token_list.clone(),
        scope_list.clone(),
        vec![Value::Number(number_utils::usize_to_rational(i))],
      );
      match result {
        CommandExecReturn::Loop(continue_loop) => {
          if !continue_loop {
            ret_none!();
          }
        }
        CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
        _ => return result,
      }
    }
  }
  ret_none!();
}

pub fn unsigned_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = unsigned!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for i in 0..num {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![Value::Unsigned(i)],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn signed_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let num = signed!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  let max = num.abs();
  if num.is_negative() {
    let max_m1 = max - 1;
    for i in 0..max {
      let result = CommandExec::run_command_with_params(
        instance,
        asl,
        token_list.clone(),
        scope_list.clone(),
        vec![Value::Signed(max_m1 - i)],
      );
      match result {
        CommandExecReturn::Loop(continue_loop) => {
          if !continue_loop {
            ret_none!();
          }
        }
        CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
        _ => return result,
      }
    }
  } else {
    for i in 0..max {
      let result = CommandExec::run_command_with_params(
        instance,
        asl,
        token_list.clone(),
        scope_list.clone(),
        vec![Value::Signed(i)],
      );
      match result {
        CommandExecReturn::Loop(continue_loop) => {
          if !continue_loop {
            ret_none!();
          }
        }
        CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
        _ => return result,
      }
    }
  }
  ret_none!();
}

pub fn string_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let string = string!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for (i, c) in string.chars().enumerate() {
    let mut s = String::with_capacity(1);
    s.push(c);
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![
        Value::Number(number_utils::usize_to_rational(i)),
        Value::String(Rc::new(s)),
      ],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn raw_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for (i, b) in raw.iter().enumerate() {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![
        Value::Number(number_utils::usize_to_rational(i)),
        Value::Raw(Rc::new(vec![b.clone()])),
      ],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn buffer_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0).clone();
  let (token_list, scope_list) = command!(params, 1);
  for (i, b) in buffer.iter().enumerate() {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![
        Value::Number(number_utils::usize_to_rational(i)),
        Value::Raw(Rc::new(vec![b.clone()])),
      ],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn dictionary_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let dictionary = dictionary!(params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for (key, value) in dictionary
    .get_key_list()
    .into_iter()
    .zip(dictionary.get_value_list())
  {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![Value::String(Rc::new(key)), value],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

pub fn map_for(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let map = map!(instance, params, 0);
  let (token_list, scope_list) = command!(params, 1);
  for (key, value) in map.get_key_list().into_iter().zip(map.get_value_list()) {
    let result = CommandExec::run_command_with_params(
      instance,
      asl,
      token_list.clone(),
      scope_list.clone(),
      vec![Value::String(Rc::new(key)), value],
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
  ret_none!();
}

// "loop" native functions

pub fn system_loop(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list, scope_list) = command!(params, 0);

  loop {
    let result = CommandExec::run_command(instance, asl, token_list.clone(), scope_list.clone());
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }
  }
}

// "while" native functions

pub fn while_loop(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list_check, scope_list_check) = command!(params, 0);
  let (token_list_body, scope_list_body) = command!(params, 1);
  let mut check;
  match utils::get_check_value(
    instance,
    asl,
    token_list_check.clone(),
    scope_list_check.clone(),
  ) {
    Ok(new_check) => check = new_check,
    Err(err) => ret_error_string!(err),
  }

  while check {
    let result = CommandExec::run_command(
      instance,
      asl,
      token_list_body.clone(),
      scope_list_body.clone(),
    );
    match result {
      CommandExecReturn::Loop(continue_loop) => {
        if !continue_loop {
          ret_none!();
        }
      }
      CommandExecReturn::Stack(_) | CommandExecReturn::NoReturn => (),
      _ => return result,
    }

    match utils::get_check_value(
      instance,
      asl,
      token_list_check.clone(),
      scope_list_check.clone(),
    ) {
      Ok(new_check) => check = new_check,
      Err(err) => ret_error_string!(err),
    }
  }
  ret_none!();
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(intr, "continue", [], Type::Permeable, send_loop_continue);
  def_func!(intr, "break", [], Type::Permeable, send_loop_break);
  def_multi_func!(
    intr,
    "for",
    [
      new_func!([Type::List, Type::Command], Type::Permeable, list_for),
      new_func!([Type::Vector, Type::Command], Type::Permeable, vector_for),
      new_func!([Type::Integer, Type::Command], Type::Permeable, number_for),
      new_func!(
        [Type::PrimitiveUnsigned, Type::Command],
        Type::Permeable,
        unsigned_for
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::Command],
        Type::Permeable,
        signed_for
      ),
      new_func!([Type::String, Type::Command], Type::Permeable, string_for),
      new_func!([Type::Raw, Type::Command], Type::Permeable, raw_for),
      new_func!([Type::Buffer, Type::Command], Type::Permeable, buffer_for),
      new_func!(
        [Type::Dictionary, Type::Command],
        Type::Permeable,
        dictionary_for
      ),
      new_func!([Type::Map, Type::Command], Type::Permeable, map_for),
    ]
  );
  def_func!(intr, "loop", [Type::Command], Type::Permeable, system_loop);
  def_func!(
    intr,
    "while",
    [Type::Command, Type::Command],
    Type::Permeable,
    while_loop
  );
}
