#[macro_use]
pub mod native_func_macros;
#[macro_use]
pub mod type_macros;
mod add_sub_mul;
mod boolean;
mod command_execution;
mod converters;
mod flow;
mod fs;
mod functions;
mod io;
mod lists_strings_raws;
mod loops;
mod numbers;
mod random;
mod system;
mod types;
mod utils;
use crate::instance::Instance;

pub fn load_native_functions(intr: &mut Instance) {
  system::load_native_functions(intr);
  functions::load_native_functions(intr);
  add_sub_mul::load_native_functions(intr);
  numbers::load_native_functions(intr);
  command_execution::load_native_functions(intr);
  flow::load_native_functions(intr);
  loops::load_native_functions(intr);
  lists_strings_raws::load_native_functions(intr);
  boolean::load_native_functions(intr);
  io::load_native_functions(intr);
  fs::load_native_functions(intr);
  converters::load_native_functions(intr);
  random::load_native_functions(intr);
}

pub fn load_native_type_functions(intr: &mut Instance) {
  types::load_native_functions(intr);
}
