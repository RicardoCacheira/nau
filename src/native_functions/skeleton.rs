use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "test" native functions

fn test(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(val_str_f!("test"));
}

// "test2" native functions

fn test2a(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::String(string!(params, 0).clone()));
}

fn test2b(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number!(params, 0).clone()));
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(intr, "test", [], Type::Any, test);
  def_multi_func!(
    intr,
    "test2",
    [
      new_func!([Type::String], Type::Any, test2a),
      new_func!([Type::Number], Type::Any, test2b),
    ]
  );
}
