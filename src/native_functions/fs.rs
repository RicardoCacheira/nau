use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "fs_read" native command

fn read_file_str(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.read_file_as_string(string!(params, 0)) {
    Ok(file) => ret_val!(Value::String(Rc::new(file))),
    Err(err) => ret_error_string!(err),
  };
}

fn read_file_to_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 1);
  match asl.fs.read_file(string!(params, 0)) {
    Ok(mut file) => {
      buffer.append(&mut file);
      ret_none!();
    }
    Err(err) => ret_error_string!(err),
  };
}

// "fs_read_to_raw" native command

fn read_file_raw(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.read_file(string!(params, 0)) {
    Ok(file) => ret_val!(Value::Raw(Rc::new(file))),
    Err(err) => ret_error_string!(err),
  };
}

// "fs_write" native command

fn write_file_str(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl
    .fs
    .write_file_as_string(string!(params, 0), string!(params, 1))
  {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

fn write_file_raw(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.write_file(string!(params, 0), raw!(params, 1)) {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

fn write_file_from_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 1);
  match asl.fs.write_file(string!(params, 0), &buffer) {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

// "fs_append" native command

fn append_file_str(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl
    .fs
    .append_file_as_string(string!(params, 0), string!(params, 1))
  {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

fn append_file_raw(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.append_file(string!(params, 0), raw!(params, 1)) {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

fn append_file_from_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 1);
  match asl.fs.append_file(string!(params, 0), &buffer) {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

// "fs_exists" native command

fn file_exists(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(asl.fs.file_exists(string!(params, 0))))
}

// "fs_is_file" native command

fn is_file(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(asl.fs.is_file(string!(params, 0))))
}

// "fs_is_dir" native command

fn is_dir(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(asl.fs.is_dir(string!(params, 0))))
}

// "fs_is_symlink" native command

fn is_symlink(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(asl.fs.is_symlink(string!(params, 0))))
}

// "fs_is_absolute" native command

fn is_absolute(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(asl.fs.is_absolute(string!(params, 0))))
}

// "fs_get_cwd" native command

fn get_cwd(_: &mut Instance, asl: &ASL, _: Vec<Value>) -> CommandExecReturn {
  match asl.fs.get_cwd() {
    Ok(path) => ret_val!(Value::String(Rc::new(path))),
    Err(err) => ret_error_string!(err),
  };
}

// "fs_set_cwd" native command

fn set_cwd(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.set_cwd(string!(params, 0).as_ref()) {
    Ok(_) => ret_none!(),
    Err(err) => ret_error_string!(err),
  };
}

// "fs_list_dir" native command

fn list_dir(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.fs.list_dir(string!(params, 0).as_ref()) {
    Ok(path_list) => ret_val!(Value::List(Rc::new(
      path_list
        .into_iter()
        .map(|path| { Value::String(Rc::new(path)) })
        .collect()
    ))),
    Err(err) => ret_error_string!(err),
  };
}

pub fn load_native_functions(intr: &mut Instance) {
  def_multi_func!(
    intr,
    "fs_read",
    [
      new_func!(
        [Type::String, Type::Buffer],
        Type::Void,
        read_file_to_buffer
      ),
      new_func!([Type::String], Type::String, read_file_str)
    ]
  );
  def_func!(
    intr,
    "fs_read_to_raw",
    [Type::String],
    Type::Raw,
    read_file_raw
  );
  def_multi_func!(
    intr,
    "fs_write",
    [
      new_func!([Type::String, Type::Raw], Type::Void, write_file_raw),
      new_func!(
        [Type::String, Type::Buffer],
        Type::Void,
        write_file_from_buffer
      ),
      new_func!([Type::String, Type::String], Type::Void, write_file_str)
    ]
  );
  def_multi_func!(
    intr,
    "fs_append",
    [
      new_func!([Type::String, Type::Raw], Type::Void, append_file_raw),
      new_func!(
        [Type::String, Type::Buffer],
        Type::Void,
        append_file_from_buffer
      ),
      new_func!([Type::String, Type::String], Type::Void, append_file_str)
    ]
  );
  def_func!(
    intr,
    "fs_exists",
    [Type::String],
    Type::Boolean,
    file_exists
  );
  def_func!(intr, "fs_is_file", [Type::String], Type::Boolean, is_file);
  def_func!(intr, "fs_is_dir", [Type::String], Type::Boolean, is_dir);
  def_func!(
    intr,
    "fs_is_symlink",
    [Type::String],
    Type::Boolean,
    is_symlink
  );
  def_func!(
    intr,
    "fs_is_absolute",
    [Type::String],
    Type::Boolean,
    is_absolute
  );
  def_func!(intr, "fs_get_cwd", [], Type::String, get_cwd);
  def_func!(intr, "fs_set_cwd", [Type::String], Type::Void, set_cwd);
  def_func!(
    intr,
    "fs_list_dir",
    [Type::String],
    ListOf!(Type::String),
    list_dir
  );
}
