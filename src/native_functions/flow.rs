use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::native_functions::utils;
use crate::parsers::Token;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "return" native functions

fn return_value(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(params[0].clone());
}

// "end" native functions

fn send_end(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  CommandExecReturn::End
}

// "throw" native functions

fn throw_error(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_error_string!(String::clone(string!(params, 0)))
}

// "exit" native functions

fn send_exit(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  CommandExecReturn::Exit(unsigned!(params, 0))
}

// "try" native functions

fn try_command(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list_body, id_list_body) = command!(params, 0);
  let (token_list_catch, id_list_catch) = command!(params, 1);

  let result =
    CommandExec::run_command(instance, asl, token_list_body.clone(), id_list_body.clone());
  match result {
    CommandExecReturn::Error(err) => {
      return CommandExec::run_command_with_params(
        instance,
        asl,
        token_list_catch.clone(),
        id_list_catch.clone(),
        vec![Value::String(Rc::new(err))],
      );
    }
    _ => return result,
  }
}

// "if" native functions

fn boolean_if_command(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  if boolean!(params, 0) {
    let (token_list, id_list) = command!(params, 1);
    return CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
  } else {
    ret_none!();
  }
}

fn command_if_command(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list_check, id_list_check) = command!(params, 0);
  match utils::get_check_value(
    instance,
    asl,
    token_list_check.clone(),
    id_list_check.clone(),
  ) {
    Ok(check) => {
      if check {
        let (token_list, id_list) = command!(params, 1);
        return CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
      } else {
        ret_none!();
      }
    }
    Err(err) => ret_error_string!(err),
  }
}

// "ifelse" native functions

fn if_else_command(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let com_list = list!(params, 0);
  //the & -2 is a way to force the size to the even number before it if its odd or keep it as is if even
  //this works because -2 in binary is all 1 besides the least significant digit
  for i in (0..(com_list.len() & (-2_isize) as usize)).step_by(2) {
    if let Value::Command {
      token_list: token_list_check,
      id_list: id_list_check,
    } = &com_list[i]
    {
      match utils::get_check_value(
        instance,
        asl,
        token_list_check.clone(),
        id_list_check.clone(),
      ) {
        Ok(check) => {
          if check {
            if let Value::Command {
              token_list: token_list_body,
              id_list: id_list_body,
            } = &com_list[i + 1]
            {
              return CommandExec::run_command(
                instance,
                asl,
                token_list_body.clone(),
                id_list_body.clone(),
              );
            } else {
              ret_error_str!("Given body is not a command");
            }
          }
        }
        Err(err) => ret_error_string!(err),
      }
    } else {
      ret_error_str!("Given check is not a command");
    }
  }
  if com_list.len() & 1 == 1 {
    if let Value::Command {
      token_list: token_list_body,
      id_list: id_list_body,
    } = &com_list[com_list.len() - 1]
    {
      return CommandExec::run_command(
        instance,
        asl,
        token_list_body.clone(),
        id_list_body.clone(),
      );
    } else {
      ret_error_str!("Given default body is not a command");
    }
  } else {
    ret_none!();
  }
}

// "switch" native functions

//
// This version is commented out because the multiple/Many detector currently doesn't allow any values after an succesful list that are also part of the list
// Multiple(String Number) String wont work because the Multiple will try to grab any String after it

fn generic_switch_command(
  instance: &mut Instance,
  asl: &ASL,
  switch_value: &Value,
  value_list: &Vec<Value>,
  default_command: Option<(Rc<Vec<Token>>, Rc<Vec<usize>>)>,
) -> CommandExecReturn {
  for i in (0..value_list.len()).step_by(2) {
    if switch_value == &value_list[i] {
      if let Value::Command {
        token_list,
        id_list,
      } = &value_list[i + 1]
      {
        return CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
      } else {
        ret_error_str!("Given body is not a command");
      }
    }
  }
  if let Some((token_list, id_list)) = default_command {
    return CommandExec::run_command(instance, asl, token_list.clone(), id_list.clone());
  } else {
    ret_none!();
  }
}

fn without_default_switch(
  instance: &mut Instance,
  asl: &ASL,

  params: Vec<Value>,
) -> CommandExecReturn {
  generic_switch_command(instance, asl, &params[0], list!(params, 1), None)
}

fn with_default_switch(
  instance: &mut Instance,
  asl: &ASL,

  params: Vec<Value>,
) -> CommandExecReturn {
  let (token_list, id_list) = command!(params, 2);
  generic_switch_command(
    instance,
    asl,
    &params[0],
    list!(params, 1),
    Some((token_list.clone(), id_list.clone())),
  )
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(intr, "return", [Type::Any], Type::Permeable, return_value);
  def_func!(intr, "end", [], Type::Permeable, send_end);
  def_func!(
    intr,
    "exit",
    [Type::PrimitiveUnsigned],
    Type::Void,
    send_exit
  );
  def_func!(intr, "throw", [Type::String], Type::Void, throw_error);
  def_func!(
    intr,
    "try",
    [Type::Command, Type::Command],
    Type::Permeable,
    try_command
  );
  def_multi_func!(
    intr,
    "if",
    [
      new_func!(
        [Type::Boolean, Type::Command],
        Type::Permeable,
        boolean_if_command
      ),
      new_func!(
        [Type::Command, Type::Command],
        Type::Permeable,
        command_if_command
      )
    ]
  );
  def_func!(
    intr,
    "ifelse",
    [Many!(Type::Command)],
    Type::Permeable,
    if_else_command
  );
  def_multi_func!(
    intr,
    "switch",
    [
      new_func!(
        [Type::Any, Many!(Type::Any, Type::Command)],
        Type::Permeable,
        without_default_switch
      ),
      new_func!(
        [Type::Any, Many!(Type::Any, Type::Command), Type::Command],
        Type::Permeable,
        with_default_switch
      )
    ]
  );
}
