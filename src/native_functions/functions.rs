use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::scopes::ScopeId;
use crate::utils::base_utils;
use crate::values::{function, FunctionType, SystemType as Type, Value};
use std::rc::Rc;

// "fn" native functions

fn create_func(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let return_type = system_type!(params, 0);
  let param_and_id_list = list!(params, 1);
  let base_start_value_id_list = list!(params, 2);
  let (com_token_list, com_id_list) = command!(params, 3);

  let mut param_list = Vec::new();
  let mut param_id_list = Vec::new();
  let mut is_reading_type = true;
  for val in param_and_id_list.iter() {
    if is_reading_type {
      param_list
        .push(val_to_system_type!(val, "Non type value received in the param types.").clone());
    } else {
      if let Value::Reference(ScopeId::Block(id)) = val {
        param_id_list.push(id.clone());
      } else {
        ret_error_str!("Non scope reference received in the the params.")
      }
    }
    is_reading_type = !is_reading_type;
  }

  match function::create_command_function(
    instance,
    return_type.clone(),
    Rc::new(param_list),
    Rc::new(param_id_list),
    base_start_value_id_list.clone(),
    com_token_list.clone(),
    com_id_list.clone(),
  ) {
    Ok(func) => ret_val!(func),
    Err(err) => ret_error_string!(err),
  }
}

// "meta" native functions

fn create_meta(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let return_type = system_type!(params, 0);
  let input_com_var_id = block_reference!(params, 1);
  let base_start_value_id_list = list!(params, 2);
  let (com_token_list, com_id_list) = command!(params, 3);

  match function::create_meta_function(
    instance,
    return_type.clone(),
    input_com_var_id.clone(),
    base_start_value_id_list.clone(),
    com_token_list.clone(),
    com_id_list.clone(),
  ) {
    Ok(func) => ret_val!(func),
    Err(err) => ret_error_string!(err),
  }
}

// "mfn" native functions

fn create_multi_func(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let funcs = list!(params, 0);
  match function::create_multi_function(funcs.clone()) {
    Ok(mfunc) => ret_val!(mfunc),
    Err(err) => ret_error_string!(err),
  }
}

// "fn_params" native functions

fn get_function_params(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut res = Vec::new();
  match &params[0] {
    Value::Function { param_list, .. } => {
      for p in param_list.iter() {
        res.push(Value::Type(p.clone()))
      }
    }
    Value::Meta { .. } => res.push(Value::Type(Type::Command)),
    _ => ret_error_str!("Non single function or meta function value received."),
  }
  ret_val!(Value::List(Rc::new(res)));
}

// "fn_param_names" native functions

fn get_function_param_names(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let mut aux = Vec::new();
  match &params[0] {
    Value::Function {
      param_list,
      function_type: FunctionType::Native { .. },
      ..
    } => {
      for (i, _) in param_list.iter().enumerate() {
        aux.push(Value::String(Rc::new(base_utils::num_to_param_str(i))))
      }
    }
    Value::Function {
      function_type: FunctionType::Command { param_id_list, .. },
      ..
    } => {
      let block_manager = instance.get_block_id_manager();
      for id in param_id_list.iter() {
        aux.push(Value::String(Rc::new(block_manager.id_to_name(id))))
      }
    }
    Value::Meta { .. } => aux.push(Value::String(Rc::new(base_utils::num_to_param_str(0)))),
    _ => ret_error_str!("Non single function or meta function value received."),
  }
  ret_val!(Value::List(Rc::new(aux)))
}

// "fn_return" native functions

fn get_function_return(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (_, return_type, _) = function!(params, 0);
  ret_val!(Value::Type(return_type.clone()));
}

fn get_meta_return(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (return_type, _, _, _, _, _) = meta!(params, 0);
  ret_val!(Value::Type(return_type.clone()));
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(
    intr,
    "fn",
    [
      Type::Type,
      Multiple!(Type::ParamType, Type::BlockReference),
      Multiple!(Type::BlockReference),
      Type::Command
    ],
    Type::CommandFunction,
    create_func
  );
  def_func!(
    intr,
    "meta",
    [
      Type::Type,
      Type::BlockReference,
      Multiple!(Type::BlockReference),
      Type::Command
    ],
    Type::Meta,
    create_meta
  );
  def_func!(
    intr,
    "mfn",
    [Type::List],
    Type::MultiFunction,
    create_multi_func
  );
  def_func!(
    intr,
    "fn_params",
    [Or!(Type::SingleFunction, Type::Meta)],
    ListOf!(Type::ParamType),
    get_function_params
  );
  def_func!(
    intr,
    "fn_param_names",
    [Or!(Type::SingleFunction, Type::Meta)],
    ListOf!(Type::String),
    get_function_param_names
  );
  def_multi_func!(
    intr,
    "fn_return",
    [
      new_func!([Type::SingleFunction], Type::Type, get_function_return),
      new_func!([Type::Meta], Type::Type, get_meta_return)
    ]
  );
}
