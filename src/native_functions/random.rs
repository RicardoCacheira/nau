use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::utils::number_utils;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "random" native functions

fn random_num(instance: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(instance.get_rng().generate()));
}

fn random_num_max(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let max = match number_utils::value_to_u64(&params[0]) {
    Some(val) => val,
    None => ret_error_str!("Maximum must be able to be converted to a valid 64-bit unsigned."),
  };
  ret_val!(Value::Unsigned(instance.get_rng().generate_to_max(max)));
}

fn random_num_range(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let min = match number_utils::value_to_u64(&params[0]) {
    Some(val) => val,
    None => ret_error_str!("Minimum must be able to be converted to a valid 64-bit unsigned."),
  };
  let max = match number_utils::value_to_u64(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Maximum must be able to be converted to a valid 64-bit unsigned."),
  };
  if min < max {
    ret_val!(Value::Unsigned(
      instance.get_rng().generate_between(min, max + 1)
    ));
  }
  ret_error_str!("Maximum must be greater than Minimum.");
}

// "random_alfa" native functions

fn random_alfa(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let size = match number_utils::get_unsigned(&params[0]) {
    Some(val) => val,
    None => ret_error_str!("Size must be able to be converted to a system unsigned number"),
  };

  ret_val!(Value::String(Rc::new(
    instance
      .get_rng()
      .generate_alfa(size)
  )));
}

// "set_random_seed" native functions

fn set_random_seed(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.get_rng().seed(unsigned!(params, 0));
  ret_none!();
}

pub fn load_native_functions(intr: &mut Instance) {
  def_multi_func!(
    intr,
    "random",
    [
      new_func!([], Type::PrimitiveUnsigned, random_num),
      new_func!([Type::AnyNumber], Type::PrimitiveUnsigned, random_num_max),
      new_func!(
        [Type::AnyNumber, Type::AnyNumber],
        Type::PrimitiveUnsigned,
        random_num_range
      ),
    ]
  );
  def_func!(
    intr,
    "random_alfa",
    [Type::AnyNumber],
    Type::String,
    random_alfa
  );
  def_func!(
    intr,
    "set_random_seed",
    [Type::PrimitiveUnsigned],
    Type::Void,
    set_random_seed
  );
}
