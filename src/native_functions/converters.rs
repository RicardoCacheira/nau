use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::parsers::number_parser;
use crate::utils::to_system_string::ToSystemString;
use crate::utils::{base_utils, number_utils};
use crate::values::{function, SystemType as Type, Value, ValueMap};
use std::char;
use std::convert::TryInto;
use std::rc::Rc;

// "to_unsigned" native functions

fn n_to_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match number_utils::rational_to_u64(number!(params, 0)) {
    Some(u) => ret_val!(Value::Unsigned(u)),
    None => ret_error_str!("The number is not a valid primitive unsigned"),
  }
}

fn s_to_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(signed!(params, 0) as u64))
}

fn r_to_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(u64::from_le_bytes(
    base_utils::vec_8u_to_u8_8_array(raw!(params, 0))
  )))
}

// "to_signed" native functions

fn n_to_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match number_utils::rational_to_i64(number!(params, 0)) {
    Some(i) => ret_val!(Value::Signed(i)),
    None => ret_error_str!("The number is not a valid primitive signed"),
  }
}

fn u_to_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(unsigned!(params, 0) as i64))
}

fn r_to_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(i64::from_le_bytes(
    base_utils::vec_8u_to_u8_8_array(raw!(params, 0))
  )))
}

// "to_float" native functions

fn r_to_f(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Float(f64::from_le_bytes(
    base_utils::vec_8u_to_u8_8_array(raw!(params, 0))
  )))
}

// to_number" native functions

fn u_to_n(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::u64_to_rational(unsigned!(
    params, 0
  ))))
}

fn s_to_n(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::i64_to_rational(signed!(
    params, 0
  ))))
}

fn f_to_n(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match number_utils::f64_to_rational(float!(params, 0)) {
    Some(r) => ret_val!(Value::Number(r)),
    None => ret_error_str!("The float is not a valid rational number"),
  }
}

fn parse_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let input = string!(params, 0);
  match number_parser::parse_number(input) {
    Ok(number_parser::ParsedNumber::Rational(value)) => ret_val!(Value::Number(value)),
    _ => ret_error_string!(format!("'{}' is not a valid number", input)),
  }
}

// "to_string" native command

fn to_string(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  if let Value::String(_) = params[0] {
    ret_val!(params[0].clone());
  }
  ret_val!(Value::String(Rc::new(params[0].to_string(instance, asl))));
}

// "raw_to_string" native command

fn raw_to_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match String::from_utf8(Vec::clone(raw!(params, 0))) {
    Ok(s) => ret_val!(Value::String(Rc::new(s))),
    Err(err) => ret_error_string!(err.to_string()),
  }
}

// "to_token_string" native command

fn to_token_string(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::String(Rc::new(
    params[0].to_token_string(instance, asl)
  )));
}

// "to_list" native functions

fn to_list_multi_func(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let func_list = multi_function!(params, 0);
  let mut res = Vec::with_capacity(func_list.len());
  for f in func_list.iter() {
    res.push(f.clone());
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn to_string_char_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let s = string!(params, 0);
  let mut res = Vec::with_capacity(s.len());
  for c in s.chars() {
    let mut char_string = String::new();
    char_string.push(c);
    res.push(Value::String(Rc::new(char_string)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn to_string_char_list_sized(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let s = string!(params, 0);
  let size = number_utils::rational_to_usize(number!(params, 1)).unwrap();
  let mut res = Vec::with_capacity(s.len() / size + if s.len() % size == 0 { 0 } else { 1 });
  let mut aux = String::with_capacity(size);
  for c in s.chars() {
    aux.push(c.clone());
    if aux.len() == size {
      res.push(Value::String(Rc::new(aux)));
      aux = String::with_capacity(size);
    }
  }
  if !aux.is_empty() {
    res.push(Value::String(Rc::new(aux)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn vector_to_list(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::List(Rc::new(vector!(instance, params, 0).clone())));
}

fn dictionary_to_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(dictionary!(params, 0).to_value_list())
}

fn map_to_list(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(map!(instance, params, 0).to_value_list());
}

fn raw_to_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let mut res = Vec::with_capacity(raw.len());
  for b in raw.iter() {
    let mut byte_vec = Vec::new();
    byte_vec.push(b.clone());
    res.push(Value::Raw(Rc::new(byte_vec)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn raw_to_list_sized(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let size = number_utils::rational_to_usize(number!(params, 1)).unwrap();
  let mut res = Vec::with_capacity(raw.len() / size + if raw.len() % size == 0 { 0 } else { 1 });
  let mut byte_vec = Vec::with_capacity(size);
  for b in raw.iter() {
    byte_vec.push(b.clone());
    if byte_vec.len() == size {
      res.push(Value::Raw(Rc::new(byte_vec)));
      byte_vec = Vec::with_capacity(size);
    }
  }
  if !byte_vec.is_empty() {
    res.push(Value::Raw(Rc::new(byte_vec)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn buffer_to_list(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let mut res = Vec::with_capacity(buffer.len());
  for b in buffer.iter() {
    let mut byte_vec = Vec::new();
    byte_vec.push(b.clone());
    res.push(Value::Raw(Rc::new(byte_vec)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn buffer_to_list_sized(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let size = number_utils::rational_to_usize(number!(params, 1)).unwrap();
  let mut res =
    Vec::with_capacity(buffer.len() / size + if buffer.len() % size == 0 { 0 } else { 1 });
  let mut byte_vec = Vec::with_capacity(size);
  for b in buffer.iter() {
    byte_vec.push(b.clone());
    if byte_vec.len() == size {
      res.push(Value::Raw(Rc::new(byte_vec)));
      byte_vec = Vec::with_capacity(size);
    }
  }
  if !byte_vec.is_empty() {
    res.push(Value::Raw(Rc::new(byte_vec)));
  }
  ret_val!(Value::List(Rc::new(res)))
}

// "to_dictionary" native functions

fn map_to_dictionary(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Dictionary(Rc::new(
    map!(instance, params, 0).clone()
  )));
}

fn list_to_dictionary(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Dictionary(Rc::new(
    ValueMap::from_value_list(instance, asl, list!(params, 0)).unwrap()
  )))
}

// "to_raw" native functions

fn buffer_to_raw(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(buffer!(instance, params, 0).clone())));
}

fn string_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(
    String::clone(string!(params, 0)).into_bytes()
  )));
}

fn unsigned_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(
    unsigned!(params, 0).to_le_bytes().to_vec().clone()
  )));
}

fn signed_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(
    signed!(params, 0).to_le_bytes().to_vec().clone()
  )));
}

fn float_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(
    float!(params, 0).to_le_bytes().to_vec().clone()
  )));
}

fn boolean_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Raw(Rc::new(vec![boolean!(params, 0) as u8])));
}

// "to_vec" native functions

fn list_to_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = instance.create_new_vector_from_list(list!(params, 0));
  ret_val!(Value::Vector(id));
}

// "to_map" native functions

fn dictionary_to_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = instance.create_new_map_from_dict(dictionary!(params, 0));
  ret_val!(Value::Map(id));
}

// "to_buffer" native functions

fn raw_to_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = instance.create_new_buffer_from_raw(raw!(params, 0));
  ret_val!(Value::Buffer(id));
}

// "charcode_to_string" native functions

fn charcode_to_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut res = String::new();
  for v in list!(params, 0).iter() {
    if let Value::Unsigned(u) = v {
      match u.clone().try_into() {
        Ok(val) => match char::from_u32(val) {
          Some(c) => res.push(c),
          None => ret_error_string!(format!("Value '{}' is not a valiug utf-8 character", u)),
        },
        Err(_) => ret_error_string!(format!("Value '{}' is not a valiug utf-8 character", u)),
      }
    }
  }
  ret_val!(Value::String(Rc::new(res)));
}

// "string_to_charcode" native functions

fn string_to_charcode(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut res = Vec::new();
  for c in string!(params, 0).chars() {
    res.push(Value::Unsigned(c as u64));
  }
  ret_val!(Value::List(Rc::new(res)));
}

// "new" native functions

fn new_wrapped_value(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match Value::wrap_value(instance, asl, system_type!(params, 0), params[1].clone()) {
    Ok(value) => ret_val!(value),
    Err(err) => ret_error_string!(err),
  }
}

// "to_value" native functions

fn wrapped_to_value(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(wrapped_value!(params, 0).as_ref().clone());
}

pub fn load_native_functions(intr: &mut Instance) {
  def_multi_func!(
    intr,
    "to_unsigned",
    [
      new_func!([Type::Number], Type::PrimitiveUnsigned, n_to_u),
      new_func!([Type::PrimitiveSigned], Type::PrimitiveUnsigned, s_to_u),
      new_func!([Type::Raw], Type::PrimitiveUnsigned, r_to_u),
    ]
  );
  def_multi_func!(
    intr,
    "to_signed",
    [
      new_func!([Type::Number], Type::PrimitiveSigned, n_to_s),
      new_func!([Type::PrimitiveUnsigned], Type::PrimitiveSigned, u_to_s),
      new_func!([Type::Raw], Type::PrimitiveSigned, r_to_s),
    ]
  );
  def_func!(intr, "to_float", [Type::Raw], Type::PrimitiveFloat, r_to_f);
  def_multi_func!(
    intr,
    "to_number",
    [
      new_func!([Type::PrimitiveUnsigned], Type::SystemUnsigned, u_to_n),
      new_func!([Type::PrimitiveSigned], Type::Integer, s_to_n),
      new_func!([Type::PrimitiveFloat], Type::Number, f_to_n),
      new_func!([Type::String], Type::Number, parse_num),
    ]
  );
  def_func!(intr, "to_string", [Type::Any], Type::String, to_string);
  def_func!(
    intr,
    "raw_to_string",
    [Type::Raw],
    Type::String,
    raw_to_string
  );
  def_func!(
    intr,
    "to_token_string",
    [Type::Any],
    Type::String,
    to_token_string
  );
  def_multi_func!(
    intr,
    "to_list",
    [
      new_func!([Type::MultiFunction], Type::List, to_list_multi_func),
      new_func!([Type::String], Type::List, to_string_char_list),
      new_func!([Type::Vector], Type::List, vector_to_list),
      new_func!([Type::Dictionary], Type::List, dictionary_to_list),
      new_func!([Type::Map], Type::List, map_to_list),
      new_func!([Type::Raw], Type::List, raw_to_list),
      new_func!([Type::Buffer], Type::List, buffer_to_list),
      new_func!(
        [Type::String, Type::Natural],
        Type::List,
        to_string_char_list_sized
      ),
      new_func!([Type::Raw, Type::Natural], Type::List, raw_to_list_sized),
      new_func!(
        [Type::Buffer, Type::Natural],
        Type::List,
        buffer_to_list_sized
      ),
    ]
  );
  def_multi_func!(
    intr,
    "to_dictionary",
    [
      new_func!([Type::Map], Type::Dictionary, map_to_dictionary),
      new_func!(
        [Sequence!(Type::String, Type::Any)],
        Type::Dictionary,
        list_to_dictionary
      ),
    ]
  );
  def_multi_func!(
    intr,
    "to_raw",
    [
      new_func!([Type::Buffer], Type::Raw, buffer_to_raw),
      new_func!([Type::String], Type::Raw, string_to_raw),
      new_func!([Type::PrimitiveUnsigned], Type::Raw, unsigned_to_raw),
      new_func!([Type::PrimitiveSigned], Type::Raw, signed_to_raw),
      new_func!([Type::PrimitiveFloat], Type::Raw, float_to_raw),
      new_func!([Type::Boolean], Type::Raw, boolean_to_raw),
    ]
  );
  def_func!(intr, "to_vec", [Type::List], Type::Vector, list_to_vector);
  def_func!(
    intr,
    "to_map",
    [Type::Dictionary],
    Type::Map,
    dictionary_to_map
  );
  def_func!(intr, "to_buffer", [Type::Raw], Type::Buffer, raw_to_buffer);
  def_func!(
    intr,
    "string_to_charcode",
    [Type::String],
    ListOf!(Type::PrimitiveUnsigned),
    string_to_charcode
  );
  def_func!(
    intr,
    "charcode_to_string",
    [ListOf!(Type::PrimitiveUnsigned)],
    Type::String,
    charcode_to_string
  );
  def_func!(
    intr,
    "new",
    [Type::CustomType, Type::Any],
    Type::WrappedValue,
    new_wrapped_value
  );
  def_func!(
    intr,
    "to_value",
    [Type::WrappedValue],
    Type::Any,
    wrapped_to_value
  );
}
