#[macro_export]
macro_rules! Multiple {
  ($($types:expr),* $(,)*) => {
      Type::Multiple(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! Many {
  ($($types:expr),* $(,)*) => {
      Type::Many(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! Set {
  ($($types:expr),* $(,)*) => {
    Type::Set(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! ListOf {
  ($($types:expr),* $(,)*) => {
    Type::ListOf(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! VectorOf {
  ($($types:expr),* $(,)*) => {
    Type::VectorOf(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! DictionaryOf {
  ($($types:expr),* $(,)*) => {
    Type::DictionaryOf(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! MapOf {
  ($($types:expr),* $(,)*) => {
    Type::MapOf(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! Sequence {
  ($($types:expr),* $(,)*) => {
    Type::Sequence(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! Or {
  ($($types:expr),* $(,)*) => {
    Type::Or(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! And {
  ($($types:expr),* $(,)*) => {
    Type::And(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! RefOf {
  ($type:expr) => {
    Type::RefOf(Rc::new($type))
  };
}

#[macro_export]
macro_rules! ValueOption {
  ($($values:expr),* $(,)*) => {
    Type::ValueOption(Rc::new(vec![$($values,)*]))
  };
}

#[macro_export]
macro_rules! SubType {
  ($($types:expr),* $(,)*) => {
    Type::SubType(Rc::new(vec![$($types,)*]))
  };
}

#[macro_export]
macro_rules! Not {
  ($t:expr) => {
    Type::Not(Rc::new($t))
  };
}
