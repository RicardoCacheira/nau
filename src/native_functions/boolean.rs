use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::utils::number_utils;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "eq" native functions

fn is_eq(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(params[0] == params[1]));
}

// "ne" native functions

fn is_not_eq(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(params[0] != params[1]));
}

// "lt" native functions

fn is_less_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(number!(params, 0) < number!(params, 1)));
}

fn is_less_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(unsigned!(params, 0) < unsigned!(params, 1)));
}

fn is_less_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(signed!(params, 0) < signed!(params, 1)));
}

fn is_less_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(float!(params, 0) < float!(params, 1)));
}

fn is_less_str(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(string!(params, 0) < string!(params, 1)));
}

// "le" native functions

fn is_less_or_eq_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(number!(params, 0) <= number!(params, 1)));
}

fn is_less_or_eq_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(unsigned!(params, 0) <= unsigned!(params, 1)));
}

fn is_less_or_eq_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(signed!(params, 0) <= signed!(params, 1)));
}

fn is_less_or_eq_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(float!(params, 0) <= float!(params, 1)));
}

fn is_less_or_eq_str(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(string!(params, 0) <= string!(params, 1)));
}

// "gt" native functions

fn is_greater_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(number!(params, 0) > number!(params, 1)));
}

fn is_greater_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(unsigned!(params, 0) > unsigned!(params, 1)));
}

fn is_greater_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(signed!(params, 0) > signed!(params, 1)));
}

fn is_greater_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(float!(params, 0) > float!(params, 1)));
}

fn is_greater_str(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(string!(params, 0) > string!(params, 1)));
}

// "ge" native functions

fn is_greater_or_eq_num(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(number!(params, 0) >= number!(params, 1)));
}

fn is_greater_or_eq_unsigned(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(unsigned!(params, 0) >= unsigned!(params, 1)));
}

fn is_greater_or_eq_signed(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(signed!(params, 0) >= signed!(params, 1)));
}

fn is_greater_or_eq_float(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(float!(params, 0) >= float!(params, 1)));
}

fn is_greater_or_eq_str(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(string!(params, 0) >= string!(params, 1)));
}

// "and" native functions

fn boolean_and(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = boolean!(params, 0);
  let b = boolean!(params, 1);
  ret_val!(Value::Boolean(a && b));
}

fn raw_and(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 1);
  let a_size = a.len();
  let b_size = b.len();
  let min;
  let max;
  if b_size > a_size {
    min = a_size;
    max = b_size;
  } else {
    min = b_size;
    max = a_size;
  };
  let mut res = Vec::with_capacity(max);
  for i in 0..min {
    res.push(a[i] & b[i]);
  }
  for _ in min..max {
    res.push(0);
  }
  ret_val!(Value::Raw(Rc::new(res)));
}

fn buffer_and(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 1);
  let buffer_size = buffer.len();
  let raw_size = raw.len();
  let raw_is_greater = raw_size > buffer_size;
  let common_max = if raw_is_greater {
    buffer_size
  } else {
    raw_size
  };
  for i in 0..common_max {
    buffer[i] = buffer[i] & raw[i];
  }
  if raw_is_greater {
    for _ in buffer_size..raw_size {
      buffer.push(0);
    }
  } else {
    for i in raw_size..buffer_size {
      buffer[i] = 0;
    }
  }
  ret_none!();
}

fn raw_and_offset(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 2);
  match number_utils::get_pos(a, &params[1]) {
    Some(offset) => {
      let a_size = a.len();
      if offset < a_size {
        let mut max = b.len() + offset;
        if max > a_size {
          max = a_size;
        }
        let mut res = Vec::with_capacity(a_size);
        for i in 0..offset {
          res.push(a[i]);
        }
        for i in offset..max {
          res.push(a[i] & b[i - offset]);
        }
        if max < a_size {
          for i in max..a_size {
            res.push(a[i]);
          }
        }
        ret_val!(Value::Raw(Rc::new(res)));
      } else {
        ret_val!(params[0].clone())
      }
    }
    None => ret_val!(params[0].clone()),
  }
}

fn buffer_and_offset(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 2);
  if let Some(offset) = number_utils::get_pos(buffer, &params[1]) {
    let buffer_size = buffer.len();
    if offset < buffer_size {
      let mut max = raw.len() + offset;
      if max > buffer_size {
        max = buffer_size;
      }
      for i in offset..max {
        buffer[i] = buffer[i] & raw[i - offset];
      }
    }
  }
  ret_none!();
}

// "or" native functions

fn boolean_or(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = boolean!(params, 0);
  let b = boolean!(params, 1);
  ret_val!(Value::Boolean(a || b));
}

fn raw_or(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 1);
  let a_size = a.len();
  let b_size = b.len();
  let b_is_greater = b_size > a_size;
  let common_max = if b_is_greater { a_size } else { b_size };
  let mut res = Vec::with_capacity(if b_is_greater { b_size } else { a_size });
  for i in 0..common_max {
    res.push(a[i] | b[i]);
  }
  if b_is_greater {
    for i in a_size..b_size {
      res.push(b[i].clone());
    }
  } else {
    for i in b_size..a_size {
      res.push(a[i].clone());
    }
  }
  ret_val!(Value::Raw(Rc::new(res)));
}

fn buffer_or(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 1);
  let buffer_size = buffer.len();
  let raw_size = raw.len();
  let raw_is_greater = raw_size > buffer_size;
  let common_max = if raw_is_greater {
    buffer_size
  } else {
    raw_size
  };
  for i in 0..common_max {
    buffer[i] = buffer[i] | raw[i];
  }
  if raw_is_greater {
    for i in buffer_size..raw_size {
      buffer.push(raw[i].clone());
    }
  }
  ret_none!();
}

fn raw_or_offset(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 2);
  match number_utils::get_pos(a, &params[1]) {
    Some(offset) => {
      let a_size = a.len();
      if offset < a_size {
        let mut max = b.len() + offset;
        if max > a_size {
          max = a_size;
        }
        let mut res = Vec::with_capacity(a_size);
        for i in 0..offset {
          res.push(a[i]);
        }
        for i in offset..max {
          res.push(a[i] | b[i - offset]);
        }
        if max < a_size {
          for i in max..a_size {
            res.push(a[i]);
          }
        }
        ret_val!(Value::Raw(Rc::new(res)));
      } else {
        ret_val!(params[0].clone())
      }
    }
    None => ret_val!(params[0].clone()),
  }
}

fn buffer_or_offset(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 2);
  if let Some(offset) = number_utils::get_pos(buffer, &params[1]) {
    let buffer_size = buffer.len();
    if offset < buffer_size {
      let mut max = raw.len() + offset;
      if max > buffer_size {
        max = buffer_size;
      }
      for i in offset..max {
        buffer[i] = buffer[i] | raw[i - offset];
      }
    }
  }
  ret_none!();
}

// "xor" native functions

fn boolean_xor(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = boolean!(params, 0);
  let b = boolean!(params, 1);
  ret_val!(Value::Boolean(a ^ b));
}

fn raw_xor(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 1);
  let a_size = a.len();
  let b_size = b.len();
  let b_is_greater = b_size > a_size;
  let common_max = if b_is_greater { a_size } else { b_size };
  let mut res = Vec::with_capacity(if b_is_greater { b_size } else { a_size });
  for i in 0..common_max {
    res.push(a[i] ^ b[i]);
  }
  if b_is_greater {
    for i in a_size..b_size {
      res.push(b[i].clone());
    }
  } else {
    for i in b_size..a_size {
      res.push(a[i].clone());
    }
  }
  ret_val!(Value::Raw(Rc::new(res)));
}

fn buffer_xor(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 1);
  let buffer_size = buffer.len();
  let raw_size = raw.len();
  let raw_is_greater = raw_size > buffer_size;
  let common_max = if raw_is_greater {
    buffer_size
  } else {
    raw_size
  };
  for i in 0..common_max {
    buffer[i] = buffer[i] ^ raw[i];
  }
  if raw_is_greater {
    for i in buffer_size..raw_size {
      buffer.push(raw[i].clone());
    }
  }
  ret_none!();
}

fn raw_xor_offset(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 2);
  match number_utils::get_pos(a, &params[1]) {
    Some(offset) => {
      let a_size = a.len();
      if offset < a_size {
        let mut max = b.len() + offset;
        if max > a_size {
          max = a_size;
        }
        let mut res = Vec::with_capacity(a_size);
        for i in 0..offset {
          res.push(a[i]);
        }
        for i in offset..max {
          res.push(a[i] ^ b[i - offset]);
        }
        if max < a_size {
          for i in max..a_size {
            res.push(a[i]);
          }
        }
        ret_val!(Value::Raw(Rc::new(res)));
      } else {
        ret_val!(params[0].clone())
      }
    }
    None => ret_val!(params[0].clone()),
  }
}

fn buffer_xor_offset(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let raw = raw!(params, 2);
  if let Some(offset) = number_utils::get_pos(buffer, &params[1]) {
    let buffer_size = buffer.len();
    if offset < buffer_size {
      let mut max = raw.len() + offset;
      if max > buffer_size {
        max = buffer_size;
      }
      for i in offset..max {
        buffer[i] = buffer[i] ^ raw[i - offset];
      }
    }
  }
  ret_none!();
}

// "not" native functions

fn boolean_not(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = boolean!(params, 0);
  ret_val!(Value::Boolean(!a));
}

fn raw_not(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let mut res = Vec::with_capacity(raw.len());
  for b in raw.iter() {
    res.push(!b);
  }
  ret_val!(Value::Raw(Rc::new(res)));
}

fn buffer_not(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  for i in 0..buffer.len() {
    buffer[i] = !buffer[i];
  }
  ret_none!();
}

fn raw_not_offset(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  if let (Some(offset), Some(amount)) = (
    number_utils::get_pos(raw, &params[1]),
    number_utils::get_unsigned(&params[2]),
  ) {
    let raw_size = raw.len();
    if offset < raw_size {
      let mut max = amount + offset;
      if max > raw_size {
        max = raw_size;
      }
      let mut res = Vec::with_capacity(raw_size);
      for i in 0..offset {
        res.push(raw[i]);
      }
      for i in offset..max {
        res.push(!raw[i]);
      }
      if max < raw_size {
        for i in max..raw_size {
          res.push(raw[i]);
        }
      }
      ret_val!(Value::Raw(Rc::new(res)));
    } else {
      ret_val!(params[0].clone())
    }
  }
  ret_val!(params[0].clone());
}

fn buffer_not_offset(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  if let (Some(offset), Some(amount)) = (
    number_utils::get_pos(buffer, &params[1]),
    number_utils::get_unsigned(&params[2]),
  ) {
    let buffer_size = buffer.len();
    if offset < buffer_size {
      let mut max = amount + offset;
      if max > buffer_size {
        max = buffer_size;
      }
      for i in offset..max {
        buffer[i] = !buffer[i];
      }
    }
  }
  ret_none!();
}

fn raw_right_shift(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  if let Some(offset) = number_utils::get_unsigned(&params[1]) {
    let raw_size = raw.len();
    let bytes_to_remove = offset / 8;
    let mut res = Vec::with_capacity(raw_size);
    for _ in 0..raw_size {
      res.push(0);
    }
    if bytes_to_remove <= raw_size {
      let in_byte_shift = offset % 8;
      let carry_shift = 8 - in_byte_shift;
      let carry_mask = (0xFF_u16 >> carry_shift) as u8;
      let mut carry = 0;
      for i in (bytes_to_remove..raw_size).rev() {
        res[i - bytes_to_remove] = (raw[i] >> in_byte_shift) | carry;
        carry = (raw[i] & carry_mask) << carry_shift;
      }
    }
    ret_val!(Value::Raw(Rc::new(res)));
  }
  ret_val!(params[0].clone());
}

fn buffer_right_shift(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  if let Some(offset) = number_utils::get_unsigned(&params[1]) {
    let buffer_size = buffer.len();
    let bytes_to_remove = offset / 8;
    let mut res = Vec::with_capacity(buffer_size);
    for _ in 0..buffer_size {
      res.push(0);
    }
    if bytes_to_remove <= buffer_size {
      let in_byte_shift = offset % 8;
      let carry_shift = 8 - in_byte_shift;
      let carry_mask = (0xFF_u16 >> carry_shift) as u8;
      let mut carry = 0;
      for i in (bytes_to_remove..buffer_size).rev() {
        res[i - bytes_to_remove] = (buffer[i] >> in_byte_shift) | carry;
        carry = (buffer[i] & carry_mask) << carry_shift;
      }
    }
    instance.replace_buffer(buffer_id!(params, 0), res);
  }
  ret_none!();
}

fn raw_left_shift(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  if let Some(offset) = number_utils::get_unsigned(&params[1]) {
    let raw_size = raw.len();
    let bytes_to_remove = offset / 8;
    let mut res = Vec::with_capacity(raw_size);
    for _ in 0..raw_size {
      res.push(0);
    }
    if bytes_to_remove <= raw_size {
      let in_byte_shift = offset % 8;
      let carry_shift = 8 - in_byte_shift;
      let carry_mask = (0xFF_u16 << carry_shift) as u8;
      let mut carry = 0;
      for i in 0..(raw_size - bytes_to_remove) {
        res[i + bytes_to_remove] = (raw[i] << in_byte_shift) | carry;
        carry = (raw[i] & carry_mask) >> carry_shift;
      }
    }
    ret_val!(Value::Raw(Rc::new(res)));
  }
  ret_val!(params[0].clone());
}

fn buffer_left_shift(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  if let Some(offset) = number_utils::get_unsigned(&params[1]) {
    let buffer_size = buffer.len();
    let bytes_to_remove = offset / 8;
    let mut res = Vec::with_capacity(buffer_size);
    for _ in 0..buffer_size {
      res.push(0);
    }
    if bytes_to_remove <= buffer_size {
      let in_byte_shift = offset % 8;
      let carry_shift = 8 - in_byte_shift;
      let carry_mask = (0xFF_u16 << carry_shift) as u8;
      let mut carry = 0;
      for i in 0..(buffer_size - bytes_to_remove) {
        res[i + bytes_to_remove] = (buffer[i] << in_byte_shift) | carry;
        carry = (buffer[i] & carry_mask) >> carry_shift;
      }
    }
    instance.replace_buffer(buffer_id!(params, 0), res);
  }
  ret_none!();
}

fn reflect_u8(val: u8) -> u8 {
  let mut res_val = 0;

  for i in 0..8 {
    if (val & (1 << i)) != 0 {
      res_val |= 1 << (7 - i);
    }
  }
  return res_val;
}

fn raw_reflect(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let mut res = Vec::with_capacity(raw.len());
  for b in raw.iter().rev() {
    res.push(reflect_u8(b.clone()));
  }
  ret_val!(Value::Raw(Rc::new(res)));
}

fn buffer_reflect(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let mut res = Vec::with_capacity(buffer.len());
  for b in buffer.iter().rev() {
    res.push(reflect_u8(b.clone()));
  }
  instance.replace_buffer(buffer_id!(params, 0), res);
  ret_none!();
}

pub fn load_native_functions(intr: &mut Instance) {
  let position_type = Or!(
    Type::PrimitiveUnsigned,
    Type::PrimitiveSigned,
    Type::Integer
  );
  def_func!(intr, "eq", [Type::Any, Type::Any], Type::Boolean, is_eq);
  def_func!(intr, "ne", [Type::Any, Type::Any], Type::Boolean, is_not_eq);
  def_multi_func!(
    intr,
    "lt",
    [
      new_func!([Type::Number, Type::Number], Type::Boolean, is_less_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::Boolean,
        is_less_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::Boolean,
        is_less_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::Boolean,
        is_less_float
      ),
      new_func!([Type::String, Type::String], Type::Boolean, is_less_str),
    ]
  );
  def_multi_func!(
    intr,
    "le",
    [
      new_func!(
        [Type::Number, Type::Number],
        Type::Boolean,
        is_less_or_eq_num
      ),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::Boolean,
        is_less_or_eq_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::Boolean,
        is_less_or_eq_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::Boolean,
        is_less_or_eq_float
      ),
      new_func!(
        [Type::String, Type::String],
        Type::Boolean,
        is_less_or_eq_str
      ),
    ]
  );
  def_multi_func!(
    intr,
    "gt",
    [
      new_func!([Type::Number, Type::Number], Type::Boolean, is_greater_num),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::Boolean,
        is_greater_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::Boolean,
        is_greater_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::Boolean,
        is_greater_float
      ),
      new_func!([Type::String, Type::String], Type::Boolean, is_greater_str),
    ]
  );
  def_multi_func!(
    intr,
    "ge",
    [
      new_func!(
        [Type::Number, Type::Number],
        Type::Boolean,
        is_greater_or_eq_num
      ),
      new_func!(
        [Type::PrimitiveUnsigned, Type::PrimitiveUnsigned],
        Type::Boolean,
        is_greater_or_eq_unsigned
      ),
      new_func!(
        [Type::PrimitiveSigned, Type::PrimitiveSigned],
        Type::Boolean,
        is_greater_or_eq_signed
      ),
      new_func!(
        [Type::PrimitiveFloat, Type::PrimitiveFloat],
        Type::Boolean,
        is_greater_or_eq_float
      ),
      new_func!(
        [Type::String, Type::String],
        Type::Boolean,
        is_greater_or_eq_str
      ),
    ]
  );
  def_multi_func!(
    intr,
    "and",
    [
      new_func!([Type::Boolean, Type::Boolean], Type::Boolean, boolean_and),
      new_func!([Type::Raw, Type::Raw], Type::Raw, raw_and),
      new_func!([Type::Buffer, Type::Raw], Type::Void, buffer_and),
      new_func!(
        [Type::Raw, position_type.clone(), Type::Raw],
        Type::Raw,
        raw_and_offset
      ),
      new_func!(
        [Type::Buffer, position_type.clone(), Type::Raw],
        Type::Void,
        buffer_and_offset
      )
    ]
  );
  def_multi_func!(
    intr,
    "or",
    [
      new_func!([Type::Boolean, Type::Boolean], Type::Boolean, boolean_or),
      new_func!([Type::Raw, Type::Raw], Type::Raw, raw_or),
      new_func!([Type::Buffer, Type::Raw], Type::Void, buffer_or),
      new_func!(
        [Type::Raw, position_type.clone(), Type::Raw],
        Type::Raw,
        raw_or_offset
      ),
      new_func!(
        [Type::Buffer, position_type.clone(), Type::Raw],
        Type::Void,
        buffer_or_offset
      )
    ]
  );
  def_multi_func!(
    intr,
    "xor",
    [
      new_func!([Type::Boolean, Type::Boolean], Type::Boolean, boolean_xor),
      new_func!([Type::Raw, Type::Raw], Type::Raw, raw_xor),
      new_func!([Type::Buffer, Type::Raw], Type::Void, buffer_xor),
      new_func!(
        [Type::Raw, position_type.clone(), Type::Raw],
        Type::Raw,
        raw_xor_offset
      ),
      new_func!(
        [Type::Buffer, position_type.clone(), Type::Raw],
        Type::Void,
        buffer_xor_offset
      )
    ]
  );
  def_multi_func!(
    intr,
    "not",
    [
      new_func!([Type::Boolean], Type::Boolean, boolean_not),
      new_func!([Type::Raw], Type::Raw, raw_not),
      new_func!([Type::Buffer], Type::Void, buffer_not),
      new_func!(
        [Type::Raw, position_type.clone(), position_type.clone()],
        Type::Raw,
        raw_not_offset
      ),
      new_func!(
        [Type::Buffer, position_type.clone(), position_type.clone()],
        Type::Void,
        buffer_not_offset
      )
    ]
  );
  def_multi_func!(
    intr,
    "left_shift",
    [
      new_func!(
        [Type::Raw, position_type.clone()],
        Type::Raw,
        raw_left_shift
      ),
      new_func!(
        [Type::Buffer, position_type.clone()],
        Type::Void,
        buffer_left_shift
      )
    ]
  );
  def_multi_func!(
    intr,
    "right_shift",
    [
      new_func!(
        [Type::Raw, position_type.clone()],
        Type::Raw,
        raw_right_shift
      ),
      new_func!(
        [Type::Buffer, position_type.clone()],
        Type::Void,
        buffer_right_shift
      )
    ]
  );
  def_multi_func!(
    intr,
    "reflect",
    [
      new_func!([Type::Raw], Type::Raw, raw_reflect),
      new_func!([Type::Buffer], Type::Void, buffer_reflect)
    ]
  );
}
