use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::parsers::Token;
use crate::values::Value;
use std::rc::Rc;

pub fn convert_key_list(list: &Vec<Value>) -> Vec<String> {
  let mut key_list = Vec::with_capacity(list.len());
  for val in list.iter() {
    if let Value::String(val) = val {
      key_list.push(String::clone(val));
    }
  }
  key_list
}

pub fn get_check_value(
  instance: &mut Instance,
  asl: &ASL,
  token_list: Rc<Vec<Token>>,
  id_list: Rc<Vec<usize>>,
) -> Result<bool, String> {
  match CommandExec::run_command(instance, asl, token_list, id_list) {
    CommandExecReturn::Stack(mut stack) => {
      if let Value::Boolean(val) = stack.pop().unwrap() {
        Ok(val)
      } else {
        Err(String::from(
          "Non boolean value received from the check command.",
        ))
      }
    }
    CommandExecReturn::NoReturn => Err(String::from("No value received from the check command.")),
    CommandExecReturn::Return(_) => Err(String::from("Return inside the check command.")),
    CommandExecReturn::Loop(continue_loop) => Err(format!(
      "{} inside the check command.",
      if continue_loop { "Continue" } else { "Break" }
    )),
    CommandExecReturn::End => Err(String::from("End inside the check command.")),
    CommandExecReturn::Exit(_) => Err(String::from("Exit inside the check command.")),
    CommandExecReturn::Error(err) => Err(err),
  }
}
