use super::utils::convert_key_list;
use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::scopes::ScopeId;
use crate::utils::to_system_string::ToSystemString;
use crate::values::value_map::ValueMap;
use crate::values::{function, SystemType as Type, Value};
use num_bigint::BigInt;
use num_rational::BigRational;
use std::rc::Rc;
use std::time::SystemTime;
use std::{thread, time};

// "instance_of" native command

fn is_instance_of(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(
    system_type!(params, 1).is_type_of(instance, asl, &params[0])
  ));
}

// "delete" native command

fn delete_variable_by_ref(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  instance.delete_var_by_ref(reference!(params, 0));
  ret_none!();
}

fn delete_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.delete_vector(vector_id!(params, 0));
  ret_none!();
}

fn delete_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.delete_map(map_id!(params, 0));
  ret_none!();
}

fn delete_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.delete_buffer(buffer_id!(params, 0));
  ret_none!();
}

// "custom_ref" native command

fn get_custom_ref(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = string!(params, 0).as_ref();
  let key_list = convert_key_list(list!(params, 1));
  let is_valid_id = instance.get_id_prefix_manager().check_id(id, &key_list);
  if is_valid_id {
    ret_val!(instance.create_local_var_ref(id));
  } else {
    ret_error_string!(format!("Id `{}` is not a valid local id.", id));
  }
}

// "new_vec" native functions

fn new_vector(instance: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Vector(instance.create_new_vector()));
}

// "clone_vec" native functions

fn clone_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match instance.clone_vector(vector_id!(params, 0)) {
    Ok(id) => {
      ret_val!(Value::Vector(id));
    }
    Err(err) => ret_error_string!(err),
  }
}

// "clear_vector" native functions

fn clear_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.clear_vector(vector_id!(params, 0));
  ret_none!();
}

// "new_map" native functions

fn new_map(instance: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Map(instance.create_new_map()));
}

// "clone_map" native functions

fn clone_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match instance.clone_map(map_id!(params, 0)) {
    Ok(id) => {
      ret_val!(Value::Map(id));
    }
    Err(err) => ret_error_string!(err),
  }
}

// "clear_map" native functions

fn clear_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.clear_map(map_id!(params, 0));
  ret_none!();
}

// "new_buffer" native functions

fn new_buffer(instance: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Buffer(instance.create_new_buffer()));
}

// "clone_buffer" native functions

fn clone_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match instance.clone_buffer(buffer_id!(params, 0)) {
    Ok(id) => {
      ret_val!(Value::Buffer(id));
    }
    Err(err) => ret_error_string!(err),
  }
}

// "clear_buffer" native functions

fn clear_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  instance.clear_buffer(buffer_id!(params, 0));
  ret_none!();
}

// "unix_timestamp" native functions

fn unix_timestamp(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
    Ok(n) => ret_val!(Value::Number(BigRational::from(BigInt::from(n.as_secs())))),
    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
  }
}

// "unix_timestamp_u" native functions

fn unix_timestamp_u(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
    Ok(n) => ret_val!(Value::Unsigned(n.as_secs())),
    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
  }
}

// "unix_timestamp_full" native functions

fn unix_timestamp_full(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
    Ok(n) => ret_val!(Value::Number(BigRational::from(BigInt::from(
      n.as_micros()
    )))),
    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
  }
}

// "unix_timestamp_full_u" native functions

fn unix_timestamp_full_u(_: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
    Ok(n) => ret_val!(Value::Unsigned(n.as_micros() as u64)),
    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
  }
}

// "system" native functions

fn run_command(_: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match asl.system.run_command(string!(params, 0).as_ref()) {
    Ok((stdout, stderr)) => ret_val!(Value::List(Rc::new(vec![
      Value::String(Rc::new(stdout)),
      Value::String(Rc::new(stderr))
    ]))),
    Err(err) => ret_error_string!(err),
  }
}

// "type_of" native functions

fn type_of_value(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Type(params[0].get_value_type()));
}

// "is_supertype_of" native command

fn is_supertype_of(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(system_type!(params, 0).is_supertype_of(
    instance,
    asl,
    system_type!(params, 1)
  )));
}

// "get_id" native command

fn get_scope_ref_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = block_reference!(params, 0);
  ret_val!(Value::Unsigned(id.clone() as u64));
}

fn get_global_ref_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let name = global_reference!(params, 0);
  ret_val!(Value::String(Rc::new(name.clone())));
}

fn get_local_ref_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = local_reference!(params, 0);
  ret_val!(Value::String(Rc::new(id.clone())));
}

fn get_vector_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = vector_id!(params, 0);
  ret_val!(Value::Unsigned(id.clone() as u64));
}

fn get_map_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = map_id!(params, 0);
  ret_val!(Value::Unsigned(id.clone() as u64));
}

fn get_buffer_id(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let id = buffer_id!(params, 0);
  ret_val!(Value::Unsigned(id.clone() as u64));
}

// "get_ref_name" native command

fn get_ref_name(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let reference = reference!(params, 0);
  ret_val!(Value::String(Rc::new(reference.to_string(instance, asl))));
}

// "wait" native command

fn wait(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  thread::sleep(time::Duration::from_millis(unsigned!(params, 0) as u64));
  ret_none!();
}

// "dump_global_vars" native command

fn dump_global_vars(instance: &mut Instance, _: &ASL, _: Vec<Value>) -> CommandExecReturn {
  let mut value_map = ValueMap::new();
  for (k, v) in instance.dump_global_vars() {
    value_map.set(&k, v);
  }
  ret_val!(Value::Dictionary(Rc::new(value_map)));
}

// "register_prefix" native command

fn register_prefix(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let prefix = string!(params, 0);
  match instance.get_id_prefix_manager().add_prefix(prefix) {
    Some(prefix_key) => ret_val!(Value::String(Rc::new(prefix_key))),
    None => ret_error_string!(String::from("Invalid prefix")),
  }
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(
    intr,
    "instance_of",
    [Type::Any, Type::Type],
    Type::Boolean,
    is_instance_of
  );
  def_multi_func!(
    intr,
    "delete",
    [
      new_func!([Type::Reference], Type::Void, delete_variable_by_ref),
      new_func!([Type::Vector], Type::Void, delete_vector),
      new_func!([Type::Map], Type::Void, delete_map),
      new_func!([Type::Buffer], Type::Void, delete_buffer)
    ]
  );
  def_func!(
    intr,
    "custom_ref",
    [
      Type::String,
      Or!(Sequence!(Type::String, Type::String), Set!(Type::String))
    ],
    Type::LocalReference,
    get_custom_ref
  );
  def_func!(intr, "new_vec", [], Type::Vector, new_vector);
  def_func!(
    intr,
    "clone_vec",
    [Type::Vector],
    Type::Vector,
    clone_vector
  );
  def_func!(intr, "clear_vec", [Type::Vector], Type::Void, clear_vector);
  def_func!(intr, "new_map", [], Type::Map, new_map);
  def_func!(intr, "clone_map", [Type::Map], Type::Map, clone_map);
  def_func!(intr, "clear_map", [Type::Map], Type::Void, clear_map);
  def_func!(intr, "new_buffer", [], Type::Buffer, new_buffer);
  def_func!(
    intr,
    "clone_buffer",
    [Type::Buffer],
    Type::Buffer,
    clone_buffer
  );
  def_func!(
    intr,
    "clear_buffer",
    [Type::Buffer],
    Type::Void,
    clear_buffer
  );
  def_func!(intr, "unix_timestamp", [], Type::Unsigned, unix_timestamp);
  def_func!(
    intr,
    "unix_timestamp_u",
    [],
    Type::PrimitiveUnsigned,
    unix_timestamp_u
  );
  def_func!(
    intr,
    "unix_timestamp_full",
    [],
    Type::Unsigned,
    unix_timestamp_full
  );
  def_func!(
    intr,
    "unix_timestamp_full_u",
    [],
    Type::PrimitiveUnsigned,
    unix_timestamp_full_u
  );
  def_func!(
    intr,
    "system",
    [Type::String],
    Set!(Type::String, Type::String),
    run_command
  );
  def_func!(intr, "type_of", [Type::Any], Type::ValueType, type_of_value);
  def_func!(
    intr,
    "is_supertype_of",
    [Type::Type, Type::Type],
    Type::Boolean,
    is_supertype_of
  );
  def_multi_func!(
    intr,
    "get_id",
    [
      new_func!(
        [Type::BlockReference],
        Type::PrimitiveUnsigned,
        get_scope_ref_id
      ),
      new_func!([Type::GlobalReference], Type::String, get_global_ref_id),
      new_func!([Type::LocalReference], Type::String, get_local_ref_id),
      new_func!([Type::Vector], Type::PrimitiveUnsigned, get_vector_id),
      new_func!([Type::Map], Type::PrimitiveUnsigned, get_map_id),
      new_func!([Type::Buffer], Type::PrimitiveUnsigned, get_buffer_id),
    ]
  );
  def_func!(
    intr,
    "get_ref_name",
    [Type::Reference],
    Type::String,
    get_ref_name
  );
  def_func!(intr, "wait", [Type::PrimitiveUnsigned], Type::Void, wait);
  def_func!(
    intr,
    "dump_global_vars",
    [],
    Type::Dictionary,
    dump_global_vars
  );
  def_func!(
    intr,
    "register_prefix",
    [Type::String],
    Type::String,
    register_prefix
  );
}
