use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "parse" native command

fn parse_command(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  match instance.parse(string!(params, 0).as_ref().clone()) {
    Ok((token_list, id_list)) => ret_val!(Value::Command {
      token_list,
      id_list
    }),
    Err(err) => ret_error_string!(err),
  };
}

// "queue_load" native command

fn load_command(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let (token_list, id_list) = command!(params, 0);
  instance.add_to_queue(token_list.clone(), id_list.clone());
  ret_none!();
}

// "load_persistant" native command

fn load_persistant_command(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let (token_list, id_list) = command!(params, 0);
  return CommandExec::run_persistant_command(instance, asl, token_list.clone(), id_list.clone());
}

pub fn load_native_functions(intr: &mut Instance) {
  def_func!(intr, "parse", [Type::String], Type::Command, parse_command);
  def_func!(
    intr,
    "queue_load",
    [Type::Command],
    Type::Void,
    load_command
  );
  def_func!(
    intr,
    "load_persistant",
    [Type::Command],
    Type::Void,
    load_persistant_command
  );
}
