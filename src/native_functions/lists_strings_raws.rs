use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::utils::to_system_string::ToSystemString;
use crate::utils::{base_utils, number_utils};
use crate::values::{function, SystemType as Type, Value};
use std::rc::Rc;

// "add" native functions
// Used by add_sub_mul.rs

pub fn join_strings(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = string!(params, 0);
  let mut res = a.as_ref().clone();
  if let Value::String(_) = params[1] {
    res.push_str(&string!(params, 1).as_ref().clone());
  } else {
    res.push_str(&params[1].to_string(instance, asl));
  }
  ret_val!(Value::String(Rc::new(res)));
}

pub fn join_lists(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = list!(params, 0);
  let b = list!(params, 1);
  let mut res = a.as_ref().clone();
  res.append(&mut b.as_ref().clone());
  ret_val!(Value::List(Rc::new(res)));
}

// "sub" native functions
// Used by add_sub_mul.rs

pub fn sub_lists(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = list!(params, 0);
  let b = list!(params, 1);
  let mut res = Vec::new();
  for val in a.iter() {
    if !b.contains(val) {
      res.push(val.clone());
    }
  }
  ret_val!(Value::List(Rc::new(res)));
}

pub fn sub_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = string!(params, 0).as_str();
  let b = string!(params, 1).as_str();
  ret_val!(Value::String(Rc::new(a.replace(b, ""))));
}

// "mul" native functions
// Used by add_sub_mul.rs

pub fn mul_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = list!(params, 0);
  let b = match number_utils::get_unsigned(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Value must be able to be converted to a system unsigned number"),
  };
  let mut result = Vec::with_capacity(a.len() * b);
  for _ in 0..b {
    for v in a.iter() {
      result.push(v.clone())
    }
  }
  ret_val!(Value::List(Rc::new(result)));
}

pub fn mul_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = string!(params, 0);
  let b = match number_utils::get_unsigned(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Value must be able to be converted to a system unsigned number"),
  };
  let mut result = String::with_capacity(a.len() * b);
  for _ in 0..b {
    result.push_str(a)
  }
  ret_val!(Value::String(Rc::new(result)));
}

pub fn mul_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = match number_utils::get_unsigned(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Value must be able to be converted to a system unsigned number"),
  };
  let mut result = Vec::with_capacity(a.len() * b);
  for _ in 0..b {
    for v in a.iter() {
      result.push(v.clone())
    }
  }
  ret_val!(Value::Raw(Rc::new(result)));
}

// "get" native commands

fn get_value_from_list(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos(list, &params[1]) {
    Some(pos) => ret_val!(list[pos].clone()),
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of length {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  };
}

fn get_value_from_vector(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let aux = &params[1].to_string(instance, asl);
  let vector = vector!(instance, params, 0);
  match number_utils::get_pos(vector, &params[1]) {
    Some(pos) => ret_val!(vector[pos].clone()),
    None => ret_error_string!(format!(
      "{} is not a valid position on an vector of length {}",
      aux,
      vector.len()
    )),
  };
}

fn get_value_from_dictionary(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let dictionary = dictionary!(params, 0);
  let key = string!(params, 1);
  match dictionary.get(key) {
    Some(value) => ret_val!(value.clone()),
    None => ret_error_string!(format!(
      "{} is not a valid key",
      &params[1].to_string(instance, asl),
    )),
  };
}

fn get_value_from_map(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let map = map!(instance, params, 0);
  let key = string!(params, 1);
  match map.get(key) {
    Some(value) => ret_val!(value.clone()),
    None => ret_error_string!(format!(
      "{} is not a valid key",
      &params[1].to_string(instance, asl),
    )),
  };
}

fn get_char_from_string(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let string = string!(params, 0);
  match number_utils::get_pos_str(string, &params[1]) {
    Some(pos) => {
      let mut char_string = String::new();
      char_string.push(string.chars().nth(pos).unwrap());
      ret_val!(Value::String(Rc::new(char_string)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a string of length {}",
      &params[1].to_string(instance, asl),
      string.len()
    )),
  };
}

fn get_value_from_raw(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match number_utils::get_pos(raw, &params[1]) {
    Some(pos) => ret_val!(Value::Raw(Rc::new(vec![raw[pos].clone()]))),
    None => ret_error_string!(format!(
      "{} is not a valid position on a raw of length {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
  };
}

fn get_value_from_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  match number_utils::get_pos(&buffer, &params[1]) {
    Some(pos) => ret_val!(Value::Raw(Rc::new(vec![buffer[pos].clone()]))),
    None => {
      let size = buffer.len();
      ret_error_string!(format!(
        "{} is not a valid position on a buffer of length {}",
        &params[1].to_string(instance, asl),
        size
      ))
    }
  };
}

// "set" native commands

fn set_value_from_list(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos(list, &params[1]) {
    Some(pos) => {
      let mut res = Vec::with_capacity(list.len());
      for (i, x) in list.iter().enumerate() {
        if i != pos {
          res.push(x.clone());
        } else {
          res.push(params[2].clone());
        }
      }
      ret_val!(Value::List(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of length {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  };
}

fn set_value_from_vector(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let aux = &params[1].to_string(instance, asl);
  let vector = vector!(instance, params, 0);
  match number_utils::get_pos(vector, &params[1]) {
    Some(pos) => {
      vector[pos] = params[2].clone();
      ret_none!()
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a vector of length {}",
      aux,
      vector.len()
    )),
  };
}

fn set_value_from_dictionary(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut dictionary = dictionary!(params, 0).as_ref().clone();
  dictionary.set(string!(params, 1), params[2].clone());
  ret_val!(Value::Dictionary(Rc::new(dictionary)));
}

fn set_value_from_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let map = map!(instance, params, 0);
  map.set(string!(params, 1), params[2].clone());
  ret_none!();
}

fn set_value_on_string(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let string_a = string!(params, 0);
  let string_b = string!(params, 2);
  match number_utils::get_pos_inclusive_str(string_a, &params[1]) {
    Some(pos) => {
      let len = string_a.len();
      let len_with_set = pos + string_b.len();
      let mut res = String::with_capacity(if len_with_set > len_with_set {
        len_with_set
      } else {
        len
      });
      let mut iter_a = string_a.chars();
      for _ in 0..pos {
        res.push(iter_a.next().unwrap().clone());
      }
      for b in string_b.chars() {
        res.push(b.clone());
        iter_a.next();
      }
      for b in iter_a {
        res.push(b.clone());
      }
      ret_val!(Value::String(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a string of length {}",
      &params[1].to_string(instance, asl),
      string_a.len()
    )),
  };
}

fn set_value_on_raw(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw_a = raw!(params, 0);
  let raw_b = raw!(params, 2);
  match number_utils::get_pos_inclusive(raw_a, &params[1]) {
    Some(pos) => {
      let len = raw_a.len();
      let len_with_set = pos + raw_b.len();
      let mut res = Vec::with_capacity(if len_with_set > len_with_set {
        len_with_set
      } else {
        len
      });
      let mut iter_a = raw_a.iter();
      for _ in 0..pos {
        res.push(iter_a.next().unwrap().clone());
      }
      for b in raw_b.iter() {
        res.push(b.clone());
        iter_a.next();
      }
      for b in iter_a {
        res.push(b.clone());
      }
      ret_val!(Value::Raw(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a raw of length {}",
      &params[1].to_string(instance, asl),
      raw_a.len()
    )),
  };
}

fn set_value_on_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let buffer_len = buffer.len();
  match number_utils::get_pos_inclusive(buffer, &params[1]) {
    Some(mut pos) => {
      let raw = raw!(params, 2);
      let len_with_set = pos + raw.len();
      let limit = if len_with_set < buffer_len {
        len_with_set
      } else {
        buffer_len
      };
      let mut iter = raw.iter();
      while pos < limit {
        buffer[pos] = iter.next().unwrap().clone();
        pos += 1;
      }
      for b in iter {
        buffer.push(b.clone());
      }
      ret_none!();
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a buffer of length {}",
      &params[1].to_string(instance, asl),
      buffer_len
    )),
  };
}

// "rem" native command

fn remove_value_from_list(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos(list, &params[1]) {
    Some(pos) => {
      let mut res = Vec::with_capacity(list.len());
      for (i, x) in list.iter().enumerate() {
        if i != pos {
          res.push(x.clone());
        }
      }
      ret_val!(Value::List(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of length {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  };
}

fn remove_value_from_vector(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let aux = &params[1].to_string(instance, asl);
  let vector = vector!(instance, params, 0);
  match number_utils::get_pos(vector, &params[1]) {
    Some(pos) => ret_val!(vector.remove(pos)),
    None => ret_error_string!(format!(
      "{} is not a valid position on a vector of length {}",
      aux,
      vector.len()
    )),
  };
}

fn remove_value_from_dictionary(
  _: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let mut dictionary = dictionary!(params, 0).as_ref().clone();
  dictionary.remove(string!(params, 1));
  ret_val!(Value::Dictionary(Rc::new(dictionary)));
}

fn remove_value_from_map(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let map = map!(instance, params, 0);
  match map.remove(string!(params, 1)) {
    Some(value) => ret_val!(value.clone()),
    None => ret_error_string!(format!(
      "{} is not a valid key",
      &params[1].to_string(instance, asl)
    )),
  };
}

// "replace" native commands

fn replace_in_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let src = string!(params, 0).as_str();
  let pattern = string!(params, 1).as_str();
  let new_str = string!(params, 2).as_str();
  ret_val!(Value::String(Rc::new(src.replace(pattern, new_str))));
}

fn replace_in_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  let old_val = &params[1];
  let new_val = &params[2];
  let mut res = Vec::with_capacity(list.len());
  for x in list.iter() {
    if x == old_val {
      res.push(new_val.clone());
    } else {
      res.push(x.clone());
    }
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn replace_in_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let vector = vector!(instance, params, 0);
  let old_val = &params[1];
  let new_val = &params[2];
  let mut to_change = Vec::new();
  for (pos, x) in vector.iter().enumerate() {
    if x == old_val {
      to_change.push(pos);
    }
  }
  for pos in to_change {
    vector[pos] = new_val.clone();
  }
  ret_none!()
}

// "contains" native commands

fn string_contains(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let src = string!(params, 0).as_str();
  let pattern = string!(params, 1).as_str();
  ret_val!(Value::Boolean(src.contains(pattern)));
}

fn list_contains(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(list!(params, 0).contains(&params[1])))
}

fn vector_contains(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(
    vector!(instance, params, 0).contains(&params[1])
  ));
}

fn dictionary_contains(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(dictionary!(params, 0).contains(&params[1])))
}

fn map_contains(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(
    map!(instance, params, 0).contains(&params[1])
  ));
}

// "contains_key" native commands

fn dictionary_contains_key(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(
    dictionary!(params, 0).contains_key(string!(params, 1))
  ))
}

fn map_contains_key(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Boolean(
    map!(instance, params, 0).contains_key(string!(params, 1))
  ));
}

// "size" native command

fn get_size_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    list!(params, 0).len()
  )));
}

fn get_size_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    vector!(instance, params, 0).len()
  )));
}

fn get_size_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    string!(params, 0).chars().count()
  )));
}

fn get_size_dictionary(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    dictionary!(params, 0).len()
  )));
}

fn get_size_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    map!(instance, params, 0).len()
  )));
}

fn get_size_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    raw!(params, 0).len()
  )));
}

fn get_size_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Number(number_utils::usize_to_rational(
    buffer!(instance, params, 0).len()
  )));
}

// "size_u" native command

fn get_size_list_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(list!(params, 0).len() as u64));
}

fn get_size_vector_u(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(vector!(instance, params, 0).len() as u64));
}

fn get_size_string_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(string!(params, 0).chars().count() as u64));
}

fn get_size_dictionary_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(dictionary!(params, 0).len() as u64));
}

fn get_size_map_u(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(map!(instance, params, 0).len() as u64));
}

fn get_size_raw_u(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(raw!(params, 0).len() as u64));
}

fn get_size_buffer_u(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Unsigned(buffer!(instance, params, 0).len() as u64));
}

// "size_s" native command

fn get_size_list_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(list!(params, 0).len() as i64));
}

fn get_size_vector_s(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(vector!(instance, params, 0).len() as i64));
}

fn get_size_string_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(string!(params, 0).chars().count() as i64));
}

fn get_size_dictionary_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(dictionary!(params, 0).len() as i64));
}

fn get_size_map_s(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(map!(instance, params, 0).len() as i64));
}

fn get_size_raw_s(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(raw!(params, 0).len() as i64));
}

fn get_size_buffer_s(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(Value::Signed(buffer!(instance, params, 0).len() as i64));
}

// "split" native command

fn split_list(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos_inclusive(list, &params[1]) {
    Some(pos) => {
      let mut list_a = Vec::with_capacity(pos);
      let mut list_b = Vec::with_capacity(list.len() - pos);
      let mut count = 0;
      let mut iter = list.iter();
      while count < pos {
        list_a.push(iter.next().unwrap().clone());
        count += 1;
      }
      for val in iter {
        list_b.push(val.clone());
      }
      ret_val!(Value::List(Rc::new(vec!(
        Value::List(Rc::new(list_a)),
        Value::List(Rc::new(list_b))
      ))))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of size {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  };
}

fn split_string(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let s = string!(params, 0);
  match number_utils::get_pos_inclusive_str(s, &params[1]) {
    Some(pos) => {
      let mut str_a = String::new();
      let mut str_b = String::new();
      let mut count = 0;
      let mut iter = s.chars();
      while count < pos {
        str_a.push(iter.next().unwrap().clone());
        count += 1;
      }
      for val in iter {
        str_b.push(val.clone());
      }
      ret_val!(Value::List(Rc::new(vec!(
        Value::String(Rc::new(str_a)),
        Value::String(Rc::new(str_b))
      ))))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of size {}",
      &params[1].to_string(instance, asl),
      s.chars().count()
    )),
  };
}

fn split_raw(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match number_utils::get_pos_inclusive(raw, &params[1]) {
    Some(pos) => {
      let mut raw_a = Vec::with_capacity(pos);
      let mut raw_b = Vec::with_capacity(raw.len() - pos);
      let mut count = 0;
      let mut iter = raw.iter();
      while count < pos {
        raw_a.push(iter.next().unwrap().clone());
        count += 1;
      }
      for val in iter {
        raw_b.push(val.clone());
      }
      ret_val!(Value::List(Rc::new(vec!(
        Value::Raw(Rc::new(raw_a)),
        Value::Raw(Rc::new(raw_b))
      ))))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a raw of size {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
  };
}

fn split_string_by_separator(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let s = string!(params, 0);
  let sep = string!(params, 1);
  let mut res = Vec::new();
  for slice in s.split(sep.as_str()) {
    res.push(Value::String(Rc::new(String::from(slice))));
  }
  ret_val!(Value::List(Rc::new(res)));
}

// "slice" native command

fn slice_list(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = vector_or_list!(instance, params, 0).clone();
  match (
    number_utils::get_pos_inclusive(&list, &params[1]),
    number_utils::get_pos_inclusive(&list, &params[2]),
  ) {
    (Some(pos_start), Some(pos_end)) => {
      if pos_end < pos_start {
        ret_error_string!(format!(
          "The end position {}({}) must be after the start position {}({})",
          &params[1].to_string(instance, asl),
          pos_start,
          &params[2].to_string(instance, asl),
          pos_end
        ));
      } else {
        let size = pos_end - pos_start;
        let mut resut = Vec::with_capacity(size);
        let mut count = 0;
        let mut iter = list.iter().skip(pos_start);
        while count < size {
          resut.push(iter.next().unwrap().clone());
          count += 1;
        }
        ret_val!(Value::List(Rc::new(resut)))
      }
    }
    (None, _) => ret_error_string!(format!(
      "{} is not a valid position on an array of size {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
    _ => ret_error_string!(format!(
      "{} is not a valid position on an array of size {}",
      &params[2].to_string(instance, asl),
      list.len()
    )),
  };
}

fn slice_string(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let string = string!(params, 0);
  match (
    number_utils::get_pos_inclusive_str(string, &params[1]),
    number_utils::get_pos_inclusive_str(string, &params[2]),
  ) {
    (Some(pos_start), Some(pos_end)) => {
      if pos_end < pos_start {
        ret_error_string!(format!(
          "The end position {}({}) must be after the start position {}({})",
          &params[1].to_string(instance, asl),
          pos_start,
          &params[2].to_string(instance, asl),
          pos_end
        ));
      } else {
        let size = pos_end - pos_start;
        let mut resut = String::with_capacity(size);
        let mut count = 0;
        let mut iter = string.chars().skip(pos_start);
        while count < size {
          resut.push(iter.next().unwrap().clone());
          count += 1;
        }
        ret_val!(Value::String(Rc::new(resut)))
      }
    }
    (None, _) => ret_error_string!(format!(
      "{} is not a valid position on a string of size {}",
      &params[1].to_string(instance, asl),
      string.len()
    )),
    _ => ret_error_string!(format!(
      "{} is not a valid position on a string of size {}",
      &params[2].to_string(instance, asl),
      string.len()
    )),
  };
}

fn slice_raw(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match (
    number_utils::get_pos_inclusive(raw, &params[1]),
    number_utils::get_pos_inclusive(raw, &params[2]),
  ) {
    (Some(pos_start), Some(pos_end)) => {
      if pos_end < pos_start {
        ret_error_string!(format!(
          "The end position {}({}) must be after the start position {}({})",
          &params[1].to_string(instance, asl),
          pos_start,
          &params[2].to_string(instance, asl),
          pos_end
        ));
      } else {
        let size = pos_end - pos_start;
        let mut resut = Vec::with_capacity(size);
        let mut count = 0;
        let mut iter = raw.iter().skip(pos_start);
        while count < size {
          resut.push(iter.next().unwrap().clone());
          count += 1;
        }
        ret_val!(Value::Raw(Rc::new(resut)))
      }
    }
    (None, _) => ret_error_string!(format!(
      "{} is not a valid position on a raw of size {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
    _ => ret_error_string!(format!(
      "{} is not a valid position on a raw of size {}",
      &params[2].to_string(instance, asl),
      raw.len()
    )),
  };
}

fn slice_list_to_end(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = vector_or_list!(instance, params, 0).clone();
  match number_utils::get_pos_inclusive(&list, &params[1]) {
    Some(pos_start) => {
      let size = list.len() - pos_start;
      let mut resut = Vec::with_capacity(size);
      let mut count = 0;
      let mut iter = list.iter().skip(pos_start);
      while count < size {
        resut.push(iter.next().unwrap().clone());
        count += 1;
      }
      ret_val!(Value::List(Rc::new(resut)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on an array of size {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  };
}

fn slice_string_to_end(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let string = string!(params, 0);
  match number_utils::get_pos_inclusive_str(string, &params[1]) {
    Some(pos_start) => {
      let size = string.chars().count() - pos_start;
      let mut resut = String::with_capacity(size);
      let mut count = 0;
      let mut iter = string.chars().skip(pos_start);
      while count < size {
        resut.push(iter.next().unwrap().clone());
        count += 1;
      }
      ret_val!(Value::String(Rc::new(resut)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a string of size {}",
      &params[1].to_string(instance, asl),
      string.len()
    )),
  };
}

fn slice_raw_to_end(instance: &mut Instance, asl: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match number_utils::get_pos_inclusive(raw, &params[1]) {
    Some(pos_start) => {
      let size = raw.len() - pos_start;
      let mut resut = Vec::with_capacity(size);
      let mut count = 0;
      let mut iter = raw.iter().skip(pos_start);
      while count < size {
        resut.push(iter.next().unwrap().clone());
        count += 1;
      }
      ret_val!(Value::Raw(Rc::new(resut)))
    }
    None => ret_error_string!(format!(
      "{} is not a valid position on a raw of size {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
  };
}

// "push" native command

fn push_value_to_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  let mut res = Vec::with_capacity(list.len() + 1);
  for val in list.iter() {
    res.push(val.clone());
  }
  res.push(params[1].clone());
  ret_val!(Value::List(Rc::new(res)))
}

fn push_value_to_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  vector!(instance, params, 0).push(params[1].clone());
  ret_none!();
}

pub fn push_to_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 1);
  let mut res = Vec::clone(a);
  res.append(&mut Vec::clone(b));
  ret_val!(Value::Raw(Rc::new(res)));
}

pub fn push_to_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let other = raw!(params, 1);
  let buffer = buffer!(instance, params, 0);
  buffer.append(&mut Vec::clone(other));
  ret_none!();
}

// "pop" native command

fn pop_value_from_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  if list.len() > 0 {
    let mut res = Vec::with_capacity(list.len() - 1);
    let len_minus_one = list.len() - 1;
    for (i, val) in list.iter().enumerate() {
      if i < len_minus_one {
        res.push(val.clone());
      }
    }
    ret_val!(Value::List(Rc::new(res)))
  } else {
    ret_error_string!(String::from("Cannot pop from an empty list."))
  }
}

fn pop_value_from_vector(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let vector = vector!(instance, params, 0);
  if vector.len() > 0 {
    ret_val!(vector.pop().unwrap());
  } else {
    ret_error_string!(String::from("Cannot pop from an empty vector."))
  }
}

fn pop_value_from_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  if raw.len() > 0 {
    let mut res = Vec::with_capacity(raw.len() - 1);
    let len_minus_one = raw.len() - 1;
    for (i, val) in raw.iter().enumerate() {
      if i < len_minus_one {
        res.push(val.clone());
      }
    }
    ret_val!(Value::Raw(Rc::new(res)))
  } else {
    ret_error_string!(String::from("Cannot pop from an empty raw."))
  }
}

fn pop_value_from_buffer(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  if buffer.len() > 0 {
    ret_val!(Value::Raw(Rc::new(vec![buffer.pop().unwrap()])));
  } else {
    ret_error_string!(String::from("Cannot pop from an empty buffer."))
  }
}

fn pop_multiple_values_from_list(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos_inclusive(list, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(list.len() - 1);
      let new_len = list.len() - to_pop;
      for i in 0..new_len {
        res.push(list[i].clone());
      }
      ret_val!(Value::List(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a list of size {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  }
}

fn pop_multiple_values_from_vector(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let vector = vector!(instance, params, 0);
  let size = vector.len();
  match number_utils::get_pos_inclusive(vector, &params[1]) {
    Some(to_pop) => {
      let res = vector.split_off(vector.len() - to_pop);
      ret_val!(Value::List(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a vector of size {}",
      &params[1].to_string(instance, asl),
      size
    )),
  }
}

fn pop_multiple_values_from_raw(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match number_utils::get_pos_inclusive(raw, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(raw.len() - 1);
      let new_len = raw.len() - to_pop;
      for i in 0..new_len {
        res.push(raw[i].clone());
      }
      ret_val!(Value::Raw(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a raw of size {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
  }
}

fn pop_multiple_values_from_buffer(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let size = buffer.len();
  match number_utils::get_pos_inclusive(buffer, &params[1]) {
    Some(to_pop) => {
      let res = buffer.split_off(buffer.len() - to_pop);
      ret_val!(Value::Raw(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a buffer of size {}",
      &params[1].to_string(instance, asl),
      size
    )),
  }
}

// "push_f" native command

fn push_value_to_list_front(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  let mut res = Vec::with_capacity(list.len() + 1);
  res.push(params[1].clone());
  for val in list.iter() {
    res.push(val.clone());
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn push_value_to_vector_front(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  vector!(instance, params, 0).insert(0, params[1].clone());
  ret_none!();
}

pub fn push_value_to_raw_front(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let a = raw!(params, 0);
  let b = raw!(params, 1);
  let mut res = Vec::clone(b);
  res.append(&mut Vec::clone(a));
  ret_val!(Value::Raw(Rc::new(res)));
}

pub fn push_value_to_buffer_front(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let other = raw!(params, 1);
  let buffer = buffer!(instance, params, 0);
  for (i, b) in other.iter().enumerate() {
    buffer.insert(i, b.clone())
  }
  ret_none!();
}

// "pop_f" native command

fn pop_value_from_list_front(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let list = list!(params, 0);
  let mut res = Vec::with_capacity(list.len() - 1);
  let mut iter = list.iter();
  iter.next();
  for val in iter {
    res.push(val.clone());
  }
  ret_val!(Value::List(Rc::new(res)))
}

fn pop_value_from_vector_front(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let vector = vector!(instance, params, 0);
  if vector.len() > 0 {
    ret_val!(vector.remove(0));
  } else {
    ret_error_string!(String::from("Cannot pop from an empty vector."))
  }
}

fn pop_value_from_raw_front(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let raw = raw!(params, 0);
  let mut res = Vec::with_capacity(raw.len() - 1);
  let mut iter = raw.iter();
  iter.next();
  for val in iter {
    res.push(val.clone());
  }
  ret_val!(Value::Raw(Rc::new(res)))
}

fn pop_value_from_buffer_front(
  instance: &mut Instance,
  _: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  if buffer.len() > 0 {
    ret_val!(Value::Raw(Rc::new(vec![buffer.remove(0)])));
  } else {
    ret_error_string!(String::from("Cannot pop from an empty buffer."))
  }
}

fn pop_multiple_values_from_list_front(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = list!(params, 0);
  match number_utils::get_pos_inclusive(list, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(list.len() - to_pop);
      let iter = list.iter().skip(to_pop);
      for val in iter {
        res.push(val.clone());
      }
      ret_val!(Value::List(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a list of size {}",
      &params[1].to_string(instance, asl),
      list.len()
    )),
  }
}

fn pop_multiple_values_from_vector_front(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let vector = vector!(instance, params, 0);
  let size = vector.len();
  match number_utils::get_pos_inclusive(vector, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(to_pop);
      for _ in 0..to_pop {
        res.push(vector.remove(0));
      }
      ret_val!(Value::List(Rc::new(res)));
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a vector of size {}",
      &params[1].to_string(instance, asl),
      size
    )),
  }
}

fn pop_multiple_values_from_raw_front(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let raw = raw!(params, 0);
  match number_utils::get_pos_inclusive(raw, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(raw.len() - to_pop);
      let iter = raw.iter().skip(to_pop);
      for val in iter {
        res.push(val.clone());
      }
      ret_val!(Value::Raw(Rc::new(res)))
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a raw of size {}",
      &params[1].to_string(instance, asl),
      raw.len()
    )),
  }
}

fn pop_multiple_values_from_buffer_front(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let buffer = buffer!(instance, params, 0);
  let size = buffer.len();
  match number_utils::get_pos_inclusive(buffer, &params[1]) {
    Some(to_pop) => {
      let mut res = Vec::with_capacity(to_pop);
      for _ in 0..to_pop {
        res.push(buffer.remove(0));
      }
      ret_val!(Value::Raw(Rc::new(res)));
    }
    None => ret_error_string!(format!(
      "Cannot pop {} values from a buffer of size {}",
      &params[1].to_string(instance, asl),
      size
    )),
  }
}

// "join" native command

fn join_list_to_string(
  instance: &mut Instance,
  asl: &ASL,
  params: Vec<Value>,
) -> CommandExecReturn {
  let list = vector_or_list!(instance, params, 0).clone();
  ret_val!(Value::String(Rc::new(base_utils::sys_str_vec_to_str(
    instance,
    asl,
    &list,
    string!(params, 1),
  ))));
}

// "reverse" native command

fn reverse_string(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let value = string!(params, 0);
  let mut result = String::with_capacity(value.len());
  for c in value.chars().rev() {
    result.push(c);
  }
  ret_val!(Value::String(Rc::new(result)));
}

fn reverse_list(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let value = list!(params, 0);
  let mut result = Vec::with_capacity(value.len());
  for v in value.iter().rev() {
    result.push(v.clone());
  }
  ret_val!(Value::List(Rc::new(result)));
}

fn reverse_vector(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut value = vector!(instance, params, 0);
  let mut aux = Vec::with_capacity(value.len());
  aux.append(&mut value);
  for v in aux.into_iter().rev() {
    value.push(v);
  }
  ret_none!();
}

fn reverse_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let value = raw!(params, 0);
  let mut result = Vec::with_capacity(value.len());
  for v in value.iter().rev() {
    result.push(v.clone());
  }
  ret_val!(Value::Raw(Rc::new(result)));
}

fn reverse_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let mut value = buffer!(instance, params, 0);
  let mut aux = Vec::with_capacity(value.len());
  aux.append(&mut value);
  for v in aux.into_iter().rev() {
    value.push(v);
  }
  ret_none!();
}

// "resise" native command

fn resize_raw(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let value = raw!(params, 0);
  let new_size = match number_utils::get_unsigned(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Value must be able to be converted to a system unsigned number"),
  };
  if new_size < value.len() {
    let mut result = Vec::with_capacity(new_size);
    let mut i = 0;
    for v in value.iter() {
      if i == new_size {
        break;
      }
      result.push(v.clone());
      i += 1;
    }
    ret_val!(Value::Raw(Rc::new(result)));
  } else {
    let mut result = Vec::clone(value);
    for _ in 0..(new_size - value.len()) {
      result.push(0);
    }
    ret_val!(Value::Raw(Rc::new(result)));
  }
}

fn resize_buffer(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  let value = buffer!(instance, params, 0);
  let new_size = match number_utils::get_unsigned(&params[1]) {
    Some(val) => val,
    None => ret_error_str!("Value must be able to be converted to a system unsigned number"),
  };
  if new_size < value.len() {
    for _ in 0..(value.len() - new_size) {
      value.pop();
    }
  } else {
    for _ in 0..(new_size - value.len()) {
      value.push(0);
    }
  }
  ret_none!();
}

// "keys" native command

fn get_keys_from_dictionary(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(dictionary!(params, 0).get_key_list_as_value());
}

fn get_keys_from_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(map!(instance, params, 0).get_key_list_as_value());
}

// "values" native command

fn get_values_from_dictionary(_: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(dictionary!(params, 0).get_value_list_as_value());
}

fn get_values_from_map(instance: &mut Instance, _: &ASL, params: Vec<Value>) -> CommandExecReturn {
  ret_val!(map!(instance, params, 0).get_value_list_as_value());
}

pub fn load_native_functions(intr: &mut Instance) {
  let vector_or_list = Or!(Type::Vector, Type::List);
  def_multi_func!(
    intr,
    "get",
    [
      new_func!(
        [Type::List, Type::AnyNumber],
        Type::Any,
        get_value_from_list
      ),
      new_func!(
        [Type::Vector, Type::AnyNumber],
        Type::Any,
        get_value_from_vector
      ),
      new_func!(
        [Type::Dictionary, Type::String],
        Type::Any,
        get_value_from_dictionary
      ),
      new_func!([Type::Map, Type::String], Type::Any, get_value_from_map),
      new_func!(
        [Type::String, Type::AnyNumber],
        Type::String,
        get_char_from_string
      ),
      new_func!([Type::Raw, Type::AnyNumber], Type::Raw, get_value_from_raw),
      new_func!(
        [Type::Buffer, Type::AnyNumber],
        Type::Raw,
        get_value_from_buffer
      )
    ]
  );
  def_multi_func!(
    intr,
    "set",
    [
      new_func!(
        [Type::List, Type::AnyNumber, Type::Any],
        Type::List,
        set_value_from_list
      ),
      new_func!(
        [Type::Vector, Type::AnyNumber, Type::Any],
        Type::Void,
        set_value_from_vector
      ),
      new_func!(
        [Type::Dictionary, Type::String, Type::Any],
        Type::Dictionary,
        set_value_from_dictionary
      ),
      new_func!(
        [Type::Map, Type::String, Type::Any],
        Type::Void,
        set_value_from_map
      ),
      new_func!(
        [Type::String, Type::AnyNumber, Type::String],
        Type::String,
        set_value_on_string
      ),
      new_func!(
        [Type::Raw, Type::AnyNumber, Type::Raw],
        Type::Raw,
        set_value_on_raw
      ),
      new_func!(
        [Type::Buffer, Type::AnyNumber, Type::Raw],
        Type::Void,
        set_value_on_buffer
      )
    ]
  );
  def_multi_func!(
    intr,
    "rem",
    [
      new_func!(
        [Type::List, Type::AnyNumber],
        Type::List,
        remove_value_from_list
      ),
      new_func!(
        [Type::Vector, Type::AnyNumber],
        Type::Any,
        remove_value_from_vector
      ),
      new_func!(
        [Type::Dictionary, Type::String],
        Type::Dictionary,
        remove_value_from_dictionary
      ),
      new_func!([Type::Map, Type::String], Type::Any, remove_value_from_map)
    ]
  );
  def_multi_func!(
    intr,
    "contains",
    [
      new_func!([Type::String, Type::String], Type::Boolean, string_contains),
      new_func!([Type::List, Type::Any], Type::Boolean, list_contains),
      new_func!([Type::Vector, Type::Any], Type::Boolean, vector_contains),
      new_func!(
        [Type::Dictionary, Type::Any],
        Type::Boolean,
        dictionary_contains
      ),
      new_func!([Type::Map, Type::Any], Type::Boolean, map_contains),
    ]
  );
  def_multi_func!(
    intr,
    "contains_key",
    [
      new_func!(
        [Type::Dictionary, Type::String],
        Type::Boolean,
        dictionary_contains_key
      ),
      new_func!([Type::Map, Type::String], Type::Boolean, map_contains_key),
    ]
  );
  def_multi_func!(
    intr,
    "replace",
    [
      new_func!(
        [Type::String, Type::String, Type::String],
        Type::String,
        replace_in_string
      ),
      new_func!(
        [Type::List, Type::Any, Type::Any],
        Type::List,
        replace_in_list
      ),
      new_func!(
        [Type::Vector, Type::Any, Type::Any],
        Type::Void,
        replace_in_vector
      ),
    ]
  );
  def_multi_func!(
    intr,
    "size",
    [
      new_func!([Type::List], Type::Unsigned, get_size_list),
      new_func!([Type::Vector], Type::Unsigned, get_size_vector),
      new_func!([Type::String], Type::Unsigned, get_size_string),
      new_func!([Type::Dictionary], Type::Unsigned, get_size_dictionary),
      new_func!([Type::Map], Type::Unsigned, get_size_map),
      new_func!([Type::Raw], Type::Unsigned, get_size_raw),
      new_func!([Type::Buffer], Type::Unsigned, get_size_buffer),
    ]
  );
  def_multi_func!(
    intr,
    "size_u",
    [
      new_func!([Type::List], Type::PrimitiveUnsigned, get_size_list_u),
      new_func!([Type::Vector], Type::PrimitiveUnsigned, get_size_vector_u),
      new_func!([Type::String], Type::PrimitiveUnsigned, get_size_string_u),
      new_func!(
        [Type::Dictionary],
        Type::PrimitiveUnsigned,
        get_size_dictionary_u
      ),
      new_func!([Type::Map], Type::PrimitiveUnsigned, get_size_map_u),
      new_func!([Type::Raw], Type::PrimitiveUnsigned, get_size_raw_u),
      new_func!([Type::Buffer], Type::PrimitiveUnsigned, get_size_buffer_u),
    ]
  );
  def_multi_func!(
    intr,
    "size_s",
    [
      new_func!([Type::List], Type::PrimitiveSigned, get_size_list_s),
      new_func!([Type::Vector], Type::PrimitiveSigned, get_size_vector_s),
      new_func!([Type::String], Type::PrimitiveSigned, get_size_string_s),
      new_func!(
        [Type::Dictionary],
        Type::PrimitiveSigned,
        get_size_dictionary_s
      ),
      new_func!([Type::Map], Type::PrimitiveSigned, get_size_map_s),
      new_func!([Type::Raw], Type::PrimitiveSigned, get_size_raw_s),
      new_func!([Type::Buffer], Type::PrimitiveSigned, get_size_buffer_s),
    ]
  );
  def_multi_func!(
    intr,
    "split",
    [
      new_func!([Type::List, Type::AnyNumber], Type::List, split_list),
      new_func!([Type::String, Type::AnyNumber], Type::List, split_string),
      new_func!(
        [Type::String, Type::String],
        Type::List,
        split_string_by_separator
      ),
      new_func!([Type::Raw, Type::AnyNumber], Type::List, split_raw),
    ]
  );
  def_multi_func!(
    intr,
    "slice",
    [
      new_func!(
        [
          Or!(Type::List, Type::Vector),
          Type::AnyNumber,
          Type::AnyNumber
        ],
        Type::List,
        slice_list
      ),
      new_func!(
        [Type::String, Type::AnyNumber, Type::AnyNumber],
        Type::String,
        slice_string
      ),
      new_func!(
        [Type::Raw, Type::AnyNumber, Type::AnyNumber],
        Type::Raw,
        slice_raw
      ),
      new_func!(
        [Or!(Type::List, Type::Vector), Type::AnyNumber],
        Type::List,
        slice_list_to_end
      ),
      new_func!(
        [Type::String, Type::AnyNumber],
        Type::String,
        slice_string_to_end
      ),
      new_func!([Type::Raw, Type::AnyNumber], Type::Raw, slice_raw_to_end),
    ]
  );
  def_multi_func!(
    intr,
    "push",
    [
      new_func!([Type::List, Type::Any], Type::List, push_value_to_list),
      new_func!([Type::Vector, Type::Any], Type::Void, push_value_to_vector),
      new_func!([Type::Raw, Type::Raw], Type::Raw, push_to_raw),
      new_func!([Type::Buffer, Type::Raw], Type::Void, push_to_buffer)
    ]
  );
  def_multi_func!(
    intr,
    "pop",
    [
      new_func!([Type::List], Type::List, pop_value_from_list),
      new_func!([Type::Vector], Type::Any, pop_value_from_vector),
      new_func!([Type::Raw], Type::Raw, pop_value_from_raw),
      new_func!([Type::Buffer], Type::Raw, pop_value_from_buffer),
      new_func!(
        [Type::List, Type::AnyNumber],
        Type::List,
        pop_multiple_values_from_list
      ),
      new_func!(
        [Type::Vector, Type::AnyNumber],
        Type::Any,
        pop_multiple_values_from_vector
      ),
      new_func!(
        [Type::Raw, Type::AnyNumber],
        Type::Raw,
        pop_multiple_values_from_raw
      ),
      new_func!(
        [Type::Buffer, Type::AnyNumber],
        Type::Raw,
        pop_multiple_values_from_buffer
      )
    ]
  );
  def_multi_func!(
    intr,
    "push_f",
    [
      new_func!(
        [Type::List, Type::Any],
        Type::List,
        push_value_to_list_front
      ),
      new_func!(
        [Type::Vector, Type::Any],
        Type::Void,
        push_value_to_vector_front
      ),
      new_func!([Type::Raw, Type::Raw], Type::Raw, push_value_to_raw_front),
      new_func!(
        [Type::Buffer, Type::Raw],
        Type::Void,
        push_value_to_buffer_front
      )
    ]
  );
  def_multi_func!(
    intr,
    "pop_f",
    [
      new_func!([Type::List], Type::List, pop_value_from_list_front),
      new_func!([Type::Vector], Type::Any, pop_value_from_vector_front),
      new_func!([Type::Raw], Type::Raw, pop_value_from_raw_front),
      new_func!([Type::Buffer], Type::Raw, pop_value_from_buffer_front),
      new_func!(
        [Type::List, Type::AnyNumber],
        Type::List,
        pop_multiple_values_from_list_front
      ),
      new_func!(
        [Type::Vector, Type::AnyNumber],
        Type::List,
        pop_multiple_values_from_vector_front
      ),
      new_func!(
        [Type::Raw, Type::AnyNumber],
        Type::Raw,
        pop_multiple_values_from_raw_front
      ),
      new_func!(
        [Type::Buffer, Type::AnyNumber],
        Type::Raw,
        pop_multiple_values_from_buffer_front
      )
    ]
  );
  def_func!(
    intr,
    "join",
    [vector_or_list.clone(), Type::String],
    Type::String,
    join_list_to_string
  );
  def_multi_func!(
    intr,
    "reverse",
    [
      new_func!([Type::String], Type::String, reverse_string),
      new_func!([Type::List], Type::List, reverse_list),
      new_func!([Type::Vector], Type::Void, reverse_vector),
      new_func!([Type::Raw], Type::Raw, reverse_raw),
      new_func!([Type::Buffer], Type::Void, reverse_buffer),
    ]
  );
  def_multi_func!(
    intr,
    "resize",
    [
      new_func!([Type::Raw, Type::AnyNumber], Type::Raw, resize_raw),
      new_func!([Type::Buffer, Type::AnyNumber], Type::Void, resize_buffer),
    ]
  );
  def_multi_func!(
    intr,
    "keys",
    [
      new_func!(
        [Type::Dictionary],
        ListOf!(Type::String),
        get_keys_from_dictionary
      ),
      new_func!([Type::Map], ListOf!(Type::String), get_keys_from_map)
    ]
  );
  def_multi_func!(
    intr,
    "values",
    [
      new_func!([Type::Dictionary], Type::List, get_values_from_dictionary),
      new_func!([Type::Map], Type::List, get_values_from_map)
    ]
  );
}
