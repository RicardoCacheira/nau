use nau::asl;
use nau::instance::Instance;
use nau::native_functions;
use nau::parsers::parser_consts::{COMMENT_BLOCK_CHAR, COMMENT_START_CHAR};
use nau::scopes::local_id_manager::INTERNAL_ID_INDICATOR;
use nau::utils::base_utils;
use nau::values::{SystemType, Value};
use std::env;
use std::fs;
use std::rc::Rc;
use std::time::SystemTime;

fn main() {
  let start_seed = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
    Ok(n) => n.as_secs(),
    Err(_) => panic!("SystemTime before UNIX EPOCH!"),
  };
  let asl = asl::create();
  asl.console.cursor_off();
  let mut instance = Instance::new(base_utils::is_debug(), start_seed);
  instance.add_base_values();
  instance.set_global_var("OS", Value::String(Rc::new(asl.system.os.clone())));
  instance.set_global_var(
    "FS_SEPARATOR",
    Value::String(Rc::new(asl.fs.separator.clone())),
  );
  SystemType::load_types_to_instance(&mut instance);
  native_functions::load_native_functions(&mut instance);
  let mut args = env::args();
  //Removes the path for the executable from the args
  args.next();
  let exec_path = String::from(
    std::env::current_exe()
      .unwrap()
      .to_str()
      .expect("Unable to get executable path"),
  );
  let args: Vec<String> = args.collect();
  instance.set_global_var(
    "ARGS",
    Value::List(Rc::new(
      args
        .iter()
        .map(|x| Value::String(Rc::new(x.clone())))
        .collect(),
    )),
  );

  //Absolute path for the executable directory
  let mut system_path = exec_path.clone();
  while !(system_path.pop() == Some(asl.fs.separator_char)) {}

  let bootstrap_path = format!("{}{}.nau_bootstrap", system_path, asl.fs.separator_char);
  let contents = format!(
    "{}{}{}{}{}{}{}{}",
    COMMENT_START_CHAR,
    COMMENT_BLOCK_CHAR,
    COMMENT_START_CHAR,
    bootstrap_path,
    COMMENT_START_CHAR,
    COMMENT_BLOCK_CHAR,
    COMMENT_START_CHAR,
    fs::read_to_string(bootstrap_path.as_str()).expect("Unable to read the .nau_bootstap file"),
  );

  let (token_list, scope_list) = instance.parse(contents).unwrap();
  // println!("---DEBUG---\n{}\n---DEBUG--\n",utils::vec_to_str(&com,"\n"));
  instance.add_to_queue(token_list, scope_list);
  let id_prefix_manager = instance.get_id_prefix_manager();
  let mut internal_id_indicator = String::new();
  internal_id_indicator.push(INTERNAL_ID_INDICATOR);
  //Registering the internal id on the manager to keep tthe code from using it
  id_prefix_manager
    .add_prefix(&internal_id_indicator)
    .unwrap();
  //Id Prefix Manager Get Master Key
  let ipmmk = id_prefix_manager.get_master_key();
  instance.set_global_var(
    "IPMMK",
    Value::List(Rc::new(vec![Value::String(Rc::new(ipmmk))])),
  );
  instance.set_global_var("EXEC", Value::String(Rc::new(exec_path)));
  instance.set_global_var("SYS", Value::String(Rc::new(system_path)));
  instance.set_global_var(
    "START",
    Value::String(Rc::new(String::from(
      env::current_dir().unwrap().to_str().unwrap(),
    ))),
  );
  instance.set_global_var("DEBUG", Value::Boolean(base_utils::is_debug()));
  if base_utils::is_debug() {
    println!("DEBUG MODE")
  }
  let result = instance.run(&asl);
  asl.console.cursor_on();
  if let Err(err) = result {
    asl.console.println(&format!("\nERROR: {}", err));
  }
  //send exit code
}
