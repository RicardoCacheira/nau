use std::fs::OpenOptions;
use std::io::Write;
use std::path::{Path, MAIN_SEPARATOR};
use std::{env, fs};

pub struct FileSystemIO {
  pub separator: String,
  pub separator_char: char,
}

impl FileSystemIO {
  pub fn new() -> Self {
    let mut separator = String::with_capacity(1);
    separator.push(MAIN_SEPARATOR);
    FileSystemIO {
      separator,
      separator_char: MAIN_SEPARATOR,
    }
  }

  pub fn read_file_as_string(&self, path: &str) -> Result<String, String> {
    match fs::read_to_string(path) {
      Ok(file) => Ok(file),
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn write_file_as_string(&self, path: &str, file: &str) -> Result<(), String> {
    match fs::write(path, file) {
      Ok(_) => Ok(()),
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn append_file_as_string(&self, path: &str, data: &str) -> Result<(), String> {
    match OpenOptions::new().append(true).open(path) {
      Ok(mut file) => match file.write(data.as_bytes()) {
        Ok(_) => Ok(()),
        Err(err) => Err(err.to_string()),
      },
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn read_file(&self, path: &str) -> Result<Vec<u8>, String> {
    match fs::read(path) {
      Ok(file) => Ok(file),
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn write_file(&self, path: &str, file: &Vec<u8>) -> Result<(), String> {
    match fs::write(path, file) {
      Ok(_) => Ok(()),
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn append_file(&self, path: &str, data: &Vec<u8>) -> Result<(), String> {
    match OpenOptions::new().append(true).open(path) {
      Ok(mut file) => match file.write(data) {
        Ok(_) => Ok(()),
        Err(err) => Err(err.to_string()),
      },
      Err(err) => Err(err.to_string()),
    }
  }

  //TODO Review other ways to check if a file exist
  pub fn file_exists(&self, path: &str) -> bool {
    match fs::metadata(path) {
      Ok(_) => true,
      Err(_) => false,
    }
  }

  pub fn is_file(&self, path: &str) -> bool {
    match fs::metadata(path) {
      Ok(metadata) => metadata.is_file(),
      Err(_) => false,
    }
  }

  pub fn is_dir(&self, path: &str) -> bool {
    match fs::metadata(path) {
      Ok(metadata) => metadata.is_dir(),
      Err(_) => false,
    }
  }

  pub fn is_symlink(&self, path: &str) -> bool {
    match fs::metadata(path) {
      Ok(metadata) => metadata.file_type().is_symlink(),
      Err(_) => false,
    }
  }

  pub fn is_absolute(&self, path: &str) -> bool {
    Path::new(path).is_absolute()
  }

  pub fn get_cwd(&self) -> Result<String, String> {
    match env::current_dir() {
      Ok(path) => match path.to_str() {
        Some(path_str) => Ok(String::from(path_str)),
        None => Err(String::from("Unable to get a valid path string.")),
      },
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn set_cwd(&self, path: &str) -> Result<(), String> {
    let path = Path::new(path);
    match env::set_current_dir(path) {
      Ok(_) => Ok(()),
      Err(err) => Err(err.to_string()),
    }
  }

  pub fn list_dir(&self, dir_path: &str) -> Result<Vec<String>, String> {
    let path = Path::new(dir_path);
    match fs::read_dir(path) {
      Ok(dir_iter) => {
        let mut res = Vec::new();

        for l in dir_iter {
          match l {
            Ok(dir_entry) => res.push(String::from(dir_entry.path().to_str().unwrap())),
            Err(err) => return Err(err.to_string()),
          }
        }
        //res.sort();
        Ok(res)
      }
      Err(err) => Err(err.to_string()),
    }
  }
}
