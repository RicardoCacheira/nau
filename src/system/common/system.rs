pub struct BaseSystem {
  pub os: String,
}

impl BaseSystem {
  pub fn new() -> Self {
    BaseSystem {
      os: String::from(std::env::consts::OS),
    }
  }

  pub fn run_command(&self, _: &str) -> Result<(String, String), String> {
    Err(String::from("Unimplemented system function."))
  }
}
