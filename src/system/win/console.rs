

pub struct ConsoleIO {
}

impl ConsoleIO {
  pub fn new() -> Self {
    ConsoleIO{}
  }

  pub fn print_direct(&self, s: &str) {
   print!("{}",s);
  }

  pub fn print(&self, s: &str) {
   print!("{}",s)
  }

  pub fn println(&self, s: &str) {
    println!("{}",s)

  }

  pub fn read(&self) -> String {
   String::new()
  }

  pub fn size(&self) -> (usize, usize) {
    (0,0)
  }

  pub fn get_cursor_pos(&self) -> (usize, usize) {
    (0,0)
  }

  pub fn clear(&self) {
  }

  pub fn set_cursor_pos(&self, _: usize, _: usize) {
  }

  pub fn cursor_on(&self) {
  }

  pub fn cursor_off(&self) {
  }
}
