use std::process::Command;

pub struct BaseSystem {
  pub os: String,
}

impl BaseSystem {
  pub fn new() -> Self {
    BaseSystem {
      os: String::from(std::env::consts::OS),
    }
  }

  pub fn run_command(&self, command: &str) -> Result<(String, String), String> {
    match Command::new("C:\\Windows\\system32\\cmd.exe").arg("/C").arg(command).output() {
      Ok(output) => Ok((
        String::from_utf8(output.stdout).unwrap(),
        String::from_utf8(output.stderr).unwrap(),
      )),
      Err(err) => Err(err.to_string()),
    }
  }
}
