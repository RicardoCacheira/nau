use crate::asl_common::FileSystemIO;
use crate::asl_common::ConsoleIO;
use crate::asl_common::BaseSystem;

//Abstract System Layer
pub struct ASL {
  pub console: ConsoleIO,
  pub fs: FileSystemIO,
  pub system: BaseSystem,
}

pub fn create() -> ASL {
  ASL {
    console: ConsoleIO::new(),
    fs: FileSystemIO::new(),
    system: BaseSystem::new(),
  }
}
