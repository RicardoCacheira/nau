use libc::{ioctl, winsize, STDOUT_FILENO, TIOCGWINSZ};
use std::ffi::CString;
use std::os::unix::io::AsRawFd;
use std::{fs, io, mem};
use termios;

pub struct ConsoleIO {
  fd: i32,
  original: termios::Termios,
}

impl ConsoleIO {
  pub fn new() -> Self {
    let tty_f;
    let fd = unsafe {
      if libc::isatty(libc::STDIN_FILENO) == 1 {
        libc::STDIN_FILENO
      } else {
        tty_f = fs::File::open("/dev/tty").unwrap();
        tty_f.as_raw_fd()
      }
    };
    let mut termios = termios::Termios::from_fd(fd).unwrap();
    let original = termios;
    termios::cfmakeraw(&mut termios);
    termios::tcsetattr(fd, termios::TCSADRAIN, &termios).unwrap();
    ConsoleIO { fd, original }
  }

  pub fn print_direct(&self, s: &str) {
    unsafe {
      libc::write(
        self.fd,
        CString::new(s).unwrap().as_ptr() as *const libc::c_void,
        s.len(),
      );
    }
  }

  pub fn print(&self, s: &str) {
    self.print_direct(&s.replace("\n", "\r\n"));
  }

  pub fn println(&self, s: &str) {
    let mut s = s.replace("\n", "\r\n");
    s.push_str("\r\n");
    self.print_direct(&s);
  }

  //Note: if the read reads the buffer max size it will check if the read values are valid utf8, and if not, it reads the buffer again to get the likely remainer of a utf-8 char that was cut while reading
  pub fn read(&self) -> String {
    let mut aux = Vec::new();
    unsafe {
      loop {
        let mut buf = [0u8; 1024];
        let read = libc::read(self.fd, buf.as_mut_ptr() as *mut libc::c_void, 1024);
        if read < 0 {
          panic!(io::Error::last_os_error())
        } else {
          aux.extend_from_slice(&buf[..read as usize]);
        }
        if read == 10 {
          if let Ok(result) = String::from_utf8(aux.clone()) {
            return result;
          }
        } else {
          break;
        }
      }
    };
    String::from_utf8(aux).unwrap()
  }

  pub fn size(&self) -> (usize, usize) {
    unsafe {
      let mut size: winsize = mem::zeroed();
      ioctl(STDOUT_FILENO, TIOCGWINSZ.into(), &mut size as *mut _);
      (size.ws_col as usize, size.ws_row as usize)
    }
  }

  pub fn get_cursor_pos(&self) -> (usize, usize) {
    self.print_direct("\x1b[6n");
    let pos_output = self.read();
    unsafe {
      let mut iter = pos_output
        .get_unchecked(2..(pos_output.len() - 1))
        .split(";");
      let row = iter.next().unwrap().parse::<usize>().unwrap() - 1;
      let col = iter.next().unwrap().parse::<usize>().unwrap() - 1;
      (col, row)
    }
  }

  pub fn clear(&self) {
    self.print_direct("\x1b[1;1H\x1b[0J");
  }

  pub fn set_cursor_pos(&self, col: usize, row: usize) {
    self.print_direct(&format!("\x1b[{};{}H", row + 1, col + 1));
  }

  pub fn cursor_on(&self) {
    self.print_direct("\x1b[?25h");
  }

  pub fn cursor_off(&self) {
    self.print_direct("\x1b[?25l");
  }
}

impl Drop for ConsoleIO {
  fn drop(&mut self) {
    termios::tcsetattr(self.fd, termios::TCSADRAIN, &self.original).unwrap();
  }
}
