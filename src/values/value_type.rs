use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::parsers::{var_checker, Token};
use crate::scopes::ScopeId;
use crate::utils::to_system_string::ToSystemString;
use crate::utils::{base_utils, number_utils};
use crate::values::{FunctionType, Value, ValueMapInterface};
use num_traits::identities::Zero;
use num_traits::sign::Signed;
use std::rc::Rc;

#[derive(Clone, PartialEq)]
pub enum SystemType {
  AnyNumber,
  Number,
  PrimitiveNumber,
  PrimitiveUnsigned,
  PrimitiveSigned,
  PrimitiveFloat,
  Integer,
  NonNegative,
  NonPositive,
  Unsigned,
  Positive,
  Negative,
  SystemUnsigned,
  Natural,
  String,
  Raw,
  VarString,
  Boolean,
  Reference,
  GlobalReference,
  LocalReference,
  BlockReference,
  Type,
  ParamType,
  ValueType,
  List,
  Vector,
  Dictionary,
  Map,
  Buffer,
  Callable,
  FunCom,
  Meta,
  Command,
  Function,
  SingleFunction,
  CommandFunction,
  NativeFunction,
  MultiFunction,
  Void,
  Permeable,
  Any,
  //Special type
  Voidable(Rc<Self>),
  Multiple(Rc<Vec<Self>>),
  Many(Rc<Vec<Self>>),
  //Definable types
  Set(Rc<Vec<Self>>),
  ListOf(Rc<Vec<Self>>),
  UniqueList(Rc<Vec<Self>>),
  Sequence(Rc<Vec<Self>>),
  Sized {
    value_type: Rc<Self>,
    min: usize,
    max: usize,
  },
  VectorOf(Rc<Vec<Self>>),
  DictionaryOf(Rc<Vec<Self>>),
  MapOf(Rc<Vec<Self>>),
  Interface(Rc<ValueMapInterface>),
  DictionaryInterface(Rc<ValueMapInterface>),
  MapInterface(Rc<ValueMapInterface>),
  Or(Rc<Vec<Self>>),
  And(Rc<Vec<Self>>),
  RefOf(Rc<Self>),
  //TODO Add Value Type
  //Value(Rc<Value>),
  ValueOption(Rc<Vec<Value>>),
  Check {
    value_type: Rc<Self>,
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
  },
  Not(Rc<Self>),
  SelfType(Rc<String>),
  SubType(Rc<Vec<Self>>),
  SuperType(Rc<Vec<Self>>),
  WrappedValue,
  CustomType,
  CustomTypeInstance {
    name: Rc<String>,
    value_type: Rc<Self>,
    to_string_token_list: Rc<Vec<Token>>,
    to_string_id_list: Rc<Vec<usize>>,
  },
  Dynamic(ScopeId),
}

impl SystemType {
  pub fn load_types_to_instance(instance: &mut Instance) {
    instance.set_global_var("AnyNumber", Value::Type(Self::AnyNumber));
    instance.set_global_var("Number", Value::Type(Self::Number));
    instance.set_global_var("PrimitiveNumber", Value::Type(Self::PrimitiveNumber));
    instance.set_global_var("PrimitiveUnsigned", Value::Type(Self::PrimitiveUnsigned));
    instance.set_global_var("PrimitiveSigned", Value::Type(Self::PrimitiveSigned));
    instance.set_global_var("PrimitiveFloat", Value::Type(Self::PrimitiveFloat));
    instance.set_global_var("Integer", Value::Type(Self::Integer));
    instance.set_global_var("NonNegative", Value::Type(Self::NonNegative));
    instance.set_global_var("NonPositive", Value::Type(Self::NonPositive));
    instance.set_global_var("Unsigned", Value::Type(Self::Unsigned));
    instance.set_global_var("Positive", Value::Type(Self::Positive));
    instance.set_global_var("Negative", Value::Type(Self::Negative));
    instance.set_global_var("SystemUnsigned", Value::Type(Self::SystemUnsigned));
    instance.set_global_var("Natural", Value::Type(Self::Natural));
    instance.set_global_var("String", Value::Type(Self::String));
    instance.set_global_var("Raw", Value::Type(Self::Raw));
    instance.set_global_var("VarString", Value::Type(Self::VarString));
    instance.set_global_var("Boolean", Value::Type(Self::Boolean));
    instance.set_global_var("Reference", Value::Type(Self::Reference));
    instance.set_global_var("WrappedValue", Value::Type(Self::WrappedValue));
    instance.set_global_var("CustomType", Value::Type(Self::CustomType));
    instance.set_global_var("GlobalReference", Value::Type(Self::GlobalReference));
    instance.set_global_var("LocalReference", Value::Type(Self::LocalReference));
    instance.set_global_var("BlockReference", Value::Type(Self::BlockReference));
    instance.set_global_var("Type", Value::Type(Self::Type));
    instance.set_global_var("ParamType", Value::Type(Self::ParamType));
    instance.set_global_var("ValueType", Value::Type(Self::ValueType));
    instance.set_global_var("List", Value::Type(Self::List));
    instance.set_global_var("Vector", Value::Type(Self::Vector));
    instance.set_global_var("Dictionary", Value::Type(Self::Dictionary));
    instance.set_global_var("Map", Value::Type(Self::Map));
    instance.set_global_var("Buffer", Value::Type(Self::Buffer));
    instance.set_global_var("Callable", Value::Type(Self::Callable));
    instance.set_global_var("FunCom", Value::Type(Self::FunCom));
    instance.set_global_var("Meta", Value::Type(Self::Meta));
    instance.set_global_var("Command", Value::Type(Self::Command));
    instance.set_global_var("Function", Value::Type(Self::Function));
    instance.set_global_var("SingleFunction", Value::Type(Self::SingleFunction));
    instance.set_global_var("CommandFunction", Value::Type(Self::CommandFunction));
    instance.set_global_var("NativeFunction", Value::Type(Self::NativeFunction));
    instance.set_global_var("MultiFunction", Value::Type(Self::MultiFunction));
    instance.set_global_var("Void", Value::Type(Self::Void));
    instance.set_global_var("Permeable", Value::Type(Self::Permeable));
    instance.set_global_var("Any", Value::Type(Self::Any));
    crate::native_functions::load_native_type_functions(instance);
  }
}

impl ToSystemString for SystemType {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    match self {
      Self::AnyNumber => return String::from("AnyNumber"),
      Self::Number => return String::from("Number"),
      Self::PrimitiveNumber => return String::from("PrimitiveNumber"),
      Self::PrimitiveUnsigned => return String::from("PrimitiveUnsigned"),
      Self::PrimitiveSigned => return String::from("PrimitiveSigned"),
      Self::PrimitiveFloat => return String::from("PrimitiveFloat"),
      Self::Integer => return String::from("Integer"),
      Self::NonNegative => return String::from("NonNegative"),
      Self::NonPositive => return String::from("NonPositive"),
      Self::Unsigned => return String::from("Unsigned"),
      Self::Positive => return String::from("Positive"),
      Self::Negative => return String::from("Negative"),
      Self::SystemUnsigned => return String::from("SystemUnsigned"),
      Self::Natural => return String::from("Natural"),
      Self::String => return String::from("String"),
      Self::Raw => return String::from("Raw"),
      Self::VarString => return String::from("VarString"),
      Self::Boolean => return String::from("Boolean"),
      Self::Reference => return String::from("Reference"),
      Self::GlobalReference => return String::from("GlobalReference"),
      Self::LocalReference => return String::from("LocalReference"),
      Self::BlockReference => return String::from("BlockReference"),
      Self::Type => return String::from("Type"),
      Self::ParamType => return String::from("ParamType"),
      Self::ValueType => return String::from("ValueType"),
      Self::List => return String::from("List"),
      Self::Vector => return String::from("Vector"),
      Self::Dictionary => return String::from("Dictionary"),
      Self::Map => return String::from("Map"),
      Self::Buffer => return String::from("Buffer"),
      Self::Callable => return String::from("Callable"),
      Self::FunCom => return String::from("FunCom"),
      Self::Meta => return String::from("Meta"),
      Self::Command => return String::from("Command"),
      Self::Function => return String::from("Function"),
      Self::SingleFunction => return String::from("SingleFunction"),
      Self::CommandFunction => return String::from("CommandFunction"),
      Self::NativeFunction => return String::from("NativeFunction"),
      Self::MultiFunction => return String::from("MultiFunction"),
      Self::WrappedValue => return String::from("WrappedValue"),
      Self::CustomType => return String::from("CustomType"),
      Self::Void => return String::from("Void"),
      Self::Permeable => return String::from("Permeable"),
      Self::Any => return String::from("Any"),
      Self::Voidable(t) => {
        return format!("Voidable( {} )", t.to_string(instance, asl));
      }
      Self::Multiple(types) => {
        return format!(
          "Multiple( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::Many(types) => {
        return format!(
          "Many( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      //Definable types
      Self::Set(types) => {
        return format!(
          "Set( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::ListOf(types) => {
        return format!(
          "ListOf( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::UniqueList(types) => {
        return format!(
          "UniqueList( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::Sequence(types) => {
        return format!(
          "Sequence( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::VectorOf(types) => {
        return format!(
          "VectorOf( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::DictionaryOf(types) => {
        return format!(
          "DictionaryOf( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::MapOf(types) => {
        return format!(
          "MapOf( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::Interface(interface) => {
        return format!("Interface( {} )", interface.to_string(instance, asl));
      }
      Self::DictionaryInterface(interface) => {
        return format!(
          "DictionaryInterface( {} )",
          interface.to_string(instance, asl)
        );
      }
      Self::MapInterface(interface) => {
        return format!("MapInterface( {} )", interface.to_string(instance, asl));
      }
      Self::Sized {
        value_type,
        min,
        max,
      } => {
        return format!(
          "Sized( {}, {}, {} )",
          value_type.to_string(instance, asl),
          min,
          max
        );
      }
      Self::Or(types) => {
        return format!(
          "Or( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::And(types) => {
        return format!(
          "And( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::RefOf(t) => {
        return format!("RefOf( {} )", t.to_string(instance, asl));
      }
      Self::ValueOption(values) => {
        return format!(
          "ValueOption( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, values, ", ")
        );
      }
      Self::Check {
        value_type,
        token_list,
        ..
      } => {
        return format!(
          "Check( {}, {} )",
          value_type.to_string(instance, asl),
          base_utils::sys_str_vec_to_str(instance, asl, token_list, ", ")
        );
      }
      Self::Not(t) => {
        return format!("Not( {} )", t.to_string(instance, asl));
      }
      Self::SelfType(value) => {
        return value.to_string();
      }
      Self::SubType(types) => {
        return format!(
          "SubType( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::SuperType(types) => {
        return format!(
          "SuperType( {} )",
          base_utils::sys_str_vec_to_str(instance, asl, types, ", ")
        );
      }
      Self::CustomTypeInstance { name, .. } => {
        return name.as_ref().clone();
      }
      Self::Dynamic(reference) => {
        return format!("Dynamic( {} )", reference.to_string(instance, asl));
      }
    }
  }
}

impl SystemType {
  //TODO Maybe return a result instead of hiding the possible errors from check
  pub fn is_type_of_with_previous(
    &self,
    instance: &mut Instance,
    asl: &ASL,
    value: &Value,
    previous_values: &Vec<Value>,
  ) -> bool {
    match self {
      Self::Any => return true,
      Self::AnyNumber => {
        if let Value::Number(_) | Value::Unsigned(_) | Value::Signed(_) | Value::Float(_) = value {
          return true;
        }
      }
      Self::Number => {
        if let Value::Number(_) = value {
          return true;
        }
      }
      Self::Integer => {
        if let Value::Number(num) = value {
          return num.is_integer();
        }
      }
      Self::NonNegative => {
        if let Value::Number(num) = value {
          return num.is_zero() || num.is_positive();
        }
      }
      Self::NonPositive => {
        if let Value::Number(num) = value {
          return num.is_zero() || num.is_negative();
        }
      }
      Self::Unsigned => {
        if let Value::Number(num) = value {
          return num.is_integer() && (num.is_zero() || num.is_positive());
        }
      }
      Self::Positive => {
        if let Value::Number(num) = value {
          return num.is_positive();
        }
      }
      Self::Negative => {
        if let Value::Number(num) = value {
          return num.is_negative();
        }
      }
      Self::SystemUnsigned => {
        if let Value::Number(num) = value {
          return num.is_integer()
            && (num.is_zero() || (num.is_positive() && num <= &number_utils::get_max_unsigned()));
        }
      }
      Self::Natural => {
        if let Value::Number(num) = value {
          return num.is_integer() && num.is_positive();
        }
      }
      Self::PrimitiveNumber => {
        if let Value::Unsigned(_) | Value::Signed(_) | Value::Float(_) = value {
          return true;
        }
      }
      Self::PrimitiveUnsigned => {
        if let Value::Unsigned(_) = value {
          return true;
        }
      }
      Self::PrimitiveSigned => {
        if let Value::Signed(_) = value {
          return true;
        }
      }
      Self::PrimitiveFloat => {
        if let Value::Float(_) = value {
          return true;
        }
      }
      Self::String => {
        if let Value::String(_) = value {
          return true;
        }
      }
      Self::VarString => {
        if let Value::String(s) = value {
          return var_checker::check_if_var_is_valid(s) == Ok(());
        }
      }
      Self::Raw => {
        if let Value::Raw(_) = value {
          return true;
        }
      }
      Self::Boolean => {
        if let Value::Boolean(_) = value {
          return true;
        }
      }
      Self::Reference => {
        if let Value::Reference(_) = value {
          return true;
        }
      }
      Self::GlobalReference => {
        if let Value::Reference(ScopeId::Global(_)) = value {
          return true;
        }
      }
      Self::LocalReference => {
        if let Value::Reference(ScopeId::Local(_)) = value {
          return true;
        }
      }
      Self::BlockReference => {
        if let Value::Reference(ScopeId::Block(_)) = value {
          return true;
        }
      }
      Self::Type => {
        if let Value::Type(_) = value {
          return true;
        }
      }
      Self::ParamType => {
        if let Value::Type(Self::Void)
        | Value::Type(Self::Permeable)
        | Value::Type(Self::Voidable(_)) = value
        {
          return false;
        } else {
          return true;
        }
      }
      Self::ValueType => {
        if let Value::Type(Self::Void)
        | Value::Type(Self::Permeable)
        | Value::Type(Self::Voidable(_))
        | Value::Type(Self::Multiple(_))
        | Value::Type(Self::Many(_)) = value
        {
          return false;
        } else {
          return true;
        }
      }
      Self::List => {
        if let Value::List(_) = value {
          return true;
        }
      }
      Self::Vector => {
        if let Value::Vector(_) = value {
          return true;
        }
      }
      Self::Dictionary => {
        if let Value::Dictionary(_) = value {
          return true;
        }
      }
      Self::Map => {
        if let Value::Map(_) = value {
          return true;
        }
      }
      Self::Buffer => {
        if let Value::Buffer(_) = value {
          return true;
        }
      }
      Self::Callable => {
        if let Value::Command { .. }
        | Value::Function { .. }
        | Value::Meta { .. }
        | Value::MultiFunction(_) = value
        {
          return true;
        }
      }
      Self::FunCom => {
        if let Value::Command { .. } | Value::Function { .. } | Value::MultiFunction(_) = value {
          return true;
        }
      }
      Self::Meta => {
        if let Value::Meta { .. } = value {
          return true;
        }
      }
      Self::Command => {
        if let Value::Command { .. } = value {
          return true;
        }
      }
      Self::Function => {
        if let Value::Function { .. } | Value::MultiFunction(_) = value {
          return true;
        }
      }
      Self::SingleFunction => {
        if let Value::Function { .. } = value {
          return true;
        }
      }

      Self::CommandFunction => {
        if let Value::Function {
          function_type: FunctionType::Command { .. },
          ..
        } = value
        {
          return true;
        }
      }
      Self::NativeFunction => {
        if let Value::Function {
          function_type: FunctionType::Native(_),
          ..
        } = value
        {
          return true;
        }
      }
      Self::MultiFunction => {
        if let Value::MultiFunction(_) = value {
          return true;
        }
      }
      Self::Set(types) => {
        if let Value::List(values) = value {
          if types.len() == values.len() {
            for (t, v) in types.iter().zip(values.iter()) {
              if !t.is_type_of_with_previous(instance, asl, v, previous_values) {
                return false;
              }
            }
            return true;
          }
          return false;
        }
      }
      Self::ListOf(types) => {
        if let Value::List(values) = value {
          for v in values.iter() {
            let mut is_valid = false;
            for t in types.iter() {
              if t.is_type_of_with_previous(instance, asl, v, previous_values) {
                is_valid = true;
                break;
              }
            }
            if !is_valid {
              return false;
            }
          }
          return true;
        }
      }
      Self::UniqueList(types) => {
        if let Value::List(values) = value {
          let mut checked_values = Vec::with_capacity(values.len());
          for v in values.iter() {
            let mut is_valid = false;
            for t in types.iter() {
              if t.is_type_of_with_previous(instance, asl, v, previous_values) {
                is_valid = true;
                break;
              }
            }
            if !is_valid || checked_values.contains(v) {
              return false;
            }
            checked_values.push(v.clone());
          }
          return true;
        }
      }
      Self::Sequence(types) => {
        if let Value::List(values) = value {
          let type_num = types.len();
          if values.len() % type_num == 0 {
            for (i, v) in values.iter().enumerate() {
              if !types[i % type_num].is_type_of_with_previous(instance, asl, v, previous_values) {
                return false;
              }
            }
            return true;
          }
          return false;
        }
      }
      Self::VectorOf(types) => {
        if let Value::Vector(id) = value {
          if let Ok(vec) = instance.get_vector(id) {
            for v in vec.clone().iter() {
              let mut is_valid = false;
              for t in types.iter() {
                if t.is_type_of_with_previous(instance, asl, v, previous_values) {
                  is_valid = true;
                  break;
                }
              }
              if !is_valid {
                return false;
              }
            }
            return true;
          } else {
            return false;
          }
        }
      }
      Self::DictionaryOf(types) => {
        if let Value::Dictionary(value_map) = value {
          for v in value_map.get_value_list().iter() {
            let mut is_valid = false;
            for t in types.iter() {
              if t.is_type_of_with_previous(instance, asl, v, previous_values) {
                is_valid = true;
                break;
              }
            }
            if !is_valid {
              return false;
            }
          }
          return true;
        }
      }
      Self::MapOf(types) => {
        if let Value::Map(id) = value {
          if let Ok(map) = instance.get_map(id) {
            for v in map.get_value_list().iter() {
              let mut is_valid = false;
              for t in types.iter() {
                if t.is_type_of_with_previous(instance, asl, v, previous_values) {
                  is_valid = true;
                  break;
                }
              }
              if !is_valid {
                return false;
              }
            }
            return true;
          } else {
            return false;
          }
        }
      }
      Self::Interface(interface) => match value {
        Value::Dictionary(value_map) => {
          return interface.is_interface_for(instance, asl, value_map);
        }
        Value::Map(id) => {
          if let Ok(map) = instance.get_map(id) {
            let map = map.clone();
            return interface.is_interface_for(instance, asl, &map);
          } else {
            return false;
          }
        }
        _ => return false,
      },
      Self::DictionaryInterface(interface) => {
        if let Value::Dictionary(value_map) = value {
          return interface.is_interface_for(instance, asl, value_map);
        }
      }
      Self::MapInterface(interface) => {
        if let Value::Map(id) = value {
          if let Ok(map) = instance.get_map(id) {
            let map = map.clone();
            return interface.is_interface_for(instance, asl, &map);
          } else {
            return false;
          }
        }
      }

      Self::Sized {
        value_type,
        min,
        max,
      } => match value {
        Value::List(values) => {
          let values_num = values.len();
          let min = min.clone();
          let max = max.clone();
          if min <= values_num && (max == 0 || values_num <= max) {
            return value_type.is_type_of_with_previous(instance, asl, value, previous_values);
          }
          return false;
        }

        Value::String(string) => {
          let values_num = string.chars().count();
          let min = min.clone();
          let max = max.clone();
          if min <= values_num && (max == 0 || values_num <= max) {
            return value_type.is_type_of_with_previous(instance, asl, value, previous_values);
          }
          return false;
        }
        Value::Vector(id) => {
          if let Ok(values) = instance.get_vector(id) {
            let values_num = values.len();
            let min = min.clone();
            let max = max.clone();
            if min <= values_num && (max == 0 || values_num <= max) {
              return value_type.is_type_of_with_previous(instance, asl, value, previous_values);
            }
          }
          return false;
        }
        _ => return false,
      },
      Self::Or(types) => {
        for t in types.iter() {
          if t.is_type_of_with_previous(instance, asl, value, previous_values) {
            return true;
          }
        }
        return false;
      }
      Self::And(types) => {
        for t in types.iter() {
          if !t.is_type_of_with_previous(instance, asl, value, previous_values) {
            return false;
          }
        }
        return true;
      }
      Self::RefOf(t) => {
        if let Value::Reference(reference) = value {
          match instance.get_var_by_ref(reference) {
            Ok(v) => return t.is_type_of_with_previous(instance, asl, &v, previous_values),
            Err(_) => return false,
          }
        }
      }
      Self::ValueOption(values) => {
        for v in values.iter() {
          if value == v {
            return true;
          }
        }
        return false;
      }

      Self::Check {
        value_type,
        token_list,
        id_list,
      } => {
        if value_type.is_type_of_with_previous(instance, asl, &value, previous_values) {
          let mut values = previous_values.clone();
          values.push(value.clone());
          if let CommandExecReturn::Stack(stack) = CommandExec::run_command_with_params(
            instance,
            asl,
            token_list.clone(),
            id_list.clone(),
            values,
          ) {
            let mut stack = stack;
            if let Some(Value::Boolean(result)) = stack.pop() {
              return result;
            }
          }
        }
        return false;
      }
      Self::Not(t) => return !t.is_type_of(instance, asl, value),
      Self::SelfType(_) => {
        if let Value::Type(t) = value {
          return self == t;
        }
      }
      Self::SubType(types) => {
        if let Value::Type(t) = value {
          for t1 in types.iter() {
            if t1.is_supertype_of(instance, asl, t) {
              return true;
            }
          }
          return false;
        }
      }
      Self::SuperType(types) => {
        if let Value::Type(t) = value {
          for t1 in types.iter() {
            if t.is_supertype_of(instance, asl, t1) {
              return true;
            }
          }
          return false;
        }
      }
      Self::WrappedValue => {
        if let Value::WrappedValue { .. } = value {
          return true;
        }
      }
      Self::CustomType => {
        if let Value::Type(Self::CustomTypeInstance { .. }) = value {
          return true;
        }
      }
      Self::CustomTypeInstance { .. } => {
        if let Value::WrappedValue { wrap_type, .. } = value {
          return self == wrap_type;
        }
      }
      Self::Dynamic(reference) => {
        if let Ok(Value::Type(referenced_type)) = instance.get_var_by_ref(reference) {
          return referenced_type.is_type_of_with_previous(instance, asl, value, previous_values);
        } else {
          return false;
        }
      }
      _ => return false,
    }
    return false;
  }

  pub fn is_type_of(&self, instance: &mut Instance, asl: &ASL, value: &Value) -> bool {
    self.is_type_of_with_previous(instance, asl, value, &Vec::new())
  }
}

impl SystemType {
  pub fn is_supertype_of(&self, instance: &mut Instance, asl: &ASL, other: &SystemType) -> bool {
    //Any type is a supertype of itself
    if self == other {
      return true;
    }

    match (self, other) {
      (Self::Any, Self::Void) => return false,
      (Self::Any, Self::Permeable) => return false,
      (Self::Any, Self::Multiple(_)) => return false,
      (Self::Any, Self::Many(_)) => return false,
      (Self::Any, _) => return true,
      (Self::AnyNumber, Self::PrimitiveNumber) => return true,
      (Self::AnyNumber, Self::PrimitiveUnsigned) => return true,
      (Self::AnyNumber, Self::PrimitiveSigned) => return true,
      (Self::AnyNumber, Self::PrimitiveFloat) => return true,
      (Self::AnyNumber, Self::Number) => return true,
      (Self::AnyNumber, Self::Integer) => return true,
      (Self::AnyNumber, Self::NonNegative) => return true,
      (Self::AnyNumber, Self::NonPositive) => return true,
      (Self::AnyNumber, Self::Unsigned) => return true,
      (Self::AnyNumber, Self::Positive) => return true,
      (Self::AnyNumber, Self::Negative) => return true,
      (Self::AnyNumber, Self::Natural) => return true,
      (Self::Number, Self::Integer) => return true,
      (Self::Number, Self::NonNegative) => return true,
      (Self::Number, Self::NonPositive) => return true,
      (Self::Number, Self::Unsigned) => return true,
      (Self::Number, Self::Positive) => return true,
      (Self::Number, Self::Negative) => return true,
      (Self::Number, Self::Natural) => return true,
      (Self::Integer, Self::Unsigned) => return true,
      (Self::Integer, Self::Natural) => return true,
      (Self::NonNegative, Self::Unsigned) => return true,
      (Self::NonNegative, Self::Positive) => return true,
      (Self::NonNegative, Self::Natural) => return true,
      (Self::NonPositive, Self::Negative) => return true,
      (Self::Unsigned, Self::Natural) => return true,
      (Self::Positive, Self::Natural) => return true,
      (Self::PrimitiveNumber, Self::PrimitiveUnsigned) => return true,
      (Self::PrimitiveNumber, Self::PrimitiveSigned) => return true,
      (Self::PrimitiveNumber, Self::PrimitiveFloat) => return true,
      (Self::String, Self::VarString) => return true,
      (Self::Reference, Self::GlobalReference) => return true,
      (Self::Reference, Self::LocalReference) => return true,
      (Self::Reference, Self::BlockReference) => return true,
      (Self::Reference, Self::RefOf(_)) => return true,
      (Self::Type, Self::ParamType) => return true,
      (Self::Type, Self::ValueType) => return true,
      (Self::Type, Self::CustomType) => return true,
      (Self::Type, Self::SubType(_)) => return true,
      (Self::Type, Self::SuperType(_)) => return true,
      (Self::SubType(types), Self::SubType(t1)) => {
        for tb in t1.iter() {
          let mut has_supertype = false;
          for ta in types.iter() {
            if ta.is_supertype_of(instance, asl, tb) {
              has_supertype = true;
              break;
            }
          }
          if !has_supertype {
            return false;
          }
        }
        return true;
      }
      (Self::SuperType(types), Self::SuperType(t1)) => {
        for tb in t1.iter() {
          let mut has_subtype = false;
          for ta in types.iter() {
            if tb.is_supertype_of(instance, asl, ta) {
              has_subtype = true;
              break;
            }
          }
          if !has_subtype {
            return false;
          }
        }
        return true;
      }
      (Self::ParamType, Self::ValueType) => return true,
      (Self::ParamType, Self::CustomType) => return true,
      (Self::ValueType, Self::CustomType) => return true,
      (Self::List, Self::ListOf(_)) => return true,
      (Self::List, Self::UniqueList(_)) => return true,
      (Self::List, Self::Sequence(_)) => return true,
      (Self::List, Self::Set(_)) => return true,
      (Self::ListOf(types), Self::ListOf(t1))
      | (Self::ListOf(types), Self::UniqueList(t1))
      | (Self::ListOf(types), Self::Sequence(t1))
      | (Self::ListOf(types), Self::Set(t1)) => {
        for tb in t1.iter() {
          let mut has_supertype = false;
          for ta in types.iter() {
            if ta.is_supertype_of(instance, asl, tb) {
              has_supertype = true;
              break;
            }
          }
          if !has_supertype {
            return false;
          }
        }
        return true;
      }
      (Self::Sequence(types), Self::Sequence(t1))
      | (Self::Sequence(types), Self::Set(t1))
      | (Self::Set(types), Self::Set(t1)) => {
        if types.len() == t1.len() {
          for (ta, tb) in types.iter().zip(t1.iter()) {
            if !ta.is_supertype_of(instance, asl, tb) {
              return false;
            }
          }
          return true;
        }
        return false;
      }
      (Self::Vector, Self::VectorOf(_)) => return true,
      (Self::VectorOf(types), Self::VectorOf(t1)) => {
        for tb in t1.iter() {
          let mut has_supertype = false;
          for ta in types.iter() {
            if ta.is_supertype_of(instance, asl, tb) {
              has_supertype = true;
              break;
            }
          }
          if !has_supertype {
            return false;
          }
        }
        return true;
      }
      (Self::Dictionary, Self::DictionaryOf(_)) => return true,
      (Self::DictionaryOf(types), Self::DictionaryOf(t1)) => {
        for tb in t1.iter() {
          let mut has_supertype = false;
          for ta in types.iter() {
            if ta.is_supertype_of(instance, asl, tb) {
              has_supertype = true;
              break;
            }
          }
          if !has_supertype {
            return false;
          }
        }
        return true;
      }
      (Self::Map, Self::MapOf(_)) => return true,
      (Self::MapOf(types), Self::MapOf(t1)) => {
        for tb in t1.iter() {
          let mut has_supertype = false;
          for ta in types.iter() {
            if ta.is_supertype_of(instance, asl, tb) {
              has_supertype = true;
              break;
            }
          }
          if !has_supertype {
            return false;
          }
        }
        return true;
      }
      (Self::Interface(interface1), Self::Interface(interface2))
      | (Self::Interface(interface1), Self::DictionaryInterface(interface2))
      | (Self::Interface(interface1), Self::MapInterface(interface2))
      | (Self::DictionaryInterface(interface1), Self::DictionaryInterface(interface2))
      | (Self::MapInterface(interface1), Self::MapInterface(interface2)) => {
        return interface1.is_super_interface_of(instance, asl, interface2);
      }
      (
        Self::Sized {
          value_type: t1,
          min: min1,
          max: max1,
        },
        Self::Sized {
          value_type: t2,
          min: min2,
          max: max2,
        },
      ) => {
        let max1 = max1.clone();
        let max2 = max2.clone();
        if t1.is_supertype_of(instance, asl, t2) {
          return min1 <= min2 && (max1 == 0 || (max2 != 0 && max2 <= max1));
        }
        return false;
      }
      (_, Self::Sized { value_type, .. }) => {
        return self.is_supertype_of(instance, asl, value_type);
      }
      (Self::Callable, Self::FunCom) => return true,
      (Self::Callable, Self::Meta) => return true,
      (Self::Callable, Self::Command) => return true,
      (Self::Callable, Self::Function) => return true,
      (Self::Callable, Self::SingleFunction) => return true,
      (Self::Callable, Self::MultiFunction) => return true,
      (Self::Callable, Self::CommandFunction) => return true,
      (Self::Callable, Self::NativeFunction) => return true,
      (Self::FunCom, Self::Command) => return true,
      (Self::FunCom, Self::Function) => return true,
      (Self::FunCom, Self::SingleFunction) => return true,
      (Self::FunCom, Self::MultiFunction) => return true,
      (Self::FunCom, Self::CommandFunction) => return true,
      (Self::FunCom, Self::NativeFunction) => return true,
      (Self::Function, Self::SingleFunction) => return true,
      (Self::Function, Self::MultiFunction) => return true,
      (Self::Function, Self::CommandFunction) => return true,
      (Self::Function, Self::NativeFunction) => return true,
      (Self::SingleFunction, Self::CommandFunction) => return true,
      (Self::SingleFunction, Self::NativeFunction) => return true,
      (Self::Voidable(t1), Self::Voidable(t2)) => {
        return t1.is_supertype_of(instance, asl, t2);
      }
      (Self::Multiple(types), Self::Multiple(t1))
      | (Self::Multiple(types), Self::Many(t1))
      | (Self::Many(types), Self::Many(t1)) => {
        if types.len() == t1.len() {
          for (ta, tb) in types.iter().zip(t1.iter()) {
            if !ta.is_supertype_of(instance, asl, tb) {
              return false;
            }
          }
          return true;
        }
        return false;
      }
      (_, Self::Or(types)) => {
        for t in types.iter() {
          if !self.is_supertype_of(instance, asl, t) {
            return false;
          }
        }
        return true;
      }
      (Self::Or(types), _) => {
        for t in types.iter() {
          if t.is_supertype_of(instance, asl, other) {
            return true;
          }
        }
        return false;
      }
      (Self::RefOf(t), Self::RefOf(t1)) => return t.is_supertype_of(instance, asl, t1),
      //TODO Review this logic and see a better way
      (_, Self::And(types)) => {
        for t in types.iter() {
          if self.is_supertype_of(instance, asl, t) {
            return true;
          }
        }
        return false;
      }
      (Self::And(types), _) => {
        for t in types.iter() {
          if t.is_supertype_of(instance, asl, other) {
            return true;
          }
        }
        return false;
      }
      (Self::ValueOption(values), Self::ValueOption(v1)) => {
        for val in v1.iter() {
          if !values.contains(val) {
            return false;
          }
        }
        return true;
      }
      (_, Self::ValueOption(values)) => {
        for val in values.iter() {
          if !self.is_type_of(instance, asl, val) {
            return false;
          }
        }
        return true;
      }
      (
        Self::Check {
          value_type: t1,
          token_list: tl1,
          id_list: il1,
        },
        Self::Check {
          value_type: t2,
          token_list: tl2,
          id_list: il2,
        },
      ) => {
        if t1.is_supertype_of(instance, asl, t2) {
          return tl1 == tl2 && il1 == il2;
        }
        return false;
      }
      (_, Self::Check { value_type: t, .. }) => {
        return self.is_supertype_of(instance, asl, t);
      }
      (Self::Not(t), Self::Not(t1)) => return t1.is_supertype_of(instance, asl, t),
      (Self::Not(t), _) => {
        return !t.is_supertype_of(instance, asl, other)
          && !other.is_supertype_of(instance, asl, t);
      }
      (_, _) => return false,
    }
  }
}
