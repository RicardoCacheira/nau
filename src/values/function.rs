use crate::asl::ASL;
use crate::command::command_exec::CommandExecReturn;
use crate::instance::Instance;
use crate::parsers::Token;
use crate::scopes::ScopeId;
use crate::values::{SystemType as Type, Value};
use std::rc::Rc;

#[derive(Clone)]
pub enum FunctionType {
  Native(fn(&mut Instance, &ASL, Vec<Value>) -> CommandExecReturn),
  Command {
    param_id_list: Rc<Vec<usize>>,
    start_value_list: Rc<Vec<Value>>,
    start_value_id_list: Rc<Vec<usize>>,
    com_token_list: Rc<Vec<Token>>,
    com_id_list: Rc<Vec<usize>>,
    all_scopes_list: Rc<Vec<usize>>,
  },
}

impl PartialEq for FunctionType {
  fn eq(&self, other: &Self) -> bool {
    match (self, other) {
      (FunctionType::Native(a), FunctionType::Native(b)) => return *a as usize == *b as usize,
      (
        FunctionType::Command {
          param_id_list: a0,
          start_value_list: a1,
          start_value_id_list: a2,
          com_token_list: a3,
          com_id_list: a4,
          ..
        },
        FunctionType::Command {
          param_id_list: b0,
          start_value_list: b1,
          start_value_id_list: b2,
          com_token_list: b3,
          com_id_list: b4,
          ..
        },
      ) => return a0 == b0 || a1 == b1 || a2 == b2 || a3 == b3 || a4 == b4,
      (_, _) => return false,
    }
  }
}

fn process_base_start_value_list(
  instance: &Instance,
  base_start_value_id_list: Rc<Vec<Value>>,
) -> Result<(Rc<Vec<Value>>, Rc<Vec<usize>>), String> {
  let mut start_value_list = Vec::new();
  let mut start_value_id_list = Vec::new();
  for val in base_start_value_id_list.iter() {
    if let Value::Reference(sid) = val {
      if let ScopeId::Block(id) = sid {
        match instance.get_var_by_ref(sid) {
          Ok(value) => {
            start_value_list.push(value);
            start_value_id_list.push(id.clone());
          }
          Err(err) => return Err(err),
        }
      }
    } else {
      return Err(String::from(
        "Non scope reference received in the the start values.",
      ));
    }
  }
  return Ok((Rc::new(start_value_list), Rc::new(start_value_id_list)));
}

pub fn create_command_function(
  instance: &Instance,
  return_type: Type,
  param_list: Rc<Vec<Type>>,
  param_id_list: Rc<Vec<usize>>,
  base_start_value_id_list: Rc<Vec<Value>>,
  com_token_list: Rc<Vec<Token>>,
  com_id_list: Rc<Vec<usize>>,
) -> Result<Value, String> {
  if param_list.len() == param_id_list.len() {
    match process_base_start_value_list(instance, base_start_value_id_list) {
      Ok((start_value_list, start_value_id_list)) => {
        let mut all_scopes_list = Vec::clone(&param_id_list);
        for id in start_value_id_list.iter() {
          all_scopes_list.push(id.clone());
        }
        return Ok(Value::Function {
          param_list,
          return_type,
          function_type: FunctionType::Command {
            param_id_list,
            start_value_list,
            start_value_id_list,
            com_token_list,
            com_id_list,
            all_scopes_list: Rc::new(all_scopes_list),
          },
        });
      }
      Err(err) => return Err(err),
    }
  } else {
    return Err(String::from(
      "Missmatched number of param types and param ids.",
    ));
  }
}

pub fn create_native_function(
  param_list: Rc<Vec<Type>>,
  return_type: Type,
  func: fn(&mut Instance, &ASL, Vec<Value>) -> CommandExecReturn,
) -> Result<Value, String> {
  return Ok(Value::Function {
    param_list,
    return_type,
    function_type: FunctionType::Native(func),
  });
}

pub fn create_multi_function(funcs: Rc<Vec<Value>>) -> Result<Value, String> {
  if funcs.len() > 0 {
    return Ok(Value::MultiFunction(funcs));
  } else {
    return Err(String::from(
      "Empty function List received for the multi function",
    ));
  }
}

pub fn create_meta_function(
  instance: &Instance,
  return_type: Type,
  input_com_var_id: usize,
  base_start_value_id_list: Rc<Vec<Value>>,
  com_token_list: Rc<Vec<Token>>,
  com_id_list: Rc<Vec<usize>>,
) -> Result<Value, String> {
  match process_base_start_value_list(instance, base_start_value_id_list) {
    Ok((start_value_list, start_value_id_list)) => {
      let mut all_scopes_list = vec![input_com_var_id];
      for id in start_value_id_list.iter() {
        all_scopes_list.push(id.clone());
      }
      return Ok(Value::Meta {
        return_type,
        input_com_var_id,
        start_value_list,
        start_value_id_list,
        com_token_list,
        com_id_list,
        all_scopes_list: Rc::new(all_scopes_list),
      });
    }
    Err(err) => return Err(err),
  }
}
