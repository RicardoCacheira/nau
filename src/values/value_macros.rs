#[macro_export]
macro_rules! val_str_f {
  ($string:expr) => {
    Value::String(Rc::new(String::from($string)))
  };
}

#[macro_export]
macro_rules! val_str_n {
  ($string:expr) => {
    Value::String(Rc::new(String::from($string)))
  };
}
