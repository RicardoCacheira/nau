use crate::asl::ASL;
use crate::command::command_exec::{CommandExec, CommandExecReturn};
use crate::instance::Instance;
use crate::parsers::{parser_consts, Token};
use crate::scopes::ScopeId;
use crate::utils::base_utils;
use crate::utils::to_system_string::ToSystemString;
use crate::values::{FunctionType, SystemType as Type, ValueMap};
use num_rational::BigRational;
use std::rc::Rc;
use std::string::ToString;

#[derive(Clone, PartialEq)]
pub enum Value {
  Number(BigRational), //TODO Maybe make it Rc<BigRational>
  Unsigned(u64),
  Signed(i64),
  Float(f64),
  String(Rc<String>),
  Raw(Rc<Vec<u8>>),
  Boolean(bool),
  Reference(ScopeId),
  Type(Type),
  List(Rc<Vec<Value>>),
  Vector(usize),
  Dictionary(Rc<ValueMap>),
  Map(usize),
  Buffer(usize),
  Command {
    token_list: Rc<Vec<Token>>,
    id_list: Rc<Vec<usize>>,
  },
  Meta {
    return_type: Type,
    input_com_var_id: usize,
    start_value_list: Rc<Vec<Value>>,
    start_value_id_list: Rc<Vec<usize>>,
    com_token_list: Rc<Vec<Token>>,
    com_id_list: Rc<Vec<usize>>,
    all_scopes_list: Rc<Vec<usize>>,
  },
  Function {
    return_type: Type,
    param_list: Rc<Vec<Type>>,
    function_type: FunctionType,
  },
  MultiFunction(Rc<Vec<Value>>),
  WrappedValue {
    wrap_type: Type,
    value: Rc<Value>,
  },
}

impl ToSystemString for Value {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    self.to_string_with_list(instance, asl, &mut Vec::new())
  }
}
impl Value {
  pub fn to_string_with_list(
    &self,
    instance: &mut Instance,
    asl: &ASL,
    list: &mut Vec<Self>,
  ) -> String {
    if list.contains(self) {
      return format!(
        "{}{}Circular{}{}",
        parser_consts::COMMENT_START_CHAR,
        parser_consts::COMMENT_BLOCK_CHAR,
        parser_consts::COMMENT_BLOCK_CHAR,
        parser_consts::COMMENT_START_CHAR
      );
    }
    match self {
      Value::Number(value) => value.to_string(),
      Value::Unsigned(value) => value.to_string(),
      Value::Signed(value) => value.to_string(),
      Value::Float(value) => value.to_string(),
      Value::String(value) => value.as_ref().clone(),
      Value::Raw(value) => format!("%| {} |%", base_utils::vec_8u_to_string(value)),
      Value::Boolean(value) => value.to_string(),
      Value::Reference(reference) => reference.to_string(instance, asl),
      Value::Type(value) => value.to_string(instance, asl),
      Value::List(values) => {
        if values.len() == 0 {
          String::from("[]")
        } else {
          let mut res = String::from("[\n  ");
          res.push_str(&base_utils::vec_to_str(
            &values
              .iter()
              .map(|value| {
                value
                  .to_token_string_with_list(instance, asl, list)
                  .replace("\n", "\n  ")
              })
              .collect(),
            ",\n  ",
          ));
          res.push_str("\n]");
          res
        }
      }
      Value::Vector(id) => match instance.get_vector(id) {
        Ok(vec) => {
          list.push(self.clone());
          if vec.len() == 0 {
            String::from("[]")
          } else {
            let vec = vec.clone();
            let mut res = String::from("[\n  ");
            res.push_str(&base_utils::vec_to_str(
              &vec
                .iter()
                .map(|value| {
                  value
                    .to_token_string_with_list(instance, asl, list)
                    .replace("\n", "\n  ")
                })
                .collect(),
              ",\n  ",
            ));
            res.push_str("\n]");
            res
          }
        }
        Err(_) => String::from("[ invalid vector ]"),
      },
      Value::Dictionary(map) => map.to_string_with_list(instance, asl, list),
      Value::Map(id) => match instance.get_map(id) {
        Ok(map) => {
          list.push(self.clone());
          map.clone().to_string_with_list(instance, asl, list)
        }
        Err(_) => String::from("{ invalid map }"),
      },
      Value::Buffer(id) => match instance.get_buffer(id) {
        Ok(buffer) => format!("%| {} |%", base_utils::vec_8u_to_string(buffer)),
        Err(_) => String::from("%| invalid buffer |%"),
      },
      Value::Command { token_list, .. } => {
        let mut string = String::from("( ");
        string.push_str(&base_utils::sys_str_vec_to_str(
          instance, asl, token_list, ", ",
        ));
        string.push_str(" )");
        string
      }
      Value::Meta {
        return_type,
        input_com_var_id,
        com_token_list,
        start_value_id_list,
        ..
      } => {
        let mut string = String::from("meta(");
        string.push_str(&return_type.to_string(instance, asl));
        let block_id_manager = instance.get_block_id_manager();
        string.push_str("\n  ");
        string.push_str(parser_consts::DECLARE_STR);
        string.push_str(&block_id_manager.id_to_name(input_com_var_id));
        string.push_str("\n  ");
        let start_var_list = start_value_id_list
          .iter()
          .map(|id| {
            format!(
              "{}{}",
              parser_consts::REF_STR,
              block_id_manager.id_to_name(id)
            )
          })
          .collect::<Vec<String>>();
        if start_var_list.len() > 0 {
          string.push_str(&base_utils::vec_to_str(&start_var_list, ", "));
          string.push_str("\n  ");
        }
        string.push_str("( ");
        string.push_str(&base_utils::sys_str_vec_to_str(
          instance,
          asl,
          com_token_list,
          ", ",
        ));
        string.push_str(" )\n)");
        string
      }
      Value::Function {
        param_list,
        return_type,
        function_type,
      } => {
        let mut string = String::from("fn(");
        string.push_str(&return_type.to_string(instance, asl));
        string.push_str("\n  ");
        match function_type {
          FunctionType::Native(_) => {
            string.push_str(&base_utils::sys_str_vec_to_str(
              instance, asl, param_list, ", ",
            ));
            string.push_str("\n  (#-native code-#)\n)");
          }
          FunctionType::Command {
            param_id_list,
            start_value_id_list,
            com_token_list,
            ..
          } => {
            let block_id_manager = instance.get_block_id_manager();
            let var_list = param_id_list
              .iter()
              .map(|id| block_id_manager.id_to_name(id))
              .collect::<Vec<String>>();
            let start_var_list = start_value_id_list
              .iter()
              .map(|id| {
                format!(
                  "{}{}",
                  parser_consts::REF_STR,
                  block_id_manager.id_to_name(id)
                )
              })
              .collect::<Vec<String>>();
            let param_values: Vec<String> = param_list
              .iter()
              .zip(var_list.into_iter())
              .map(|(param, id)| {
                format!(
                  "{} {}{}",
                  param.to_string(instance, asl),
                  parser_consts::DECLARE_STR,
                  id
                )
              })
              .collect();
            if param_values.len() > 0 {
              string.push_str(&base_utils::vec_to_str(&param_values, ", "));
              string.push_str("\n  ");
            }
            if start_var_list.len() > 0 {
              string.push_str(&base_utils::vec_to_str(&start_var_list, ", "));
              string.push_str("\n  ");
            }
            string.push_str("( ");
            string.push_str(&base_utils::sys_str_vec_to_str(
              instance,
              asl,
              com_token_list,
              ", ",
            ));
            string.push_str(" )\n)");
          }
        }
        string
      }
      Value::MultiFunction(function_list) => {
        let mut string = String::from("mfn[\n  ");
        string.push_str(
          &base_utils::sys_str_vec_to_str(instance, asl, function_list, ",\n")
            .replace("\n", "\n  "),
        );
        string.push_str("\n]");
        string
      }
      Value::WrappedValue {
        wrap_type:
          Type::CustomTypeInstance {
            to_string_token_list,
            to_string_id_list,
            ..
          },
        value,
      } => {
        let result = CommandExec::run_command_with_params(
          instance,
          asl,
          to_string_token_list.clone(),
          to_string_id_list.clone(),
          vec![Value::clone(value)],
        );
        match &result {
          CommandExecReturn::Error(_) => {
            return String::from("<to_string command exec error>");
          }
          CommandExecReturn::Return(_) => {
            return String::from("<to_string command return received error>");
          }
          CommandExecReturn::Loop(_) => {
            return String::from("<to_string command continue/break received error>");
          }
          CommandExecReturn::Exit(_) => {
            return String::from("<to_string command exit received error>");
          }
          CommandExecReturn::End => {
            return String::from("<to_string command end received error>");
          }
          CommandExecReturn::NoReturn => {
            return String::from("<to_string command no return error>");
          }
          CommandExecReturn::Stack(stack) => {
            if let Value::String(str_result) = &stack[stack.len() - 1] {
              return String::clone(str_result);
            } else {
              return String::from("<to_string non-string return error>");
            }
          }
        }
      }
      Value::WrappedValue { .. } => panic!("Invalid wrapped value reached."),
    }
  }

  pub fn to_token_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    self.to_token_string_with_list(instance, asl, &mut Vec::new())
  }

  pub fn to_token_string_with_list(
    &self,
    instance: &mut Instance,
    asl: &ASL,
    list: &mut Vec<Self>,
  ) -> String {
    match self {
      Value::String(value) => base_utils::to_token_string(value),
      Value::Unsigned(value) => format!("{}u", value),
      Value::Signed(value) => format!("{}s", value),
      Value::Float(value) => format!("{}f", value),
      Value::WrappedValue { .. } => {
        base_utils::to_token_string(&self.to_string_with_list(instance, asl, list))
      }
      _ => return self.to_string_with_list(instance, asl, list),
    }
  }

  pub fn get_value_type(&self) -> Type {
    return match self {
      Value::Number(_) => Type::Number,
      Value::Unsigned(_) => Type::PrimitiveUnsigned,
      Value::Signed(_) => Type::PrimitiveSigned,
      Value::Float(_) => Type::PrimitiveFloat,
      Value::String(_) => Type::String,
      Value::Raw(_) => Type::Raw,
      Value::Boolean(_) => Type::Boolean,
      Value::Reference(ScopeId::Global(_)) => Type::GlobalReference,
      Value::Reference(ScopeId::Local(_)) => Type::LocalReference,
      Value::Reference(ScopeId::Block(_)) => Type::BlockReference,
      Value::Type(_) => Type::Type,
      Value::List(_) => Type::List,
      Value::Vector(_) => Type::Vector,
      Value::Dictionary(_) => Type::Dictionary,
      Value::Map(_) => Type::Map,
      Value::Buffer(_) => Type::Buffer,
      Value::Command { .. } => Type::Command,
      Value::Function {
        function_type: FunctionType::Command { .. },
        ..
      } => Type::CommandFunction,
      Value::Function {
        function_type: FunctionType::Native(_),
        ..
      } => Type::NativeFunction,
      Value::Meta { .. } => Type::Meta,
      Value::MultiFunction(_) => Type::MultiFunction,
      Value::WrappedValue { wrap_type, .. } => wrap_type.clone(),
    };
  }

  pub fn wrap_value(
    instance: &mut Instance,
    asl: &ASL,
    wrap_type: &Type,
    value: Value,
  ) -> Result<Value, String> {
    if let Type::CustomTypeInstance { value_type, .. } = wrap_type {
      if value_type.is_type_of(instance, asl, &value) {
        return Ok(Value::WrappedValue {
          wrap_type: wrap_type.clone(),
          value: Rc::new(value),
        });
      } else {
        return Err(format!(
          "Invalid base type for the received value. Expected `{}`, received `{}`.",
          value_type.to_string(instance, asl),
          value.get_value_type().to_string(instance, asl)
        ));
      }
    } else {
      panic!("Attempted to create a wrapped value with a non-custom type.")
    }
  }
}
