use crate::asl::ASL;
use crate::instance::Instance;
use crate::utils::base_utils;
use crate::utils::to_system_string::ToSystemString;
use crate::values::{SystemType, Value};
use std::collections::HashMap;
use std::rc::Rc;

#[derive(Clone)]
pub struct ValueMap {
  map: HashMap<String, Value>,
}

impl ValueMap {
  pub fn new() -> Self {
    ValueMap {
      map: HashMap::new(),
    }
  }

  pub fn from_value_list(
    instance: &mut Instance,
    asl: &ASL,
    list: &Vec<Value>,
  ) -> Result<Self, String> {
    if list.len() % 2 == 0 {
      let mut map = HashMap::new();
      let mut iter = list.iter();
      for _ in 0..list.len() / 2 {
        let key = iter.next().unwrap();
        if let Value::String(k) = key {
          map.insert(k.as_ref().clone(), iter.next().unwrap().clone());
        } else {
          return Err(format!(
            "The key {} must be a string.",
            key.to_string(instance, asl)
          ));
        }
      }
      return Ok(ValueMap { map });
    }
    return Err(String::from(
      "A dictionary must have an equal number of keys and values",
    ));
  }

  pub fn get_key_str_list(&self) -> Vec<&str> {
    let keys = self.map.keys().collect::<Vec<&String>>();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(key.as_str());
    }
    res.sort_unstable();
    return res;
  }

  pub fn get_key_ref_list(&self) -> Vec<&String> {
    let keys = self.map.keys().collect::<Vec<&String>>();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(key);
    }
    res.sort_unstable();
    return res;
  }

  pub fn get_key_list(&self) -> Vec<String> {
    let keys = self.map.keys().collect::<Vec<&String>>();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(key.clone());
    }
    res.sort_unstable();
    return res;
  }

  pub fn get_value_list(&self) -> Vec<Value> {
    let mut keys = self.map.keys().collect::<Vec<&String>>();
    keys.sort_unstable();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(self.map.get(key).unwrap().clone());
    }
    return res;
  }

  pub fn get_key_list_as_value(&self) -> Value {
    let keys = self.get_key_list();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(Value::String(Rc::new(key.clone())));
    }
    return Value::List(Rc::new(res));
  }

  pub fn get_value_list_as_value(&self) -> Value {
    return Value::List(Rc::new(self.get_value_list()));
  }

  pub fn get_key_value_ref_list(&self) -> Vec<(&str, &Value)> {
    let keys = self.get_key_str_list();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push((key, self.map.get(key).unwrap()));
    }
    return res;
  }

  pub fn get(&self, key: &str) -> Option<&Value> {
    return self.map.get(key);
  }
  pub fn set(&mut self, key: &str, value: Value) {
    self.map.insert(String::from(key), value);
  }
  pub fn contains(&self, value: &Value) -> bool {
    self.map.values().collect::<Vec<&Value>>().contains(&value)
  }
  pub fn contains_key(&self, key: &str) -> bool {
    self.map.contains_key(key)
  }
  pub fn remove(&mut self, key: &str) -> Option<Value> {
    self.map.remove(key)
  }
  pub fn clear(&mut self) {
    self.map.clear();
  }
  pub fn to_value_list(&self) -> Value {
    let keys = self.get_key_list();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(Value::String(Rc::new(key.clone())));
      res.push(self.map.get(&key).unwrap().clone());
    }
    return Value::List(Rc::new(res));
  }
  pub fn len(&self) -> usize {
    self.map.len()
  }
  pub fn to_string_with_list(
    &self,
    instance: &mut Instance,
    asl: &ASL,
    list: &mut Vec<Value>,
  ) -> String {
    let key_list = self.get_key_list();
    if key_list.len() == 0 {
      return String::from("{}");
    }
    let mut res = String::from("{\n  ");
    res.push_str(&base_utils::vec_to_str(
      &key_list
        .iter()
        .map(|key| {
          format!(
            "{} {}",
            base_utils::to_token_string(key),
            &self
              .map
              .get(key)
              .unwrap()
              .to_token_string_with_list(instance, asl, list)
              .replace("\n", "\n  ")
          )
        })
        .collect(),
      ",\n  ",
    ));
    res.push_str("\n}");
    return res;
  }
}

impl PartialEq<ValueMap> for ValueMap {
  fn eq(&self, other: &ValueMap) -> bool {
    let entries = self.get_key_ref_list();
    let other_entries = other.get_key_ref_list();
    if entries == other_entries {
      for key in self.map.keys() {
        if self.map.get_key_value(key) != other.map.get_key_value(key) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}

impl ToSystemString for ValueMap {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    self.to_string_with_list(instance, asl, &mut Vec::new())
  }
}

#[derive(Clone)]
pub struct ValueMapInterface {
  map: HashMap<String, SystemType>,
}

impl ValueMapInterface {
  pub fn from_value_map(list: &ValueMap) -> Result<Self, String> {
    let keys = list.map.keys().collect::<Vec<&String>>();
    let mut map = HashMap::new();
    for key in keys {
      if let Value::Type(t) = list.map.get(key).unwrap() {
        map.insert(key.clone(), t.clone());
      } else {
        return Err(String::from(
          "A Value Map Interface must only have Types as values.",
        ));
      }
    }
    Ok(ValueMapInterface { map })
  }

  fn get_key_ref_list(&self) -> Vec<&String> {
    let keys = self.map.keys().collect::<Vec<&String>>();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(key);
    }
    res.sort_unstable();
    return res;
  }

  fn get_key_list(&self) -> Vec<String> {
    let keys = self.map.keys().collect::<Vec<&String>>();
    let mut res = Vec::with_capacity(keys.len());
    for key in keys {
      res.push(key.clone());
    }
    res.sort_unstable();
    return res;
  }

  pub fn is_interface_for(&self, instance: &mut Instance, asl: &ASL, value_map: &ValueMap) -> bool {
    for key in self.map.keys() {
      if let Some(value) = value_map.get(key) {
        if !self.map.get(key).unwrap().is_type_of(instance, asl, value) {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }
  pub fn is_super_interface_of(
    &self,
    instance: &mut Instance,
    asl: &ASL,
    other: &ValueMapInterface,
  ) -> bool {
    for key in self.map.keys() {
      if let Some(t) = other.map.get(key) {
        if !self.map.get(key).unwrap().is_supertype_of(instance, asl, t) {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }
}

impl PartialEq<ValueMapInterface> for ValueMapInterface {
  fn eq(&self, other: &ValueMapInterface) -> bool {
    let entries = self.get_key_ref_list();
    let other_entries = other.get_key_ref_list();
    if entries == other_entries {
      for key in self.map.keys() {
        if self.map.get_key_value(key) != other.map.get_key_value(key) {
          return false;
        }
      }
      return true;
    }
    return false;
  }
}

impl ToSystemString for ValueMapInterface {
  fn to_string(&self, instance: &mut Instance, asl: &ASL) -> String {
    let mut res = String::from("{");
    let key_list = self.get_key_list();
    let mut keys = key_list.iter();
    if let Some(k) = keys.next() {
      res.push(' ');
      res.push_str(&base_utils::to_token_string(k));
      res.push(' ');
      res.push_str(&self.map.get(k).unwrap().to_string(instance, asl));
    }
    for key in keys {
      res.push_str(", ");
      res.push_str(&base_utils::to_token_string(key));
      res.push(' ');
      res.push_str(&self.map.get(key).unwrap().to_string(instance, asl));
    }
    res.push_str(" }");
    return res;
  }
}
