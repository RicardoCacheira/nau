build-debug : clean-debug debug-assets
	cargo build
	cp ./target/debug/nau ./debug_bundle/nau

install-debug :
	PATH=$PATH:$(pwd)/debug_bundle

debug-assets :
	cp -R ./assets ./debug_bundle

clean-debug :
	rm -fR ./debug_bundle

build-release : clean-release release-assets
	cargo build --release
	cp ./target/release/nau ./release_bundle/nau

install-release :
	PATH=$PATH:$(pwd)/release_bundle

release-assets :
	cp -R ./assets ./release_bundle

clean-release : clean-zip
	rm -fR ./release_bundle

clean-zip :
	rm -f mac_release.zip
	rm -f linux_release.tar.gz

clean : clean-release clean-debug

zip-mac : build-release
	zip -r -X mac_release release_bundle

zip-linux : build-release
	tar -zcf linux_release.tar.gz release_bundle