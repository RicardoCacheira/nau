clear()
println("REPL.nau\n")
::'PROMPT ">"

::_get_prompt fn(String
  (
    if(instance_of(PROMPT,FunCom)
      (
        return(to_string(PROMPT()))
      )
    )
    return(to_string(PROMPT))
  )
)

::_clean_error fn(String
  String ->err
  (
    return(|>(err,[split,"\n"],[filter,(not(contains(?,"repl.nau")))],[join,"\n"]))
  )
)

# History Logic

::history new_vec()
::history_pos 0u

::history_prev fn(Void
  Reference ->buffer Reference ->buffer_pos
  (
    if(ne(history_pos,0u)
      (
        sub(&history_pos,1u)
        $@buffer get(history,history_pos)
        $@buffer_pos size_u(@buffer)
      )
    )
  )
)

::history_next fn(Void
  Reference ->buffer Reference ->buffer_pos
  (
    ifelse(
      (eq(history_pos,size_u(history)))
      (
        $@buffer ""
        $@buffer_pos 0u
      )
      (
        add(&history_pos,1u)
        ifelse(
          (eq(history_pos,size_u(history)))
          (
            $@buffer ""
            $@buffer_pos 0u
          )
          (
            $@buffer get(history,history_pos)
            $@buffer_pos size_u(@buffer)
          )
        )
      )
    )
  )
)

::update_history fn(Void
  String ->buffer
  (
    ifelse(
      (eq(history_pos,size_u(history)))
      (
        push(history,buffer)
      )
      (ne(get(history,history_pos),buffer))
      (
        push(history,buffer)
      )
      (
        push(history,rem(history,history_pos))
      )
    )
    $history_pos size_u(history)
  )
)


#Auto Complete Logic

->functions

::get_current_value fn(String
  String ->input PrimitiveUnsigned ->pos
  (
    return(|>(slice(input,0,pos),[split," "],[get,-1]))
  )
)

::auto_complete_options fn(ListOf(String)
  String ->input
  (
    return(filter(functions,(starts_with(?,input))))
  )
)

::get_common_start fn(String
  ListOf(String) ->options
  (
    if(is_empty(options)
      (return(""))
    )
    ::common get(options,0)
    for(pop_f(options)
      (
        ::curr_size size_u(?)
        if(gt(size_u(common),curr_size)
          (
            $common slice(common,0u,curr_size)
          )
        )
        loop(
          (
            ifelse(
              (starts_with(?,common))
              (break())
              (
                $common slice(common,0u,sub(size_u(common),1u))
              )
            )
          )
        )
        if(is_empty(common)
          (return(""))
        )
      )
    )
    return(common)
  )
)

::get_auto_complete fn(Many(ListOf(String),String)
  String ->input PrimitiveUnsigned ->pos
  (
    $input |>(input,
      [replace,"("," "],
      [replace,")"," "]
      [replace,"["," "]
      [replace,"]"," "]
      [replace,"{"," "]
      [replace,"}"," "]
      [replace,","," "]
    )
    ::prefix ""
    ::curr get_current_value(input,pos)
    ifelse(
      (starts_with(curr,"::"))
      ($prefix "::")
      (starts_with(curr,"$@"))
      ($prefix "$@")
      (starts_with(curr,"$"))
      ($prefix "$")
      (starts_with(curr,"->"))
      ($prefix "->")
      (starts_with(curr,"&"))
      ($prefix "&")
      (starts_with(curr,"@"))
      ($prefix "@")
    )
    $curr slice(curr,size_u(prefix),size_u(curr))
    ::options auto_complete_options(curr)
    ::common_start get_common_start(options)
    if(is_empty(common_start)
      (return([[],""]))
    )
    return([options,slice(common_start,size_u(curr),size_u(common_start))])
  )
)

::_add_to_buffer fn(Void Reference ->buffer Reference ->buffer_pos String ->input
  (
    ifelse(
      (eq(@buffer_pos,size_u(@buffer)))
      (
        $@buffer add(@buffer,input)
      )
      (
        $@buffer add(slice(@buffer,0,@buffer_pos) add(input,slice(@buffer,@buffer_pos,size_u(@buffer))))
      )
    )
    $@buffer_pos add(@buffer_pos,size_u(input))
  )
)

::prompt_extension fn(Void
  Reference ->buffer Reference ->buffer_pos String ->input
  ( 
    switch(input
      'KEY_UP
      (history_prev(buffer,buffer_pos))
      'KEY_DOWN
      (history_next(buffer,buffer_pos))
      'KEY_TAB
      (
        ::options ::auto_complete_part get_auto_complete(@buffer,@buffer_pos)
        _add_to_buffer(buffer,buffer_pos,auto_complete_part)
      )
    )
  )  
)

::process_result mfn(
  fn(String
    Any ->value
    (
      return(to_token_string(value))
    )
  )
  fn(String
    (
      return("No value.")
    )
  )
)

# Main Loop

loop(
  (
    $functions keys(dump_global_vars())
    ::input prompt(_get_prompt(),0u,prompt_extension)
    update_history(input)
    print("\n")
    if(eq(input,"exit")
      (break())
    )
    try(
      (
        ::com parse(input)
        println("\n" process_result(com()))
      )
      (println("\n" _clean_error(?)))
    )
  )
)