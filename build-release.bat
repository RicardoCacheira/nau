del /s /q .\release_bundle
xcopy /s /i .\assets .\release_bundle
cargo build --release
copy ".\target\release\nau.exe" ".\release_bundle\nau.exe"