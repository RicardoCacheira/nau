FUNCTION_LIST=$(dirname "$0")
ROOT=$FUNCTION_LIST/../..

function getFile(){
  if [ ! -f "$1" ]; then
      curl $2 > $1
  fi
}

cd $ROOT
make build-release
$ROOT/release_bundle/nau $FUNCTION_LIST/generate_function_json.nau
getFile $FUNCTION_LIST/simplehtml.js "https://bitbucket.org/RicardoCacheira/simplehtml/raw/master/simplehtml.js"
getFile $FUNCTION_LIST/w3.css "https://www.w3schools.com/w3css/4/w3.css"
echo "<script>\nlet functions = $(cat $FUNCTION_LIST/functions.json)\n</script>\n$(cat $FUNCTION_LIST/base.html)" > $FUNCTION_LIST/index.html